var app = angular.module('myApp', ['ui.bootstrap']);

app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.controller('TestCaseShowList', function ($scope, $http) {

    $scope.list = [];
    $('#mess').hide();

    var showtext = function(){
        $('#mess').show();
    };

    var hidetext = function(){
        $('#mess').hide();
    };

    $http.post('/api/testcase/showList','').then(function(response){
        $scope.list = response.data;
        $scope.listNumber = $scope.list.length;
    });

    var reload = function(){
        $http.post('/api/testcase/showList','').then(function(response){
            $scope.list = response.data;
            $scope.listNumber = $scope.list.length;
        });
    };

    $scope.getCaseId = function(e){
        $scope.idGet = $(e.target).data('id');
        $scope.nameGet = $(e.target).data('name');
    };

    $scope.removeCase = function(){
        var id = $scope.idGet;
        $http({
            method: 'POST',
            url: '/api/testcase/removeAt',
            data: id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
            $('#btnClose').click();
            reload();
            if(response.data > 0){
                showtext();
            }
        })
    };

});

app.controller('TestCaseAction', function ($scope, $http) {


    $scope.columns = [];
    $scope.isNumber = 0;
    $scope.ArrObjects = {};
    $scope.TestCase = {};

    var showtext = function(text){
        $('#mess').show();
        $('#mess').html(text);
    };

    var hidetext = function(){
        $('#mess').hide();
    };

    $scope.addNewColumn = function () {
        var newItemNo = $scope.columns.length + 1;
        $scope.isNumber = $scope.columns.length + 1;
        $scope.ArrObjects = { 'colId': 'col' + newItemNo, 'StepCase': newItemNo };
        $scope.columns.push($scope.ArrObjects);
    };

    $scope.removeColumn = function (index) {
        // remove the row specified in index
        if ($scope.columns.length > -1) {
            $scope.columns.splice(index, 1);
        }
        if($scope.columns.length === 0){
            $scope.isNumber = 0;
        }
    };

    $scope.addCase = function(){
        if($scope.columns.length === 0){
            showtext('Need Add Steps');
        }else{
            hidetext();
            if($scope.TestCase.Description)
            addTestCase($scope.TestCase);
        }
    };

    var addTestCase = function(caseObj){
        $http({
            method: 'POST',
            url: '/api/testcase/addnew',
            data: caseObj,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
            console.log(response);
        });
    };

    /*
    $scope.addCase = function(){
        if($scope.columns.length === 0){
            showtext('Need Add Steps');
        } else {
            hidetext();
            Object.assign($scope.ArrObjects, {'CaseName': $scope.CaseName, 'Link' : $scope.Link, 'TestItems': $scope.TestItems});
            for (var i = 0; i < $scope.columns.length; i++) {
                $scope.columns[i].CaseName = $scope.CaseName;
                $scope.columns[i].Link = $scope.Link;
                $scope.columns[i].TestItems = $scope.TestItems;

                $http({
                    method: 'POST',
                    url: '/api/testcase/addnew',
                    data: $scope.columns[i],
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (response) {
                    if (i == $scope.columns.length) {
                        window.location.href = '/admin/testcase/list';
                    }
                });

            }
        }
    };
    */

    $scope.checkCaseName = function(){
        var casename = $('#CaseName').val();
        $http({
            method: 'POST',
            url: '/api/testcase/checkName',
            data: casename,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
            if(response.data.length > 0){
                showtext('Case Name is exits !');
            }else{
                hidetext();
            }
        });
    };

    $scope.resetForm = function(){
        alert('reset form');
    };
});

app.controller('TestCaseEditU', function($scope, $http){

    $scope.columns = [];
    $scope.ArrObjects = {};
    $scope.Piority = [];


    var showtext = function(text){
        $('#mess').show();
        $('#mess').html(text);
    };

    var hidetext = function(){
        $('#mess').hide();
    };


    $scope.addNewColumn = function () {
        var newItemNo = $scope.columns.length + 1;
        $scope.isNumber = $scope.columns.length + 1;
        $scope.ArrObjects = { 'colId': 'col' + newItemNo, 'StepCase': newItemNo };
        $scope.columns.push($scope.ArrObjects);
        $scope.Piority.push(newItemNo);
    };

    $scope.removeColumn = function (index) {
        // remove the row specified in index
        if ($scope.columns.length != 1 && $scope.columns.length != 0) {
            $scope.columns.splice(index, 1);
            $scope.Piority.splice(index, 1);
        }
    };

    $http({
        method: 'POST',
        url: '/api/testcase/checkName',
        data: $('#CaseName').val(),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function (response) {
        $scope.columns = response.data;
        for(var i = 0; i < response.data.length; i++){
            $scope.Piority.push(response.data[i].Piority);
        }
        $scope.Piority = arrUnique($scope.Piority);
    });

    var arrUnique = function(arr) {
        var obj = {};
        var ret_arr = [];
        for (var i = 0; i < arr.length; i++) {
            obj[arr[i]] = true;
        }
        for (var key in obj) {
            ret_arr.push(key);
        }
        return ret_arr;
    };


    $scope.updateFunc = function(){
        Object.assign($scope.ArrObjects, {'CaseName': $('#CaseName').val(), 'Link': $('#Link').val(), 'TestItems': $('#TestItems').val()});
        var caseid = $('#caseid').val();
        $http({
            method: 'POST',
            url: '/api/testcase/removeAt',
            data: caseid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
            if(response.data > 0){
                for (var i = 0; i < $scope.columns.length; i++) {
                    $scope.columns[i].CaseName = $('#CaseName').val();
                    $scope.columns[i].Link = $('#Link').val();
                    $scope.columns[i].TestItems = $('#TestItems').val();
                    $http({
                        method: 'POST',
                        url: '/api/testcase/addnew',
                        data: $scope.columns[i],
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function (response) {
                        if (i == $scope.columns.length) {
                            window.location.href = '/admin/testcase/list';
                        }
                    });
                }
            }
        })

    };

    $scope.checkLink = function(){
        var link = $('#Link').val();
        var http = link.substring(0,7);
        var https = link.substring(0,8);
        if(http === 'http://'){
            hidetext();
        } else if (https === 'https://'){
            hidetext();
        }else{
            showtext('Test Link invaild !');
        }
    };

});