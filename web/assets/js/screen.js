function resizeLine() {
  var lineHeight = $( document ).height() * 0.43; // line height = 40%
  $('.screen-line-resize').height(lineHeight);
}

function reloadIframes(board) {
  // Get all iframes
  $('iframe', board).each(function() {
    // If current src is empty
    if ($(this).attr('src') === '') {
      // Reload
      $(this).attr('src', $(this).data('src'));
    }
  });
}

function unloadIframes(board) {
  // Get all iframes
  $('iframe', board).each(function() {
    // Upload (set src to empty)
    $(this).attr('src', '');
  });
}

function loadScreensByIndex(index) {
  console.log('Loading screen ' + index);
  // Load 2 first screens
  var firstScreen = index;
  var secondScreen = index + 1;
  var $screenItems = $('.screen-item');

  if (secondScreen > ($screenItems.size() - 1) ) {
    secondScreen = 0;
  }

  // Reload first screen
  reloadIframes('.screen-item[data-screen=' + firstScreen + ']');

  // Reload second screen in 10 secs
  if (firstScreen !== secondScreen) {
    setTimeout(function() {
      reloadIframes('.screen-item[data-screen=' + secondScreen + ']');
    }, 10 * 1000);
  }

  // Unload all other screens
  $screenItems.each(function() {
    var currentScreenIndex = $(this).data('screen');
    if (currentScreenIndex !== firstScreen && currentScreenIndex !== secondScreen) {
      unloadIframes(this);
    }
  });
}

$(function() {
  // Resize items
  resizeLine();

  $(window).resize(function() {
    resizeLine();
  });

  // Unload all boards
  // Get all iframes
  $('.board-wrapper iframe').each(function() {
    // Reload
    $(this).data('src', $(this).attr('src'))
      .attr('src', '');
  });

  // Reload first 2 frame
  loadScreensByIndex(0);

  // Carousel
  var $carousel = $('.carousel');
  $carousel.carousel({
    pause: 'hover',
    interval: 30 * 1000 // Change slide very 30s
  });

  // On transition
  $carousel.on('slide.bs.carousel', function (e) {
    var screenIndex = $(e.relatedTarget).data('screen');
    loadScreensByIndex(screenIndex);
  });

  // Refresh whole page after 10 minutes
  var tenMinutes = 10 * 60 * 1000;
  setInterval(function() {
    window.location.reload(true)
  }, tenMinutes);
});