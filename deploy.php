<?php
namespace Deployer;
require 'recipe/symfony.php';

// Configuration
set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@bitbucket.org:brodev/metrics-hub.git');
set('branch', 'master');
add('shared_files', array());
add('shared_dirs', array(
    'web/files',
    'tools/selenium',
    'web/reports',
));

add('writable_dirs', array(
    'web/files',
    'web/files/boards',
    'web/files/items',
    'web/reports',
    'web/reports/data',
    'web/reports/zip',
));

// Servers
server('production', 'metrics-hub.beeketing.net')
    ->user('run')
    ->set('web_server', 'apache')
    ->set('http_user', 'www-data')
    ->identityFile()
    ->set('deploy_path', '/home/run/sites/metrics-hub-dep')
    ->pty(true);

server('staging', '54.214.137.114', 16889)
    ->user('beeketing')
    ->set('web_server', 'fpm')
    ->set('http_user', 'www-data')
    ->identityFile()
    ->set('deploy_path', '/home/beeketing/metrics-hub')
    ->pty(true);

// Tasks

desc('Restart apache service');
task('apache:reload', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    if (get('web_server') === 'apache') {
        run('sudo service apache2 reload');
    } else {
        run('sudo service php5.6-fpm reload');
    }
});
after('deploy:symlink', 'apache:reload');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Bump assets version
task('deploy:assets:update_version', function () {
    // Replace assets version
    run('sed -i -e "s/assets_version\s*:\s*.*/assets_version: {{ release_name }}/g" {{release_path}}/app/config/parameters.yml');
    run('cd {{release_path}} && bower install');
})->desc('Update assets version to invalidate cache');
after('deploy:vendors', 'deploy:assets:update_version');
