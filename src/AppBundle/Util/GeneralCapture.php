<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Util;


use Symfony\Component\Process\Process;

class GeneralCapture
{

    /**
     * @var String
     */
    private $casperJSBin;

    /** @var String */
    private $kernelRootDir;

    /**
     * @var String
     */
    private $modeAnalyticsEmail;

    /**
     * @var String
     */
    private $modeAnalyticsPassword;

    /**
     * GeneralCapture constructor.
     * @param String $casperJSBin
     * @param String $kernelRootDir
     * @param String $modeAnalyticsEmail
     * @param String $modeAnalyticsPassword
     */
    public function __construct($casperJSBin, $kernelRootDir, $modeAnalyticsEmail, $modeAnalyticsPassword)
    {
        $this->casperJSBin = $casperJSBin;
        $this->kernelRootDir = $kernelRootDir;
        $this->modeAnalyticsEmail = $modeAnalyticsEmail;
        $this->modeAnalyticsPassword = $modeAnalyticsPassword;
    }

    /**
     * Capture an url to file
     * @param $url
     * @param $width
     * @param $height
     * @param $waitTime
     * @param $output
     * @return bool
     * @throws \Exception
     */
    public function capture($url, $width, $height, $waitTime, $output)
    {
        $command = sprintf('
        export PATH=$PATH:/usr/local/bin/:' . dirname($this->casperJSBin) . ' && 
        %s %s/tools/screenshot/general.js "%s" "%s" --width=%d --height=%d --wait-time=%d --ma-email=%s --ma-password=%s',
            $this->casperJSBin, $this->kernelRootDir . '/..', base64_encode($url), $output,
            $width, $height, $waitTime, $this->modeAnalyticsEmail, $this->modeAnalyticsPassword);

        $process = new Process($command);

        $process->run();

        // Throw error if failed to get screenshot
        if (!$process->isSuccessful()) {
            throw new \Exception(sprintf('Failed to get screenshot with command "%s"', $command));
        }

        return $process->isSuccessful();
    }

}