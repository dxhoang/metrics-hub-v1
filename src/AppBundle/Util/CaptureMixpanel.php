<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Util;


use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;

class CaptureMixpanel
{

    /**
     * @var String
     */
    private $mixpanelEmail;

    /**
     * @var String
     */
    private $mixpanelPassword;

    /**
     * @var String
     */
    private $seleniumHost;

    /** @var String */
    private $kernelRootDir;

    /**
     * @var WebDriver
     */
    private $driver;

    /**
     * CaptureMixpanel constructor.
     * @param String $mixpanelEmail
     * @param String $mixpanelPassword
     * @param String $seleniumHost
     * @param String $kernelRootDir
     */
    public function __construct($mixpanelEmail, $mixpanelPassword, $seleniumHost, $kernelRootDir)
    {
        $this->mixpanelEmail = $mixpanelEmail;
        $this->mixpanelPassword = $mixpanelPassword;
        $this->seleniumHost = $seleniumHost;
        $this->kernelRootDir = $kernelRootDir;
    }


    /**
     * Capture a mixpanel report to image file
     * @param $url
     * @param $itemId
     * @return bool
     * @throws \Exception
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    public function capture($url, $itemId)
    {
        $output = $this->kernelRootDir . '/../web/files/items/' . $itemId . '.png';
        CommandEcho::writeln(sprintf('Capturing "%s" to "%s"', $url, $output));

        // Setup selenium
        $capabilities =  DesiredCapabilities::chrome();
        $capabilities->setCapability("chromeOptions", array(
            "args" => array(
                "--headless",
                "--no-sandbox",
                "--disable-gpu",
                "--no-proxy-server",
                "--no-default-browser-check",
                "--no-first-run",
                "--disable-boot-animation",
                "--disable-default-apps",
                "--disable-extensions",
                "--disable-translate"
            ),
        ));

        $this->driver = RemoteWebDriver::create($this->seleniumHost, $capabilities, 5000);
        $this->driver->manage()->window()->setSize(new WebDriverDimension(1280, 1024));

        // Ref code: https://github.com/DavertMik/php-webdriver-demo/blob/master/tests/GitHubTest.php
        ###########
        ## Login ##
        ###########
        $loginUrl = 'https://mixpanel.com/login/?next=/report/566847/ab_test/';
        $this->driver->get($loginUrl);
        $this->driver->wait(30);
        // Type in email
        $emailField = $this->driver->findElement(WebDriverBy::id('id_email'));
        $emailField->click();
        $this->driver->getKeyboard()->sendKeys($this->mixpanelEmail);

        // Type in password
        $passwordField = $this->driver->findElement(WebDriverBy::id('id_password'));
        $passwordField->click();
        $this->driver->getKeyboard()->sendKeys($this->mixpanelPassword);

        // pressing "Enter"
        $this->driver->getKeyboard()->pressKey(WebDriverKeys::ENTER);

        // Wait until logged in
        $element = $this->driver->findElement(WebDriverBy::cssSelector('.mp-chrome-header-row-primary'));
        $this->driver->wait()->until(
            WebDriverExpectedCondition::visibilityOf($element)
        );

        #####################
        ## Open report URL ##
        #####################
        // Debug
        // $url = 'https://mixpanel.com/report/566847/funnels/#view/1481685/conversion_unit:day,from_date:-29,length:30,length_unit:day,to_date:0';
        $this->driver->get($url);
        // wait 10 secs to report content to load
        $this->driver->wait(10);

        #####################
        ## Take screenshot ##
        #####################
        // Remove unused parts
        $this->driver->executeScript("$('.report_header, .segfilter, .retention_event_controls, #messages, .logo_footer, mp-chrome-header, mp-help-widget').hide();");

        // Take screen shot
        $this->driver->takeScreenshot($output);

        // Done close
        $this->driver->close();
        CommandEcho::writeln('Done');

        return true;
    }

    /**
     * Take element screenshot
     * @param RemoteWebElement $element
     * @return bool|string
     * @throws \Exception
     */
    protected function takeElementScreenShot(RemoteWebElement $element, $output)
    {
        // Change the Path to your own settings
        $screenshot = tempnam(sys_get_temp_dir(), time() . mt_rand(1, 1000) . '.png');

        // Change the driver instance
        $this->driver->takeScreenshot($screenshot);
        if(!file_exists($screenshot)) {
            throw new \Exception('Could not save screenshot');
        }

        $elementScreenshot = $output;

        $elementWidth = $element->getSize()->getWidth();
        $elementHeight = $element->getSize()->getHeight();

        $elementSrcX = $element->getLocation()->getX();
        $elementSrcY= $element->getLocation()->getY();

        // Create image instances
        $src = imagecreatefrompng($screenshot);
        $dest = imagecreatetruecolor($elementWidth, $elementHeight);

        // Copy
        imagecopy($dest, $src, 0, 0, $elementSrcX, $elementSrcY, $elementWidth, $elementHeight);

        imagepng($dest, $elementScreenshot);

        unlink($screenshot); // unlink function might be restricted in mac os x.

        if( ! file_exists($elementScreenshot)) {
            throw new \Exception('Could not save element screenshot');
        }

        return $elementScreenshot;
    }

}