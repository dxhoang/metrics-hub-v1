<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Util;


use Symfony\Component\Console\Output\ConsoleOutput;

class CommandEcho
{
    /**
     * Enable command echo?
     * @var bool
     */
    public static $enable = false;

    /**
     * Write message
     * @param $message
     * @param bool $newLine
     */
    public static function write($message, $newLine = false)
    {
        if (!self::$enable) {
            return;
        }

        // Echo using console output
        static $output = null;

        if ($output === null) {
            $output = new ConsoleOutput();
        }

        $output->write($message, $newLine);
    }

    /**
     * Write message and new line
     * @param $message
     */
    public static function writeln ($message)
    {
        CommandEcho::write($message, true);
    }

    /**
     * Is current progress run from cli command?
     * @return bool
     */
    public static function isCLICommand()
    {
        if (PHP_SAPI == 'cli') {
            $isCLICommand = true;
        } else {
            $isCLICommand = false;
        }

        return $isCLICommand;
    }
}