<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\RabbitMQ;


use AppBundle\Manager\ItemManager;
use AppBundle\Util\CommandEcho;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class CaptureImageConsumer implements ConsumerInterface
{

    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * CaptureImageConsumer constructor.
     * @param ItemManager $imageManager
     */
    public function __construct(ItemManager $imageManager)
    {
        $this->itemManager = $imageManager;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    public function execute(AMQPMessage $msg)
    {
        CommandEcho::writeln(sprintf('Processing %s', $msg->getBody()));
        $data = json_decode($msg->getBody(), true);

        // Find item
        $item = $this->itemManager->get($data['item_id']);

        // If item is null
        if (!$item || !$item->getId()) {
            // Skip
            CommandEcho::writeln('Skip item because id is null');
            return;
        }

        // If there are no mixpanel in url
        if (strpos($item->getUrl(), 'mixpanel.com') === false) {
            // Change to mode analytics return true
            $item->setType('modeanalytics');
            $this->itemManager->save($item);

            CommandEcho::writeln('Not mixpanel ~> set to mode analytics');
            return;
        }

        CommandEcho::writeln(sprintf('Start capturing image for item %d', $item->getId()));
        // If getRefreshAt is null then continue
        if (!$item->getRefreshedAt()) {
            CommandEcho::writeln('Item is not refreshed yet');
            // If item is updated last 2 hours
        } else if ($item->getRefreshedAt()->getTimestamp() > time() - 2 * 3600) {
            // Skip
            CommandEcho::writeln('Skip item ' . $item->getId() . ' because already updated last 2 hours');
            return;
        }
        // Stop if command not found
        if (!$item) {
            CommandEcho::writeln(sprintf('Item %s not found', $data['item_id']));
        } else {
            // Refresh
            $this->itemManager->refresh($item);

            // Update database
            $this->itemManager->setUpdated($item);
        }
    }
}