<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Campaign
 *
 * @ORM\Table(name="schedules")
 * @ORM\Entity
 */
class Schedule
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Board
     * @ORM\ManyToOne(targetEntity="Board")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $board;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="cycle", type="string", length=255, nullable=true)
     */
    private $cycle;

    /**
     * @var \DateTime
     * @ORM\Column(name="time", type="datetime", nullable=true)
     */
    private $time;

    /**
     * @var string
     * @ORM\Column(name="notify_slacks", type="text", nullable=true)
     */
    private $notifySlacks;

    /**
     * @var \DateTime
     * @ORM\Column(name="next_update_time", type="datetime", nullable=true)
     */
    private $nextUpdateTime;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="ph_schedule_mention_user"
     * )
     */
    private $mentionUsers;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mentionUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Schedule
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set board
     *
     * @param \AppBundle\Entity\Board $board
     * @return Schedule
     */
    public function setBoard(\AppBundle\Entity\Board $board = null)
    {
        $this->board = $board;

        return $this;
    }

    /**
     * Get board
     *
     * @return \AppBundle\Entity\Board 
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Set cycle
     *
     * @param string $cycle
     * @return Schedule
     */
    public function setCycle($cycle)
    {
        $this->cycle = $cycle;

        return $this;
    }

    /**
     * Get cycle
     *
     * @return string 
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return Schedule
     */
    public function setTime(\DateTime $time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set nextUpdateTime
     *
     * @param \DateTime $nextUpdateTime
     * @return Schedule
     */
    public function setNextUpdateTime($nextUpdateTime)
    {
        $this->nextUpdateTime = $nextUpdateTime;

        return $this;
    }

    /**
     * Get nextUpdateTime
     *
     * @return \DateTime 
     */
    public function getNextUpdateTime()
    {
        return $this->nextUpdateTime;
    }

    /**
     * Set notifySlacks
     *
     * @param string $notifySlacks
     * @return Schedule
     */
    public function setNotifySlacks($notifySlacks)
    {
        $this->notifySlacks = $notifySlacks;

        return $this;
    }

    /**
     * Get notifySlacks
     *
     * @return string 
     */
    public function getNotifySlacks()
    {
        return $this->notifySlacks;
    }

    /**
     * Add mentionUser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $mentionUser
     *
     * @return Schedule
     */
    public function addMentionUser(\Application\Sonata\UserBundle\Entity\User $mentionUser)
    {
        if (!$this->mentionUsers->contains($mentionUser)) {
            $this->mentionUsers[] = $mentionUser;
        }

        return $this;
    }

    /**
     * Remove mentionUser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $mentionUser
     */
    public function removeMentionUser(\Application\Sonata\UserBundle\Entity\User $mentionUser)
    {
        $this->mentionUsers->removeElement($mentionUser);
    }

    /**
     * Get mentionUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMentionUsers()
    {
        return $this->mentionUsers;
    }
}
