<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Campaign
 *
 * @ORM\Table(name="boards")
 * @ORM\Entity
 */
class Board
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Item", mappedBy="board")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $items;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     * @ORM\Column(name="in_screen", type="boolean")
     */
    private $inScreen = false;

    /**
     * @var integer
     * @ORM\Column(name="sort", type="integer", length=10, nullable=true)
     */
    private $sort = 10;

    /**
     * @var boolean
     * @ORM\Column(name="iframe", type="boolean")
     */
    private $iframe = false;

    /**
     * @var string
     * @ORM\Column(name="frame_url", type="string", length=255, nullable=true)
     */
    private $frameUrl;

    /**
     * @var boolean
     * @ORM\Column(name="service_status", type="boolean")
     */
    private $serviceStatus = false;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Service")
     */
    private $services;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Board
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Convert board to string
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Board
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Board
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Board
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add items
     *
     * @param \AppBundle\Entity\Item $items
     * @return Board
     */
    public function addItem(\AppBundle\Entity\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \AppBundle\Entity\Item $items
     */
    public function removeItem(\AppBundle\Entity\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function isInScreen()
    {
        return $this->inScreen;
    }

    /**
     * @param bool $inScreen
     */
    public function setInScreen($inScreen)
    {
        $this->inScreen = $inScreen;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isIframe()
    {
        return $this->iframe;
    }

    /**
     * @param bool $iframe
     */
    public function setIframe($iframe)
    {
        $this->iframe = $iframe;
    }

    /**
     * @return string
     */
    public function getFrameUrl()
    {
        return $this->frameUrl;
    }

    /**
     * @param string $frameUrl
     */
    public function setFrameUrl($frameUrl)
    {
        $this->frameUrl = $frameUrl;
    }


    /**
     * Get inScreen
     *
     * @return boolean
     */
    public function getInScreen()
    {
        return $this->inScreen;
    }

    /**
     * Get iframe
     *
     * @return boolean
     */
    public function getIframe()
    {
        return $this->iframe;
    }

    /**
     * Set serviceStatus
     *
     * @param boolean $serviceStatus
     *
     * @return Board
     */
    public function setServiceStatus($serviceStatus)
    {
        $this->serviceStatus = $serviceStatus;

        return $this;
    }

    /**
     * Get serviceStatus
     *
     * @return boolean
     */
    public function getServiceStatus()
    {
        return $this->serviceStatus;
    }

    /**
     * Add service
     *
     * @param \PHBundle\Entity\Service $service
     *
     * @return Board
     */
    public function addService(\PHBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \PHBundle\Entity\Service $service
     */
    public function removeService(\PHBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }
}
