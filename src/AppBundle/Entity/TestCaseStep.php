<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Campaign
 *
 * @ORM\Table(name="test_case_step")
 * @ORM\Entity
 */
class TestCaseStep
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="CaseId", type="string", length=255, nullable=true)
     */
    private $caseid;

    /**
     * @var integer
     * @ORM\Column(name="CaseStep", type="integer")
     */
    private $casestep;

    /**
     * @var string
     * @ORM\Column(name="Description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="Expectation", type="string", length=1000, nullable=true)
     */
    private $expectation;

    /**
     * @var string
     * @ORM\Column(name="Status", type="string", length=255, nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set caseid
     *
     * @param string $caseid
     * @return TestCaseStep
     */
    public function setCaseId($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return string
     */
    public function getCaseId()
    {
        return $this->caseid;
    }


    /**
     * Set casestep
     *
     * @param int $casestep
     * @return TestCaseStep
     */
    public function setCaseStep($casestep)
    {
        $this->casestep = $casestep;

        return $this;
    }

    /**
     * Get casestep
     *
     * @return int
     */
    public function getCaseStep()
    {
        return $this->casestep;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TestCaseStep
     */
    public function setDesctiption($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set expectation
     *
     * @param string $expectation
     * @return TestCaseStep
     */
    public function setExpectation($expectation)
    {
        $this->expectation = $expectation;

        return $this;
    }

    /**
     * Get expectation
     *
     * @return string
     */
    public function getExpectation()
    {
        return $this->expectation;
    }


    /**
     * Set status
     *
     * @param string $status
     * @return TestCaseStep
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}