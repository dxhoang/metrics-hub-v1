<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 4/20/16
 * Time: 11:48 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Campaign
 *
 * @ORM\Table(name="test_case")
 * @ORM\Entity
 */
class TestCase
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     * @ORM\Column(name="CaseId", type="string", length=255, nullable=true)
     */
    private $caseid;


    /**
     * @var string
     * @ORM\Column(name="CaseIdMix", type="string", length=255, nullable=true)
     */
    private $caseidmix;

    /**
     * @var string
     * @ORM\Column(name="CaseTitle", type="string", length=255, nullable=true)
     */
    private $casetitle;

    /**
     * @var string
     * @ORM\Column(name="CaseApp", type="string", length=255, nullable=true)
     */
    private $caseapp;

    /**
     * @var string
     * @ORM\Column(name="CaseModule", type="string", length=255, nullable=true)
     */
    private $casemodule;

    /**
     * @var string
     * @ORM\Column(name="Description", type="string", length=1000, nullable=true)
     */
    private $description;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="Created_At", type="datetime")
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set caseid
     *
     * @param string $caseid
     * @return TestCase
     */
    public function setCaseId($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return string
     */
    public function getCaseId()
    {
        return $this->caseid;
    }

    /**
     * Set caseidmix
     *
     * @param string $caseidmix
     * @return TestCase
     */
    public function setCaseIdMix($caseidmix)
    {
        $this->caseidmix = $caseidmix;

        return $this;
    }

    /**
     * Get caseidmix
     *
     * @return string
     */
    public function getCaseIdMix()
    {
        return $this->caseidmix;
    }

    /**
     * Set casetitle
     *
     * @param string $casetitle
     * @return TestCase
     */
    public function setCaseTitle($casetitle)
    {
        $this->casetitle = $casetitle;

        return $this;
    }

    /**
     * Get casetitle
     *
     * @return string
     */
    public function getCaseTitle()
    {
        return $this->casetitle;
    }


    /**
     * Set caseapp
     *
     * @param string $caseapp
     * @return TestCase
     */
    public function setCaseApplication($caseapp)
    {
        $this->caseapp = $caseapp;

        return $this;
    }

    /**
     * Get caseapp
     *
     * @return string
     */
    public function getCaseApplication()
    {
        return $this->caseapp;
    }


    /**
     * Set casemodule
     *
     * @param string $casemodule
     * @return TestCase
     */
    public function setCaseModule($casemodule)
    {
        $this->casemodule = $casemodule;

        return $this;
    }

    /**
     * Get casemodule
     *
     * @return string
     */
    public function getCaseModule()
    {
        return $this->casemodule;
    }



    /**
     * Set description
     *
     * @param string $description
     * @return TestCase
     */
    public function setDesctiption($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TestCase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


}