<?php
/**
 * (C) Beeketing, Inc.
 * Quan Truong <quan@beeketing.com>
 */

namespace AppBundle\StatsEntity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="high_sales_product")
 * @ORM\Entity
 */
class Product
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="img_url", type="string", length=255, nullable=true)
     */
    private $imgUrl;

    /**
     * @var string
     * @ORM\Column(name="product_ref_id", type="integer", length=11, nullable=true)
     */
    private $productRefId;

    /**
     * @var string
     * @ORM\Column(name="shop_id", type="integer", length=11, nullable=true)
     */
    private $shopId;

    /**
     * @var string
     * @ORM\Column(name="price", type="float", length=11, nullable=true)
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="sales_count", type="integer", length=11, nullable=true)
     */
    private $salesCount;

    /**
     * @var string
     * @ORM\Column(name="tags", type="text", nullable=true)
     */
    private $tags;

    /**
     * @var string
     * @ORM\Column(name="custom_tags", type="text", nullable=true)
     */
    private $customTags;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getImgUrl()
    {
        return $this->imgUrl;
    }

    /**
     * @param string $imgUrl
     */
    public function setImgUrl($imgUrl)
    {
        $this->imgUrl = $imgUrl;
    }

    /**
     * @return string
     */
    public function getProductRefId()
    {
        return $this->productRefId;
    }

    /**
     * @param string $productRefId
     */
    public function setProductRefId($productRefId)
    {
        $this->productRefId = $productRefId;
    }

    /**
     * @return string
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param string $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getSalesCount()
    {
        return $this->salesCount;
    }

    /**
     * @param string $salesCount
     */
    public function setSalesCount($salesCount)
    {
        $this->salesCount = $salesCount;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getCustomTags()
    {
        return $this->customTags;
    }

    /**
     * @param string $customTags
     */
    public function setCustomTags($customTags)
    {
        $cleanCustomTags = explode(',', $customTags);

        foreach ($cleanCustomTags as $key => $tag) {
            $tag = trim($tag);
            if (empty($tag)) {
                unset($cleanCustomTags[$key]);
                continue;
            }
            $cleanCustomTags[$key] = $tag;
        }

        $this->customTags = implode(', ', $cleanCustomTags);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}