<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 12/20/16
 * Time: 4:27 PM
 */

namespace AppBundle\Security;


use Application\Sonata\UserBundle\Entity\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseClass
{

    private $allowedDomains;

    /**
     * @param mixed $allowedDomains
     */
    public function setAllowedDomains($allowedDomains)
    {
        $this->allowedDomains = $allowedDomains;
    }

    /**
     * Return true if user domain is allowed to use
     * @param $email
     * @return bool
     */
    private function isAllowedDomain($email) {
        $parts = explode('@', $email, 2);
        return in_array($parts[1], $this->allowedDomains);
    }

    /**
     * @param User $user
     * @param UserResponseInterface $response
     * @throws \Exception
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        // Validate domain
        if (!$this->isAllowedDomain($response->getEmail())) {
            throw new \Exception($response->getEmail() . ' is not allowed to access this site');
        }

        $property = $this->getProperty($response);
        $username = $response->getUsername();
        //on connect - get the access token and the user ID

        $setter = 'set'.ucfirst($property);
        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter(null);
            $this->userManager->updateUser($previousUser);
        }
        //we connect current user
        $user->$setter($username);

        // Set firstname, lastname
        if (!$user->getFirstname() || !$user->getLastname()) {
            $user->setFirstname($response->getFirstName());
            $user->setLastname($response->getLastName());
        }

        $this->userManager->updateUser($user);
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        // Validate domain
        if (!$this->isAllowedDomain($response->getEmail())) {
            throw new \Exception($response->getEmail() . ' is not allowed to access this site');
        }

        $username = $response->getUsername();
        $property = $this->getProperty($response);
        /** @var User $user */
        $user = $this->userManager->findUserBy(array($property => $username));

        // If user not found, try to find with email
        if (null === $user) {
            $user = $this->userManager->findUserByEmail($response->getEmail());
        }

        //when the user is registrating
        if (null === $user) {
            // create new user here
            $user = $this->userManager->createUser();
            //I have set all requested data with the user's username
            //modify here with relevant data
            $user->setUsername($response->getEmail());
            $user->setEmail($response->getEmail());
            $user->setPassword($username);
            $user->setEnabled(true);

            // Set firstname, lastname
            $user->setFirstname($response->getFirstName());
            $user->setLastname($response->getLastName());
        } else {
            //if user exists - go with the HWIOAuth way
            $serviceName = $response->getResourceOwner()->getName();
            $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

            //update access token
            $user->$setter($response->getAccessToken());
        }

        // Set id
        $setter = 'set'.ucfirst($property);
        $user->$setter($username);


        // Set first name, last name if empty
        if (!$user->getFirstname() || !$user->getLastname()) {
            // Set firstname, lastname
            $user->setFirstname($response->getFirstName());
            $user->setLastname($response->getLastName());
        }
        $this->userManager->updateUser($user);

        return $user;
    }
}
