<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 12/22/16
 * Time: 6:29 PM
 */

namespace AppBundle\Command;


use AppBundle\Entity\Item;
use AppBundle\Manager\ItemManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateMixpanelCommand extends ContainerAwareCommand
{

    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * @var int $mixpanelRefreshMinutes
     */
    private $mixpanelRefreshMinutes = 60;

    protected function configure()
    {
        $this->setName('update-mixpanel');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->itemManager = $this->getContainer()->get('item_manager');
        $this->mixpanelRefreshMinutes = $this->getContainer()->getParameter('mixpanel_update_minutes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf('Start finding mixpanel images to refresh'));
        // Get all mixpanel items that haven't been updated last 60 minutes (configurable)
        $needRefreshItems = $this->itemManager->getNeedRefresh($this->mixpanelRefreshMinutes, 'mixpanel');

        $output->writeln(sprintf('Found %d items', count($needRefreshItems)));

        // Queue to update
        /** @var Item $item */
        foreach ($needRefreshItems as $item) {
            $this->itemManager->queueRefresh($item);
        }
    }

}