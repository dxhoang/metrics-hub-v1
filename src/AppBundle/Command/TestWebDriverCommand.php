<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 9/20/18
 * Time: 3:49 AM
 */

namespace AppBundle\Command;


use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestWebDriverCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('test-wd');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seleniumHost = 'http://127.0.0.1:4444/wd/hub';
//        $seleniumHost = 'http://67.205.171.56:44440/wd/hub';
        $browser = 'firefox';
//        $browser = 'chrome';

        // Setup selenium
        if ($browser === 'chrome') {
            $capabilities =  DesiredCapabilities::chrome();
            $capabilities->setCapability("chromeOptions", array(
                "args" => array(
                    "--headless",
                    "--no-sandbox",
                    "--disable-gpu",
                    "--no-proxy-server",
                    "--no-default-browser-check",
                    "--no-first-run",
                    "--disable-boot-animation",
                    "--disable-default-apps",
                    "--disable-extensions",
                    "--disable-translate"
                ),
            ));
        } else {
            $capabilities =  DesiredCapabilities::firefox();
            $capabilities->setCapability("moz:firefoxOptions", array(
                "args" => array(
                    "-headless",
                ),
            ));
        }

        $driver = RemoteWebDriver::create($seleniumHost, $capabilities, 5000);

        // Ref code: https://github.com/DavertMik/php-webdriver-demo/blob/master/tests/GitHubTest.php
        ###########
        ## Login ##
        ###########
        $loginUrl = 'https://mixpanel.com/login/?next=/report/566847/ab_test/';
        $driver->get($loginUrl);
        $driver->wait(30);

        $file = $this->getContainer()->getParameter('kernel.root_dir') . '/../web/test.png';
        shell_exec('rm -rf ' . $file);
        $driver->takeScreenshot($file);
        $output->writeln('Screenshot to ' . $file);
    }

}