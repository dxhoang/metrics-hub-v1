<?php

namespace AppBundle\Command;

use OldSound\RabbitMqBundle\Command\BaseConsumerCommand;
use PhpAmqpLib\Exception\AMQPProtocolConnectionException;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\DummyException;

/**
 * This is wrapper class will override rabbit consumer command
 * Class ConsumerCommand
 * @package SCN\CoreBundle\Command
 */
class ConsumerCommand extends BaseConsumerCommand
{
    protected function configure()
    {
        parent::configure();
        $this->setName('bk:rabbitmq:consumer')
        ;
    }

    /**
     * Override execute method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            parent::execute($input, $output);
        } catch (AMQPTimeoutException $timeEx) {
            // Timeout catch
        } catch (AMQPProtocolConnectionException $brokenEx) {
            // Broken connection
        } catch (AMQPRuntimeException $runtimeEx) {
            // Invalid agr and code
        } catch (DummyException $dummyEx) {
            // Dummy exception
        }
        return true;
    }

    protected function getConsumerService()
    {
        return 'old_sound_rabbit_mq.%s_consumer';
    }
}
