<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Command;


use AppBundle\Entity\Board;
use AppBundle\Entity\Item;
use AppBundle\Entity\Schedule;
use AppBundle\Manager\BoardManager;
use AppBundle\Manager\ItemManager;
use AppBundle\Manager\ScheduleManager;
use AppBundle\Util\CommandEcho;
use AppBundle\Util\GeneralCapture;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScheduleRunnerCommand extends ContainerAwareCommand
{

    /**
     * @var ScheduleManager
     */
    private $scheduleManager;

    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * @var BoardManager
     */
    private $boardManager;

    /**
     * @var GeneralCapture
     */
    private $generalCapture;

    /**
     * @var Router
     */
    private $router;

    protected function configure()
    {
        $this->setName('schedule-runner')
            ->addOption('current-time', 'c', InputOption::VALUE_OPTIONAL,
                'Option to hack time', new \DateTime())
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL , 'Type of schedules', null)
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->scheduleManager = $this->getContainer()->get('schedule_manager');
        $this->itemManager = $this->getContainer()->get('item_manager');
        $this->boardManager = $this->getContainer()->get('board_manager');
        $this->router = $this->getContainer()->get('router');
        $this->generalCapture = $this->getContainer()->get('general_capture');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Set current time
        $currentTime = $input->getOption('current-time');
        if (!($currentTime instanceof \DateTime)) {
            $currentTime = new \DateTime($currentTime);
        }

        // Find all schedules that need to run (next update time < now)
        $type = $input->getOption('type');
        $schedules = $this->scheduleManager->getForRunner($currentTime, $type);

        $output->writeln(sprintf('Found %d schedules (%s) need to run', count($schedules), $type ? $type : 'all types'));

        // Traverse
        /** @var Schedule $schedule */
        foreach ($schedules as $schedule) {
            $output->writeln(sprintf('Processing schedule %d', $schedule->getId()));

            // Only update / notify if next update time is not null
            if ($schedule->getNextUpdateTime() !== null) {
                // If this is update
                if ($schedule->getType() == 'update') {
                    // ~> Run update

                    $output->writeln('- Updating board');
                    $this->updateBoard($schedule->getBoard());

                } else {
                    // If this is notify
                    // ~> Run notify
                    $output->writeln('- Notifying board');
                    $this->notifySchedule($schedule);

                }
            } else {
                $output->writeln('Skip update or notify because null next update time');
            }

            // Set next update time
            $output->writeln('Set next updated time of board');
            $schedule->setNextUpdateTime($this->getNextUpdateTime($currentTime, $schedule->getCycle(), $schedule->getTime()));

            $this->scheduleManager->save($schedule, false);
        }

        $this->scheduleManager->flush();
    }

    /**
     * Get next update turn
     * @param \DateTime $currentTime
     * @param $cycle
     * @param \DateTime $time
     * @return \DateTime
     */
    private function getNextUpdateTime(\DateTime $currentTime, $cycle, \DateTime $time)
    {
        $nextUpdateTime = clone $currentTime;
        $nextUpdateTime->setTime($time->format('H'), $time->format('i'), $time->format('s'));

        switch ($cycle) {
            case 'weekly':
                $nextUpdateTime->modify('this ' . $time->format('l')); // eg: this sunday

                // Next week if passed
                if ($nextUpdateTime->getTimestamp() < $currentTime->getTimestamp()) {
                    $nextUpdateTime->modify('+1 week');
                }

                break;

            case 'monthly':
                $nextUpdateTime->setDate($nextUpdateTime->format('Y'), $nextUpdateTime->format('m'), $time->format('d'));

                // Next week if passed
                if ($nextUpdateTime->getTimestamp() < $currentTime->getTimestamp()) {
                    $nextUpdateTime->modify('+1 month');
                }

                break;

            case 'yearly':
                $nextUpdateTime->setDate($nextUpdateTime->format('Y'), $time->format('m'), $time->format('d'));

                // Next week if passed
                if ($nextUpdateTime->getTimestamp() < $currentTime->getTimestamp()) {
                    $nextUpdateTime->modify('+1 year');
                }

                break;

            case 'daily':
            default:

                // Tomorrow if passed
                if ($nextUpdateTime->getTimestamp() < $currentTime->getTimestamp()) {
                    $nextUpdateTime->modify('+1 day');
                }

        }

        return $nextUpdateTime;
    }

    /**
     * Update a board
     * @param Board $board
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateBoard(Board $board)
    {
        // Update db that we need to refresh items in this board
        $this->boardManager->refresh($board);
    }

    /**
     * Notify a schedule
     * @param Schedule $schedule
     * @throws \Exception
     */
    public function notifySchedule(Schedule $schedule)
    {
        // Skip if this is not product hub board
        if (!$schedule->getBoard()->getServiceStatus()) {
            CommandEcho::writeln('Skip because not service status board');
            return;
        }
        $this->getContainer()->get('ph.api.slack')->notifyBoard($schedule);
    }

}