<?php
/**
 * Copyright Beeketing Pte Ltd.
 * (c) Anhld <anhle@brodev.com>
 * Date: 9/19/16
 * Time: 18:59
 */

namespace AppBundle\Command;


use Buzz\Browser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RollbarLogSummaryCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    protected $apiEndPoint = 'https://api.rollbar.com/api/1/items/';

    /**
     * @var Browser
     */
    protected $buzz;

    /**
     * Configuration
     */
    protected function configure()
    {
        $this
            ->setName('summary-rollbar-log')
            ->setDescription('Summary rollbar log')
            ->addOption('dbUsername', null, InputOption::VALUE_OPTIONAL, 'username')
            ->addOption('dbPwd', null, InputOption::VALUE_OPTIONAL, 'password')
            ->addOption('dbName', null, InputOption::VALUE_OPTIONAL, 'db name');
    }

    /**
     * Initialize
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->buzz = $this->getContainer()->get('buzz');
    }

    /**
     * Override execute method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbUsername = $input->getOption('dbUsername') ? $input->getOption('dbUsername') : $this->getContainer()->getParameter('database_user');
        $dbPwd = $input->getOption('dbPwd') ? $input->getOption('dbPwd') : $this->getContainer()->getParameter('database_password');
        $dbName = $input->getOption('dbName') ? $input->getOption('dbName') : $this->getContainer()->getParameter('database_name');

        $host = $this->getContainer()->getParameter('database_host');
        $port = $this->getContainer()->getParameter('database_port');
        $port = $port ? $port : 3306;

        $pdo = new \PDO("mysql:host=$host;port=$port", $dbUsername, $dbPwd);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $dbName = "`".str_replace("`","``",$dbName)."`";
        $pdo->query("CREATE DATABASE IF NOT EXISTS $dbName");
        $pdo->query("use $dbName");

        $tableName = "rollbar_log";
        $pdo->query("CREATE TABLE IF NOT EXISTS $tableName (
                      id INT(11) AUTO_INCREMENT PRIMARY KEY, 
                      project_name VARCHAR(30) NOT NULL,
                      critical INT(11) NOT NULL,
                      error INT(11) NOT NULL,
                      warning INT(11) NOT NULL,
                      created_at DATETIME)");

        $stmt = $pdo->prepare("INSERT INTO $tableName (project_name, critical, error, warning, created_at) VALUES (?, ?, ?, ?, NOW())");

        $projects = $this->getRollbarProjectDetails();
        foreach ($projects as $key => $project) {
            if (!isset($project['access_token']) || !$project['access_token']) {
                $output->writeln('Access token not found');
                continue;
            }

            if (!isset($project['env']) || !$project['env']) {
                $output->writeln('Env token not found');
                continue;
            }

            $token = $project['access_token'];
            $env = $project['env'];

            try {
                $responseCritical = $this->buzz->get("$this->apiEndPoint?access_token=$token&level=error&level=critical&environment=$env&status=active");
                $responseError = $this->buzz->get("$this->apiEndPoint?access_token=$token&level=error&level=error&environment=$env&status=active");
                $responseWarning = $this->buzz->get("$this->apiEndPoint?access_token=$token&level=error&level=warning&environment=$env&status=active");
            } catch (\Exception $e) {
                continue;
            }

            $resultCritical = json_decode($responseCritical->getContent(), true);
            $criticalCount = 0;
            if (isset($resultCritical['result']['total_count'])) {
                $criticalCount += $resultCritical['result']['total_count'];
            }

            $resultError = json_decode($responseError->getContent(), true);
            $errorCount = 0;
            if (isset($resultError['result']['total_count'])) {
                $errorCount += $resultError['result']['total_count'];
            }

            $resultWarning = json_decode($responseWarning->getContent(), true);
            $warningCount = 0;
            if (isset($resultWarning['result']['total_count'])) {
                $warningCount += $resultWarning['result']['total_count'];
            }


            $stmt->bindParam(1, $key);
            $stmt->bindParam(2, $criticalCount);
            $stmt->bindParam(3, $errorCount);
            $stmt->bindParam(4, $warningCount);

            $stmt->execute();
        }
    }

    /**
     * @return array
     */
    protected function getRollbarProjectDetails()
    {
        $projects = [
            'bplatform' => [
                'access_token' => '95193efd19f9409d9c3754cc8e205447',
                'env' => 'production_prod',
            ],
            'bridge' => [
                'access_token' => '0a75df5b272341c7b42700ef3e2e719f',
                'env' => 'prod',
            ],
            'tracking' => [
                'access_token' => 'fa26105efcad443a883e4bea2788b59e',
                'env' => 'prod',
            ],
            'woocommerce' => [
                'access_token' => 'e35423fd2e434467a42931c99f6bc693',
                'env' => 'prod',
            ],
        ];

        return $projects;
    }
}