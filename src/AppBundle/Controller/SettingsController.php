<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Dmishh\Bundle\SettingsBundle\Controller\SettingsController as BaseSettingsController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SettingsController extends Controller
{
    /**
     * @Route("/admin/settings", name="settings")
     */
    public function SettingsConfig()
    {
        $number = random_int(0,100);
        // replace this example code with whatever you need
        return $this->render('@AppBundle/Resources/views/Settings/settings.html.twig', array(
            'number' => $number,
        ));
    }



}