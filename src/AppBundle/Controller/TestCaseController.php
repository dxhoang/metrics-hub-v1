<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Dmishh\Bundle\SettingsBundle\Controller\SettingsController as BaseSettingsController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\TestCase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class TestCaseController extends Controller
{
    /**
     * @var EntityManager
     */
    private $em;


    /**
     * @Route("/admin/testcase", name="testcase_default")
     */
    public function TestCase()
    {
        $this->get('session')->remove('status');
        return $this->redirect('/admin/testcase/list');
    }

    /**
     * @Route("/admin/testcase/list", name="testcase")
     */
    public function ListCase()
    {
        $status = $this->get('session')->get('status');
        return $this->render('@AppBundle/Resources/views/TestCase/test_case.html.twig', ['status' => $status]);
    }

    /**
     * @Route("/admin/testcase/addnew", name="addnew_show")
     */
    public function AddNewShow()
    {
        return $this->render('@AppBundle/Resources/views/TestCase/add_new_test_case.html.twig');
    }

    /**
     * @Route("/admin/testcase/edit/{id}", name="testcase_edit")
     */
    public function actionEditCase($id){
        $case = $this->get('test_case_manager');
        $arr = $case->getById($id);
        return $this->render('@AppBundle/Resources/views/TestCase/edit_test_case.html.twig',['casename' => $arr[0]['CaseName'], 'caseid' => $id , 'link' => $arr[0]['Link'],'testitems' => $arr[0]['TestItems'] , 'full' => $arr]);
    }


    /**
     * @Route("/admin/testcase/view/{id}", name="testcase_view")
     */
    public function actionViewCase($id){
        $case = $this->get('test_case_manager');
        $arr = $case->getById($id);
        return $this->render('@AppBundle/Resources/views/TestCase/view_test_case.html.twig',['casename' => $arr[0]['CaseName'],'link' => $arr[0]['Link'],'testitems' => $arr[0]['TestItems'], 'caseid' => $id ,'full' => $arr]);
    }



    /***** API ******/

    /**
     * @Route("/api/testcase/addnew", methods={"POST","GET"})
     *
     */
    public function AddNewTestCase(Request $request){
        $testcase = new TestCase();
        $saveCase = $this->get('test_case_manager');
        if($this->getRequest()->isMethod('POST')) {
            $data = json_decode($request->getContent(), true);

            //$this->get('session')->set('status', 'success');
            return new JsonResponse($data);
        }else{
            return $this->redirect('/admin/testcase/list');
        }
    }

    /**
     * @Route("/api/testcase/checkName", methods={"POST","GET"})
     */
    public function CheckCaseName(Request $request){
        $case = $this->get('test_case_manager');
        if($this->getRequest()->isMethod('POST')) {
            $data = $request->getContent();
            $arr = $case->getByCaseName($data);
            return new JsonResponse($arr);
        } else {
            return $this->redirect('/admin/testcase/list');
        }
    }

    /**
     * @Route("/api/testcase/removeAt", methods={"POST","GET"})
     */
    public function RemoveCaseName(Request $request){
        $case = $this->get('test_case_manager');
        if($this->getRequest()->isMethod('POST')) {
            $data = $request->getContent();
            $remove = $case->removeAction($data);
            return new JsonResponse($remove);
        } else {
            return $this->redirect('/admin/testcase/list');
        }
    }


    /**
     * @Route("/api/testcase/showList", methods={"POST","GET"})
     */
    public function showList(){
        if($this->getRequest()->isMethod('POST')) {
            $case = $this->get('test_case_manager');
            return new JsonResponse($case->getAll());
        } else {
            return $this->redirect('/admin/testcase/list');
        }
    }



}