<?php
/**
 * (C) Beeketing, Inc.
 * Quan Truong <quan@beeketing.com>
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Funnel;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FunnelAdminController extends CRUDController
{
    /**
     * @param ProxyQueryInterface $query
     * @return RedirectResponse
     */
    public function batchActionReset(ProxyQueryInterface $query)
    {
        // Write code to reset data here
        /** @var Funnel $funnel */
        foreach ($query->execute() as $funnel) {
            $sql = 'DELETE FROM funnel_result WHERE funnel_id = ' . $funnel->getId();
            $this->container->get('doctrine.orm.default_entity_manager')->getConnection()->exec($sql);
        }

        $this->container->get('session')->getFlashBag()->set('success', 'Funnels data is reset.');

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    /**
     * Duplicate an object
     * @param $id
     * @return RedirectResponse
     */
    public function duplicateAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        // Be careful, you may need to overload the __clone method of your object
        // to set its id to null !
        $clonedObject = clone $object;

        $clonedObject->setName($object->getName().' (Copy)');
        $clonedObject->setUpdatedAt(new \DateTime());

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Duplicated successfully');

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

}