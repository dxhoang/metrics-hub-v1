<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/board")
 */
class BoardController extends AdminController
{

    /**
     * @Route("/view/{id}/{refresh}", name="board_view")
     * @param $id
     * @param bool $refresh
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function viewAction($id, $refresh = false)
    {
        $boardManager = $this->get('board_manager');
        $board = $boardManager->get($id);

        if (!$board) {
            throw new NotFoundHttpException();
        }

        // If refresh
        if ($refresh) {
            $boardManager->refresh($board);
            $this->container->get('session')->getFlashBag()->add('success', 'Queued to refresh board');

            return $this->redirect($this->generateUrl('board_view', array(
                'id' => $id,
                'refresh' => false,
            )));
        }

        return $this->render('AppBundle:Board:view.html.twig', array(
            'board' => $board,
        ));
    }


    /**
     * View screen of public boards
     * @Route("/screen", name="board_screen")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function screenAction()
    {
        $boards = $this->container->get('board_manager')->getScreenBoards();

        return $this->render('@App/Board/screen.html.twig', array(
            'boards' => $boards,
        ));
    }

}