<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{

    public function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters['admin_pool'] = $this->get('sonata.admin.pool');
        return parent::render($view, $parameters, $response);
    }

}