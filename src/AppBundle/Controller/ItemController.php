<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/item")
 */
class ItemController extends Controller
{

    /**
     * @Route("/show/{id}", name="item_show")
     */
    public function showAction($id)
    {
        $item = $this->get('item_manager')->get($id);

        if (!$item) {
            throw new NotFoundHttpException();
        }

        // Redirect to source of this is mode analytics
        if ($item->getType() == 'modeanalytics') {
            return $this->redirect($item->getUrl());
        }

        // Mixpanel
        return $this->render('@App/Item/show.html.twig', [
            'item' => $item,
            'refreshUrl' => $this->generateUrl('item_refresh', [
                'id' => $id,
            ])
        ]);
    }

    /**
     * Check if an item is refreshed
     * @Route("/refresh/{id}", name="item_refresh")
     */
    public function refreshAction($id)
    {
        $itemManager = $this->get('item_manager');

        $item = $itemManager->get($id);

        if (!$item) {
            throw new NotFoundHttpException();
        }

        // Return success if item is refreshed
        if ($item->getRefresh() == false) {
            return new JsonResponse([
                'success' => true,
                'id' => $item->getId(),
            ]);
        }

        return new JsonResponse([
            'success' => false,
        ]);
    }

}