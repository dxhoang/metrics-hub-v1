<?php
/**
 * (C) Beeketing, Inc.
 * Quan Truong <quan@beeketing.com>
 */

namespace AppBundle\Admin;

class NeedTagProductAdmin extends ProductAdmin
{

    protected $baseRouteName = 'need_tag_product';
    protected $baseRoutePattern = 'need_tag_product';

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->orX(
                $query->getRootAliases()[0] . '.customTags IS NULL',
                $query->expr()->eq($query->getRootAliases()[0] . '.customTags', ':empty')
            )
        );

        $query->setParameter('empty', '');

        return $query;
    }

}