<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ScheduleAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Schedule')
                ->add('board')
                ->add('type', 'choice', array(
                    'choices' => array(
                        'update' => 'Update',
                        'notify' => 'Notify (only support product service status boards)',
                    ),
                ))
                ->add('cycle', 'choice', array(
                    'choices' => array(
                        'daily' => 'Daily',
                        'weekly' => 'Weekly',
                        'monthly' => 'Monthly',
                        'yearly' => 'Yearly',
                    ),
                ))
                ->add('time', 'sonata_type_datetime_picker')
            ->end()
            ->with('Notify')
                ->add('notifySlacks', 'text', array(
                    'label' => 'Notify to slack usernames (comma-separated)',
                    'required' => false,
                ))
                ->add('mentionUsers', null, array(
                    'by_reference' => false,
                ))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('board')
            ->add('type')
            ->add('cycle')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('type')
            ->add('cycle')
            ->add('time')
            ->add('notifySlacks')
            ->add('board')
        ;
    }

}