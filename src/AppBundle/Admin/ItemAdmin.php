<?php
/**
 * Copyright Beeketing Pte Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ItemAdmin extends Admin
{

    private $typeChoices = [
        'mixpanel' => 'Mixpanel',
        'modeanalytics' => 'ModeAnalytics',
    ];

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('board')
            ->add('type', 'choice', [
                'choices' => $this->typeChoices,
            ])
            ->add('url')
            ->add('sort')
            ->add('size', 'number', [
                'label' => 'Size (x/12)',
            ])
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('board')
            ->add('type')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('board')
            ->add('type')
            ->add('url')
            ->add('sort')
        ;
    }

}