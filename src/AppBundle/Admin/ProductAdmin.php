<?php
/**
 * (C) Beeketing, Inc.
 * Quan Truong <quan@beeketing.com>
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'product';
    protected $baseRoutePattern = 'product';
    protected $maxPerPage = 128;
    protected $perPageOptions = array(64, 128);
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 64,
        '_sort_order' => 'DESC',
        '_sort_by' => 'salesCount',
    );

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('imgUrl')
            ->add('productRefId')
            ->add('shopId')
            ->add('price')
            ->add('salesCount')
            ->add('tags')
            ->add('customTags')
            ->add('url')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('productRefId')
            ->add('shopId')
            ->add('price')
            ->add('salesCount', null, [
                'show_filter' => true,
            ])
            ->add('tags', null, array(
                'show_filter' => true,
            ))
            ->add('customTags', null, array(
                'show_filter' => true,
            ))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name')
            ->add('image', null, [
                'template' => 'AppBundle:Admin:product_image.html.twig'
            ])
            ->add('info', null, [
                'template' => 'AppBundle:Admin:product_info.html.twig'
            ])
            ->add('salesCount')
            ->add('price')
            ->add('tags')
            ->add('customTags', 'text', [
                'editable' => true,
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                ],
            ])
        ;
    }

}