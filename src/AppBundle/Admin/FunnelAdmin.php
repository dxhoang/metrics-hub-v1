<?php
/**
 * Created by IntelliJ IDEA.
 * User: ducnt114
 * Date: 3/27/17
 * Time: 9:22 AM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FunnelAdmin extends Admin
{

    protected $datagridValues = [

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'updatedAt',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('duplicate', $this->getRouterIdParameter().'/duplicate');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('description', null, [
                'required' => false,
            ])
            ->add('content')
            ->add('active')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('content')
            ->add('active')
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name')
            ->add('description')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(
                        'template' => 'AppBundle:Admin:funnel_actions.html.twig'
                    )
                )
            ))
            ->add('active')
            ->add('content')
            ->add('updatedAt')
            ->add('createdAt')
        ;
    }

    public function getTemplate($name)
    {
        if ($name === 'edit') {
            return 'AppBundle:Funnel:edit.html.twig';
        }

        return parent::getTemplate($name);
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        $actions['reset'] = [
            'label' => 'Reset Data',
            'ask_confirmation' => true,
        ];

        return $actions;
    }
}