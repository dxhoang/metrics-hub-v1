<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 4/21/16
 * Time: 1:29 AM
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BoardAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('user')
            ->add('inScreen')
            ->add('sort', null, array(
                'help' => 'Smaller show first',
            ))
            ->add('iframe', null, array(
                'label' => 'Is this board to show iFrame?',
            ))
            ->add('frameUrl')
            ->add('serviceStatus', null, array(
                'label' => 'Is this board to show service status?',
            ))
            ->add('services')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('user')
            ->add('inScreen')
            ->add('iframe')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('user')
            ->add('inScreen')
            ->add('iframe')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', 'actions', array(
                'actions' => array(

                    'view' => array(
                        'template' => 'AppBundle:Admin:board_actions.html.twig'
                    )
                )
            ))
        ;
    }

}