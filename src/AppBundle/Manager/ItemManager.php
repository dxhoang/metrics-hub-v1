<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Board;
use AppBundle\Entity\Item;
use AppBundle\Util\CaptureMixpanel;
use AppBundle\Util\CommandEcho;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ItemManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CaptureMixpanel
     */
    private $captureMixpanel;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ItemManager constructor.
     * @param ContainerInterface $container
     * @param EntityManager $em
     * @param CaptureMixpanel $captureMixpanel
     */
    public function __construct(ContainerInterface $container, EntityManager $em, CaptureMixpanel $captureMixpanel)
    {
        $this->container = $container;
        $this->em = $em;
        $this->captureMixpanel = $captureMixpanel;
    }

    /**
     * Get repository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('AppBundle:Item');
    }

    /**
     * Get item by id
     * @param $id
     * @return null|Item
     */
    public function get($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * Get items by board
     * @param Board $board
     * @return mixed
     */
    public function getByBoard(Board $board)
    {
        return $this->getRepository()->createQueryBuilder('i')
            ->where('i.board = :board')
            ->orderBy('i.sort', 'ASC')
            ->getQuery()
            ->setParameter('board', $board)
            ->execute();
    }

    /**
     * Set an item need to refresh to false
     * @param Item $item
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUpdated(Item $item)
    {
        $item->setRefresh(false);
        $item->setRefreshedAt(new \DateTime());

        $this->em->persist($item);
        $this->em->flush();
    }

    /**
     * Set an item
     * @param Item $item
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Item $item)
    {
        $this->em->persist($item);
        $this->em->flush();
    }

    /**
     *
     * @param Item $item
     * @return boolean
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    public function refresh(Item $item)
    {
        CommandEcho::writeln(sprintf('Type: %s', $item->getType()));
        if ($item->getType() != 'mixpanel' || $this->captureMixpanel->capture($item->getUrl(), $item->getId())) {
            $this->setUpdated($item);

            return true;
        }

        return false;
    }

    /**
     * Queue rabbitmq to refresh content
     * @param $item
     */
    public function queueRefresh(Item $item)
    {
        $this->container->get('old_sound_rabbit_mq.capture_image_producer')->publish(json_encode(array(
            'item_id' => $item->getId(),
        )));
    }

    /**
     * Get need to refresh items
     * @param $minutes
     * @param null $type
     * @return mixed
     */
    public function getNeedRefresh($minutes, $type = null)
    {
        $refreshTime = new \DateTime();
        $refreshTime->modify(sprintf('-%d minutes', $minutes));

        $qb = $this->getRepository()
            ->createQueryBuilder('i')
            ->where('(i.refreshedAt <= :refreshTime OR i.refresh = :refresh)')
            ->setParameter('refreshTime', $refreshTime)
            ->setParameter('refresh', true)
        ;

        if ($type) {
            $qb->andWhere('i.type = :type')
                ->setParameter('type', $type)
            ;
        }

        return $qb->getQuery()
            ->execute();
    }

}
