<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Board;
use AppBundle\Entity\Item;
use Doctrine\ORM\EntityManager;

class BoardManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * BoardManager constructor.
     * @param EntityManager $em
     * @param ItemManager $itemManager
     */
    public function __construct(EntityManager $em, ItemManager $itemManager)
    {
        $this->em = $em;
        $this->itemManager = $itemManager;
    }

    /**
     * Get repository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('AppBundle:Board');
    }

    /**
     * Get board by id
     * @param $id
     * @return null|Board
     */
    public function get($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * Refresh a board
     * @param Board $board
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function refresh(Board $board)
    {
        $items = $board->getItems();
        /** @var Item $item */
        foreach ($items as $item) {
            $item->setRefresh(true);
            $this->em->persist($item);
            $this->em->flush();

            // Queue to refresh
            $this->itemManager->queueRefresh($item);
        }

        $board->setUpdatedAt(new \DateTime());
        $this->em->persist($board);

        $this->em->flush();
    }

    /**
     * Get boards in screen
     * @return array
     */
    public function getScreenBoards()
    {
        return $this->getRepository()
            ->createQueryBuilder('b')
            ->where('b.inScreen = true')
            ->orderBy('b.sort', 'ASC')
            ->getQuery()
            ->execute();
    }

}