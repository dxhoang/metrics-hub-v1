<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Manager;


use AppBundle\Entity\TestCase;
use AppBundle\Entity\TestCaseStep;
use Doctrine\ORM\EntityManager;

class TestCaseManager
{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * TestCaseManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get repository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('AppBundle:TestCase');
    }

    public function getByCaseName($casename)
    {
        $queryBuilder = $this->em->getConnection()->createQueryBuilder();
        $arr = $queryBuilder
            ->select( 'tc.*')
            ->from('test_case', 'tc')
            ->where('CaseName = ?')
            ->setParameter(0, $casename)
        ;
        return $arr->execute()->fetchAll();
    }

    public function getById($id)
    {
        $queryBuilder = $this->em->getConnection()->createQueryBuilder();
        $arr = $queryBuilder
            ->select( 'tc.*')
            ->from('test_case', 'tc')
            ->where('CaseId = ?')
            ->setParameter(0, $id)
        ;
        return $arr->execute()->fetchAll();
    }

    public function getAll()
    {
        $queryBuilder = $this->em->getConnection()->createQueryBuilder();
        $arr = $queryBuilder
            ->select( 'tc.*')
            ->from('test_case', 'tc')
        ;
        return $arr->execute()->fetchAll();
    }

    public function removeAction($id){
        $queryBuilder = $this->em->getConnection()->createQueryBuilder();
        $rev = $queryBuilder->delete('test_case')
            ->where('CaseId = ?')
            ->setParameter(0, $id);

        return $rev->execute();
    }


    /**
     * Set an testcase
     * @param TestCase $testcase
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TestCase $testcase)
    {
        $this->em->persist($testcase);
        $this->em->flush();
    }


    /**
     * Set an testcasestep
     * @param TestCaseStep $testcasestep
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveStep(TestCaseStep $testcasestep)
    {
        $this->em->persist($testcasestep);
        $this->em->flush();
    }
}
