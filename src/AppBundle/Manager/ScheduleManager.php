<?php
/**
 * Copyright Beeketing Pte. Ltd.
 * (c) Quan MT <quan@beeketing.com>
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Schedule;
use Doctrine\ORM\EntityManager;

class ScheduleManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ScheduleManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get repo
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('AppBundle:Schedule');
    }

    /**
     * Get schedules for runner
     * @param \DateTime $currentTime
     * @param null $type
     * @return mixed
     */
    public function getForRunner(\DateTime $currentTime, $type = null)
    {
        $qb = $this->getRepository()->createQueryBuilder('s')
            ->where('s.nextUpdateTime <= :currentTime OR s.nextUpdateTime is NULL');

        if ($type) {
            $qb->andWhere('s.type = :type')
                ->setParameter('type', $type)
            ;
        }

        $query = $qb->getQuery();

        return $query->setParameter('currentTime', $currentTime)
            ->execute();
    }

    /**
     * Save
     * @param Schedule $schedule
     * @param bool $flush
     */
    public function save(Schedule $schedule, $flush = true)
    {
        $this->em->persist($schedule);

        if ($flush) {
            $this->flush();
        }
    }

    /**
     * Flush
     */
    public function flush()
    {
        $this->em->flush();
    }

}