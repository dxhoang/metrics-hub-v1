<?php
/**
 * User: quaninte
 */

namespace OKRBundle\Twig;

use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\UserManagerInterface;
use OKRBundle\Entity\Objective;
use OKRBundle\Manager\KeyResultManager;

class OKRHelperExtension extends \Twig_Extension
{

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var KeyResultManager
     */
    private $keyResultManager;

    /**
     * OKRHelperExtension constructor.
     * @param UserManagerInterface $userManager
     * @param KeyResultManager $keyResultManager
     */
    public function __construct(UserManagerInterface $userManager, KeyResultManager $keyResultManager)
    {
        $this->userManager = $userManager;
        $this->keyResultManager = $keyResultManager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_user_list', array($this, 'getUserList')),
        );
    }

    public function getUserList()
    {
        return $this->userManager->findUsers();
    }
}