<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 7/15/18
 * Time: 4:43 PM
 */

namespace OKRBundle\Command;


use OKRBundle\Entity\Objective;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteUnchangedObjectivesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('delete-unchanged-objectives');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('okr.objective_manager')->deleteUnchanged();
    }

}