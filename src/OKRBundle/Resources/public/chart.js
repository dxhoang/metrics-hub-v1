function renderChart(data, yFrom, yTo, yLegend) {
    // Build y range to make sure we have a chart
    findArr = data.values;
    findArr.push(yFrom);
    findArr.push(yTo);

    yFrom = arrayMin(findArr);
    yTo = arrayMax(findArr);

    // render chart
    var ctx = $("#obj-chart");
    var objChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data.labels,
            datasets: [{
                label: yLegend,
                data: data.values,
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            plugins: {
                datalabels: {
                    color: 'black',
                    backgroundColor: 'white',
                    borderRadius: 10,
                    borderColor: 'black',
                    borderWidth: 1.5,
                    font: {
                        weight: 'bold',
                        size: 14
                    },
                    anchor: 'top'
                }
            },
            legend: {
                display: true
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'linear',
                    ticks: {
                        source: 'labels'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yLegend
                    },
                    ticks: {
                        min: yFrom,
                        max: yTo
                    }
                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 0
                }
            }
        }
    });
}

function arrayMin(arr) {
    var len = arr.length, min = Infinity;
    while (len--) {
        if (arr[len] < min) {
            min = arr[len];
        }
    }
    return min;
}

function arrayMax(arr) {
    var len = arr.length, max = -Infinity;
    while (len--) {
        if (arr[len] > max) {
            max = arr[len];
        }
    }
    return max;
}

$(function() {
    // Prepare data
    dateFormat = 'YYYY-MM-DD';
    for (var i = 0; i < chartData.length; i++) {
        $(chartData[i].labels).each(function(key, value) {
            chartData[i].labels[key] = moment(value, dateFormat);
        });
    }

    // Don't show chart if there are no data
    if (chartData.length == 0) {
        console.log('No chart data');
        return;
    }

    renderChart(chartData[0], yFrom[0], yTo[0], yLegend[0]);

    // Add primary to first button
    $('.switch-chart-button:first').removeClass('btn-default').addClass('btn-primary');

    // Update chart when switch
    $('.switch-chart-button').click(function () {
        $('.switch-chart-button').not(this).removeClass('btn-primary').addClass('btn-default');
        $(this).removeClass('btn-default').addClass('btn-primary');

        var index = $(this).data('chart-index');
        renderChart(chartData[index], yFrom[index], yTo[index], yLegend[index]);
    });
});