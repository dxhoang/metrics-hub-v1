var holderSelector = '.sortable';

// Get url to send request
function getUrl(uri) {
    return baseRequestUrl + uri;
}

// Zoom state in cookie so we can reload and plan
function getCookieKey() {
    return 'zs_' + objectType + objectId;
}

function getZoomState() {
    var state = Cookies.getJSON(getCookieKey());
    if (typeof state === 'undefined') {
        state = [];
    }

    return state;
}

function saveZoomState(type, objId) {
    objId = parseInt(objId);

    var state = getZoomState();

    if (type === 'in' && $.inArray(objId, state) !== -1) {
        state.splice( $.inArray(objId, state), 1 );
    } else if ($.inArray(objId, state) === -1) {
        state.push(objId);
    }

    Cookies.set(getCookieKey(), state, { expires: 365 });
}

function zoomObjective(type, objId) {
    var holder = $('#Obj_' + objId);
    var zoomButton = holder.find('.objective-wrapper:first .zoom-button');

    if (type === 'out') {
        holder.addClass('zoomed-out');
        zoomButton.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        holder.removeClass('zoomed-out');
        zoomButton.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }

    // Save zoom state
    saveZoomState(type, objId);
}

// Init elements after changes
function initElements() {
    $('.add-object-button').unbind('click').click(function(e) {
        // Create a new objective via ajax
        var clickedButton = this;
        $.getJSON(getUrl('/okr/objective/create/' + goalId), function (response) {
            if (response.success) {
                // If this is inside objective-holder
                var holder = $(clickedButton).closest('.objective-holder');

                // Mark to save position
                var newItem = $(response.html);
                newItem.data('changed-position', 1);

                if (holder.size()) {
                    // Add right next
                    holder.after(newItem);
                } else {
                    // Append last
                    $('#object-list-holder').append(newItem);
                }

                // Need to bind again
                initElements();
            } else {
                alert(response.error);
            }
        });

        e.preventDefault();
    });

    $('.zoom-button').unbind('click').click(function(e) {
        var holder = $(this).closest('.objective-holder');
        var type;
        if ($(this).hasClass('glyphicon-minus')) {
            // zooming out
            type = 'out';
        } else {
            // zooming in
            type = 'in';
        }
        zoomObjective(type, holder.data('id'));

        // Update odd class of rows
        addOddToObjWrapper();

        e.preventDefault();
    });

    $('#object-list-holder select:visible').select2({
        width: 'resolve',
        minimumResultsForSearch: 10,
        allowClear: true
    });

    // Init nested sortable
    $(holderSelector).nestedSortable({
        items: 'li',
        toleranceElement: '> div',
        cancel: '.contenteditable',
        placeholder: "sortable-placeholder",
        handle: '.move-button',
        helper: function() {
            return '<div class="btn btn-default glyphicon glyphicon-move drag-helper"></div>';
        },
        relocate: function(e, ui) {
            // Need to bind again
            initElements();

            // Add changed position to save
            $(ui.item).data('changed-position', 1);
            warnBeforeExitPage();
        }
    });

    // Delete objective
    $('.delete-button').unbind('click').click(function(e) {
        if (confirm('Are you sure?')) {
            var holder = $(this).closest('.objective-holder');
            holder.attr('data-delete', 1);
            holder.find('.objective-holder').attr('data-delete', 1);
            holder.hide();
        }

        e.preventDefault();
    });

    // Edit objective
    $('.edit-objective-button').unbind('click').click(function(e) {
        var url = $(this).attr('href');
        var objectiveId = $(this).closest('.objective-holder').data('id');

        $.get(url, function(data) {
            var modalBody = $('#edit-modal .modal-body');
            modalBody.html(data);
            $('#edit-modal .modal-title').html('Edit Objective');
            $('#edit-modal').modal('show');

            bindAjaxModalForm(modalBody, objectiveId);
        });

        e.preventDefault();
    });

    // Edit key result
    $('.objective-holder a.progress, .modal-link').unbind('click').click(function(e) {
        var url = $(this).attr('href');
        var objectiveId = $(this).closest('.objective-holder').data('id');

        $.get(url, function(data) {
            var modalBody = $('#edit-modal .modal-body');
            modalBody.html(data);
            $('#edit-modal .modal-title').html('Edit Key Result');
            $('#edit-modal').modal('show');

            bindAjaxModalForm(modalBody, objectiveId);
        });

        e.preventDefault();
    });

    // Remember zoom state (default all zoom in)
    var state = getZoomState();
    for (var i = 0; i < state.length; i++) {
        zoomObjective('out', state[i]);
    }

    // Change background color to be odd
    addOddToObjWrapper();

    // Watch and add changed state of objectives
    watchAndAddChangedState();

    // Toggle height of objective wrapper to show all KR
    $('.toggle-expanded').unbind('click').click(function (e) {
        $(this).parents('.objective-wrapper:first').toggleClass('expanded');

        e.preventDefault();
    });
}

function addOddToObjWrapper() {
    $('.objective-wrapper').removeClass('odd');
    $('.objective-wrapper:visible:odd').addClass('odd');
}

// Update content of objective
function updateContentOfObjective(objectiveId) {
    $.getJSON(getUrl('/okr/objective/' + objectiveId + '/' + objectType), function (response) {
        if (response.success) {
            // If this is inside objective-holder
            var holder = $('.objective-holder[data-id="' + objectiveId + '"]');
            if (holder.size()) {
                // Replace content inside
                holder.find('.objective-wrapper:first').html($(response.html).find('div:first').html());
            } else {
                alert('Cant find objective holder to update');
            }

            // Need to bind again
            initElements();
        } else {
            alert(response.error);
        }
    });
}

// Success submit edit objective form
function succesSubmitEditObjectiveForm(objectiveId) {
    return function() {
        // success submit
        $('#edit-modal').modal('hide');

        // Update content of objective
        updateContentOfObjective(objectiveId);

        // If objective has parent, update parent as well
        var holder = $('.objective-holder[data-id="' + objectiveId + '"]');
        var parent = holder.parents('.objective-holder:first');

        if (parent.size()) {
            updateContentOfObjective(parent.data('id'));
        }
    };
}

// Bind modal to handle success submit
function bindAjaxModalForm(modalBody, objectiveId) {
    bindAjaxForm(modalBody, succesSubmitEditObjectiveForm(objectiveId), function() {
        bindAjaxModalForm(modalBody);
    });
}

// Bind ajax form to work as ajax
function bindAjaxForm(content, successCallback, failureCallback) {
    $('form:first', content).submit(function() {
        $.ajax({
            url     : $(this).attr('action'),
            type    : $(this).attr('method'),
            data    : $(this).serialize(),
            success: function(response, status, xhr){
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct.indexOf('html') > -1) {
                    $(content).html(response);
                    failureCallback();
                } else if (ct.indexOf('json') > -1) {
                    if (response.result === 'ok') {
                        successCallback();
                    }
                }
            }
        });
        return false;
    });
}

// Get data to save
function getGoalData () {
    var array = $(holderSelector).nestedSortable('toArray');

    for (var i = 0; i < array.length; i++) {
        var obj = array[i];

        // skip if item_id is null
        if (!obj.id) {
            continue;
        }

        // Find obj dom
        var objHolder = $('#Obj_' + obj.id);

        // Add values
        obj.name = $('[data-field="name"]:first', objHolder).text().trim();
        obj.assignee_id = parseInt($('[data-field="assignee"]:first', objHolder).val());
        obj.confident = parseFloat($('[data-field="confident"]:first', objHolder).text());

        // Add changed states
        obj.changed_name = ($('[data-field="name"]:first', objHolder).data('changed') == 1 ? 1 : 0);
        obj.changed_assignee_id = ($('[data-field="assignee"]:first', objHolder).data('changed') == 1 ? 1 : 0);
        obj.changed_confident = ($('[data-field="confident"]:first', objHolder).data('changed') == 1 ? 1 : 0);
        obj.changed_position= ($(objHolder).data('changed-position') == 1 ? 1 : 0);

        // Reset all changed data state, so we won't over save
        $('[data-field="name"]:first', objHolder).data('changed', 0);
        $('[data-field="assignee"]:first', objHolder).data('changed', 0);
        $('[data-field="confident"]:first', objHolder).data('changed', 0);
        $(objHolder).data('changed-position', 0);

        // Transform types
        obj.id = parseInt(obj.id);
        obj.parent_id = parseInt(obj.parent_id);

        // Delete?
        obj.delete = parseInt(objHolder.data('delete'));

        // reassign
        array[i] = obj;
    }

    return array;
}

// Warn before exit form
function warnBeforeExitPage() {
    window.onbeforeunload = function() {
        return 'Do you want to leave this page without saving?';
    };
}
function cancelWarnBeforeExitPage() {
    window.onbeforeunload = null;
}

function watchAndAddChangedState() {
    var contentEditableChangeEvents = 'blur keyup paste input';
    // Name & confident
    $('[data-field="name"], [data-field="confident"]').unbind(contentEditableChangeEvents).on(contentEditableChangeEvents, function() {
        $(this).data('changed', 1);
        warnBeforeExitPage();
    });

    // assignee
    $('[data-field="assignee"]:not(.bind-changed-state)').addClass('bind-changed-state').on('change', function () {
        $(this).data('changed', 1);
        warnBeforeExitPage();
    });
}

$(document).ready(function(){
    // Init elements
    initElements();

    // Save when click
    $('.save-goal-button').click(function (e) {
        var btn = this;
        var text = $(btn).text();
        $(btn).text('Wait...');

        // Send request to save data
        $.ajax({
            type: 'POST',
            url: getUrl('/okr/objective/save-goal/' + goalId + '/' + objectType),
            data: JSON.stringify(getGoalData()),
            success: function(data) {
                console.log('saved');
                $(btn).text(text);

                // Don't warn after save
                cancelWarnBeforeExitPage();
            },
            contentType: "application/json",
            dataType: 'json'
        });

        e.preventDefault();
    });

    // Show loading in ajax requests
    var $loading = $('.loading-holder').hide();
    $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });

    // Close sidebar by default
    $('body').addClass('sidebar-collapse');

    // Hide main add obj if there are already obj existed
    if ($('.objective-holder').size()) {
        $('.add-object-button.main').hide();
    }
});