<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 7/18/18
 * Time: 12:00 AM
 */

namespace OKRBundle\Util;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;

class DesignAdminMenuBuilder
{

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * DesignAdminMenuBuilder constructor.
     * @param Router $router
     * @param SecurityContext $securityContext
     */
    public function __construct(Router $router, SecurityContext $securityContext)
    {
        $this->router = $router;
        $this->securityContext = $securityContext;
    }

    /**
     * @param MenuItemInterface $menu
     * @param $action
     * @param $goalId
     */
    public function configureTabMenu(MenuItemInterface $menu, $action, $goalId)
    {
        if ($action != 'show') {
            return;
        }

        $menu->addChild('My Objectives', [
            'uri' => $this->router->generate('user_objectives', [
                'goalId' => $goalId,
                'userId' => $this->securityContext->getToken()->getUser()->getId(),
            ]),
            'attributes' => array(
                'class' => 'btn',
            ),
            'linkAttributes' => array(
                'class' => 'my-objectives-button',
                'target' => '_blank',
            ),
        ]);
        $menu->addChild('Add Objective', [
            'uri' => '#',
            'attributes' => array(
                'class' => 'btn add-object-button main',
            ),
        ]);
        $menu->addChild('Save', [
            'uri' => '#',
            'attributes' => array(
                'class' => 'btn btn-primary',
            ),
            'linkAttributes' => array(
                'class' => 'save-goal-button',
                'style' => 'color: white',
            ),
        ]);
    }

}