<?php
/**
 * User: quaninte
 */

namespace OKRBundle\Admin;


use Doctrine\ORM\EntityManagerInterface;
use OKRBundle\Entity\KeyResult;
use OKRBundle\Entity\Objective;
use OKRBundle\Manager\KeyResultManager;
use OKRBundle\Util\DesignAdminMenuBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;

class ObjectiveAdmin extends AbstractAdmin
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var DesignAdminMenuBuilder
     */
    private $damb;

    /**
     * @var KeyResultManager
     */
    private $keyResultManager;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * ObjectiveAdmin constructor.
     * @param $code
     * @param $class
     * @param $baseControllerName
     * @param EntityManagerInterface $em
     * @param DesignAdminMenuBuilder $damb
     * @param KeyResultManager $keyResultManager
     * @param TwigEngine $templating
     */
    public function __construct($code, $class, $baseControllerName, EntityManagerInterface $em,
                                DesignAdminMenuBuilder $damb, KeyResultManager $keyResultManager, TwigEngine $templating)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em = $em;
        $this->damb = $damb;
        $this->keyResultManager = $keyResultManager;
        $this->templating = $templating;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('goal')
            ->add('parent')
            ->add('notes')
            ->add('keyResults', 'sonata_type_collection', array(
                'by_reference' => true,
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
            ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('goal')
            ->add('assignee')
            ->add('confident')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('goal', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('parent', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('assignee')
            ->add('confident')
            ->add('progress')
            ->add('keyResults')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Build content for key results
        $this->subject->setFullKeyResultsHtml($this->templating->render('@OKR/Admin/KeyResult/children_key_results.html.twig', [
            'keyResults' => $this->subject->getFullKeyResults(),
            'objective' => $this->subject,
        ]));

        $showMapper
            ->add('name')
            ->add('goal', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('parent', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('notes')
            ->add('progress')
            ->add('fullKeyResultsHtml', 'html', [
                'label' => 'Key Results',
            ])
            ->end()
        ;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($this->getSubject()) {
            $this->damb->configureTabMenu($menu, $action, $this->getSubject()->getGoal()->getId());
        }
    }

    /**
     * @param Objective $object
     */
    public function preUpdate($object)
    {
        /** @var KeyResult $keyResult */
        foreach ($object->getKeyResults() as $keyResult) {
            $keyResult->setObjective($object);
            $this->em->persist($keyResult);
        }
    }

}