<?php
/**
 * User: quaninte
 */

namespace OKRBundle\Admin;


use OKRBundle\Util\DesignAdminMenuBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class GoalAdmin extends AbstractAdmin
{

    /**
     * @var DesignAdminMenuBuilder
     */
    private $damb;

    public function __construct($code, $class, $baseControllerName, DesignAdminMenuBuilder $damb)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->damb = $damb;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('description')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('description')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('description')
            ->end()
        ;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($this->getSubject()) {
            $this->damb->configureTabMenu($menu, $action, $this->getSubject()->getId());
        }
    }

}