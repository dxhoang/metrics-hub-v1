<?php
/**
 * User: quaninte
 */

namespace OKRBundle\Admin;


use Doctrine\ORM\EntityManagerInterface;
use OKRBundle\Entity\KeyResult;
use OKRBundle\Entity\ResultUpdate;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class KeyResultAdmin extends AbstractAdmin
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct($code, $class, $baseControllerName, EntityManagerInterface $em)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->em = $em;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('from', null, array(
                'required' => false,
            ))
            ->add('to', null, array(
                'required' => false,
            ))
            ->add('unit')
            ->add('showInParentObjective')
        ;

        if ($this->getRoot()->getClass() == 'OKRBundle\Entity\KeyResult') {
            $formMapper
                ->add('objective')
                ->add('resultUpdates', 'sonata_type_collection', array(
                    'by_reference' => true,
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ;
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('unit')
            ->add('showInParentObjective')
            ->add('objective')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name')
            ->add('from')
            ->add('to')
            ->add('unit')
            ->add('objective')
            ->add('lastResult')
            ->add('showInParentObjective')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('objective', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('from')
            ->add('to')
            ->add('unit')
            ->add('progress')
            ->add('showInParentObjective')
            ->add('resultUpdates')
            ->end()
        ;
    }

    /**
     * @param KeyResult $object
     */
    public function preUpdate($object)
    {
        /** @var ResultUpdate $resultUpdate */
        foreach ($object->getResultUpdates() as $resultUpdate) {
            $resultUpdate->setKeyResult($object);
            $this->em->persist($resultUpdate);
        }

        $keyResult = $object;

        $keyResult->updateLastResult();
        $this->em->persist($keyResult);
    }

}