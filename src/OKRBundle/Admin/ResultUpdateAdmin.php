<?php
/**
 * User: quaninte
 */

namespace OKRBundle\Admin;


use Doctrine\ORM\EntityManagerInterface;
use OKRBundle\Entity\KeyResult;
use OKRBundle\Entity\ResultUpdate;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;

class ResultUpdateAdmin extends AbstractAdmin
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct($code, $class, $baseControllerName, EntityManagerInterface $em)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->em = $em;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('date', DatePickerType::class, array(
                'format' => 'yyyy-MM-dd',
                )
            )
            ->add('value')
        ;

        if ($this->getRoot()->getClass() == 'OKRBundle\Entity\ResultUpdate') {
            $formMapper->add('keyResult');
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('date')
        ;
    }

    public function postRemove($object)
    {
        /** @var KeyResult $keyResult */
        $keyResult = $object->getKeyResult();

        $keyResult->updateLastResult();
        $this->em->persist($keyResult);
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('date')
            ->add('value')
            ->add('keyResult')
            ->add('keyResult.objective')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }


}