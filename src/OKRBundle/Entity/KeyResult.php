<?php

namespace OKRBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;

/**
 * KeyResult
 *
 * @ORM\Table(name="key_result")
 * @ORM\Entity(repositoryClass="OKRBundle\Repository\KeyResultRepository")
 */
class KeyResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=255)
     */
    private $unit;

    /**
     * @var float
     *
     * @ORM\Column(name="from_value", type="float")
     */
    private $from = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="to_value", type="float")
     */
    private $to;

    /**
     * @var Objective
     *
     * @ORM\ManyToOne(targetEntity="OKRBundle\Entity\Objective", inversedBy="keyResults", cascade={"persist"})
     */
    private $objective;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_in_parent_objective", type="boolean")
     */
    private $showInParentObjective;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="OKRBundle\Entity\ResultUpdate", mappedBy="keyResult", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $resultUpdates;


    /**
     * @var ResultUpdate
     *
     * @ORM\OneToOne(targetEntity="OKRBundle\Entity\ResultUpdate", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $lastResult;

    public function __toString()
    {
        try {
            if ($lastResult = $this->getLastResult()) {
                return $this->getName() . ': ' . ' ' . $this->getProgress() . ' ' . ' (' . number_format($lastResult->getValue()) . ' ' . $this->getUnit() . ')';
            }
        } catch (EntityNotFoundException $e) {
            // Update last result
            $this->lastResult = null;
        }

        return $this->getName() . ': [No update]';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KeyResult
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return KeyResult
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return Objective
     */
    public function getObjective()
    {
        return $this->objective;
    }

    /**
     * @param Objective $objective
     * @return KeyResult
     */
    public function setObjective($objective)
    {
        $this->objective = $objective;

        return $this;
    }

    /**
     * @return float
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param float $from
     * @return KeyResult
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return float
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param float $to
     * @return KeyResult
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getResultUpdates()
    {
        return $this->resultUpdates;
    }

    /**
     * @param ArrayCollection $resultUpdates
     * @return KeyResult
     */
    public function setResultUpdates($resultUpdates)
    {
        /** @var ResultUpdate $resultUpdate */
        foreach ($this->resultUpdates as $resultUpdate) {
            $resultUpdate->setKeyResult($this);
        }

        $this->resultUpdates = $resultUpdates;

        return $this;
    }

    /**
     * @param ResultUpdate $resultUpdate
     */
    public function addResultUpdates($resultUpdate)
    {
        $resultUpdate->setKeyResult($this);

        $this->resultUpdates[] = $resultUpdate;
    }

    /**
     * @return ResultUpdate
     */
    public function getLastResult()
    {
        return $this->lastResult;
    }

    /**
     * @param ResultUpdate $lastResult
     * @return KeyResult
     */
    public function setLastResult($lastResult)
    {
        $this->lastResult = $lastResult;

        return $this;
    }

    /**
     * Update last result update
     */
    public function updateLastResult()
    {
        $this->setLastResult(null);

        // Traverse all result updates
        /** @var ResultUpdate $resultUpdate */
        foreach ($this->getResultUpdates() as $resultUpdate) {
            // If date of this result update is later than latest result
            if (!$this->getLastResult() || $resultUpdate->getDate()->getTimestamp() > $this->getLastResult()->getDate()->getTimestamp()) {
                // Switch
                $this->setLastResult($resultUpdate);
            }
        }
    }

    /**
     * From 0 ~ 1
     * Get progress of this key result
     * @return float|int
     */
    public function getProgress()
    {
        // No last result?
        if (!$this->lastResult || $this->getFrom() === $this->getTo()) {
            return 0;
        }

        if ($this->getFrom() > $this->getTo()) {
            $isRevert = true;
        } else {
            $isRevert = false;
        }

        // If not revert
        if (!$isRevert) {
            $progress = ($this->getLastResult()->getValue() - $this->getFrom()) / ($this->getTo() - $this->getFrom());
        } else {
            // Is revert
            $progress =  ($this->getLastResult()->getValue() - $this->getFrom()) / ($this->getTo() - $this->getFrom());
        }

        return round($progress, 2);
    }

    /**
     * Tooltip for goal design
     * @param Objective $currentObj
     * @return string
     */
    public function getTooltip(Objective $currentObj)
    {
        // prepend
        if ($currentObj->getId() != $this->objective->getId()) {
            $prepend = $this->objective->getName() . '<br />';
        } else {
            $prepend = '';
        }

        // append
        if ($this->getLastResult()) {
            $append = $this->formatNumber($this->getLastResult()->getValue()) . ' ' . $this->getUnit();
        } else {
            $append = '[No update]';
        }

        return $prepend . $this->getName() . ': ' . $append . '<br /> From: ' . $this->formatNumber($this->getFrom(), 2) . ' To: ' . $this->formatNumber($this->getTo(), 2);
    }

    /**
     * Format number in tooltip
     * @param $number
     * @return int
     */
    private function formatNumber($number)
    {
        if ($number === floor($number)) {
            $decimal = 0;
        } else {
            $decimal = 2;
        }

        return number_format($number, $decimal);
    }

    /**
     * Get result updates in index value list
     * @return array
     */
    public function getUpdateList()
    {
        $labels = array();
        $values = array();

        /** @var ResultUpdate $resultUpdate */
        foreach ($this->getResultUpdates() as $resultUpdate) {
            $labels[] = $resultUpdate->getDate()->format('Y-m-d');
            $values[] = $resultUpdate->getValue();
        }

        return array(
            'labels' => $labels,
            'values' => $values,
        );
    }

    /**
     * @return bool
     */
    public function isShowInParentObjective()
    {
        return $this->showInParentObjective;
    }

    /**
     * @param bool $showInParentObjective
     */
    public function setShowInParentObjective($showInParentObjective)
    {
        $this->showInParentObjective = $showInParentObjective;
    }

}

