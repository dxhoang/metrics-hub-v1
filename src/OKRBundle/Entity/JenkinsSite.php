<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace OKRBundle\Entity;


class JenkinsSite
{

    private $path;

    private $username;

    private $apiKey;

    /**
     * JenkinsSite constructor.
     * @param $path
     * @param $username
     * @param $apiKey
     */
    public function __construct($path, $username, $apiKey)
    {
        $this->path = $path;
        $this->username = $username;
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

}