<?php

namespace OKRBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResultUpdate
 *
 * @ORM\Table(name="result_update")
 * @ORM\Entity(repositoryClass="OKRBundle\Repository\ResultUpdateRepository")
 */
class ResultUpdate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var KeyResult
     *
     * @ORM\ManyToOne(targetEntity="OKRBundle\Entity\KeyResult", inversedBy="resultUpdates", cascade={"persist"})
     */
    private $keyResult;

    /**
     * ResultUpdate constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function __toString()
    {
        return $this->getDate()->format('Y-m-d') . ': '.  $this->getValue();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ResultUpdate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return ResultUpdate
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return KeyResult
     */
    public function getKeyResult()
    {
        return $this->keyResult;
    }

    /**
     * @param KeyResult $keyResult
     */
    public function setKeyResult($keyResult)
    {
        $this->keyResult = $keyResult;
    }

}

