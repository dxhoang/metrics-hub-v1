<?php

namespace OKRBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Object
 *
 * @ORM\Table(name="objective")
 * @ORM\Entity
 */
class Objective
{

    const DEFAULT_NAME = 'Please change or this wont be saved';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = Objective::DEFAULT_NAME;

    /**
     * @var Goal
     *
     * @ORM\ManyToOne(targetEntity="OKRBundle\Entity\Goal", inversedBy="children")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $goal;

    /**
     * @var Objective
     *
     * @ORM\ManyToOne(targetEntity="OKRBundle\Entity\Objective", inversedBy="children")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="OKRBundle\Entity\Objective", mappedBy="parent")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $children;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="OKRBundle\Entity\KeyResult", mappedBy="objective", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $keyResults;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $assignee;

    /**
     * @var float
     *
     * @ORM\Column(name="confident", type="float", nullable=true)
     */
    private $confident;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    private $sort = 10000;

    /**
     * Placeholder for html content to be rendered in show objective page
     * @var string
     */
    private $fullKeyResultsHtml;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Objective
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Objective
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Objective $parent
     * @return Objective
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     * @return Objective
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return array|ArrayCollection
     */
    public function getKeyResults($showInParentObjectiveOnly = false)
    {
        if ($showInParentObjectiveOnly) {
            $result = [];
            /** @var KeyResult $keyResult */
            foreach ($this->keyResults as $keyResult) {
                if (!$keyResult->isShowInParentObjective()) {
                    continue;
                }

                $result[] = $keyResult;
            }

            return $result;
        }

        return $this->keyResults;
    }

    /**
     * @param ArrayCollection $keyResults
     * @return Objective
     */
    public function setKeyResults($keyResults)
    {
        $this->keyResults = $keyResults;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Objective
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Objective
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * @param Goal $goal
     * @return Objective
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * @return User
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * @param User $assignee
     * @return Objective
     */
    public function setAssignee($assignee)
    {
        $this->assignee = $assignee;

        return $this;
    }

    /**
     * @return float
     */
    public function getConfident()
    {
        return $this->confident;
    }

    /**
     * @param float $confident
     * @return Objective
     */
    public function setConfident($confident)
    {
        $this->confident = $confident;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function addKeyResult(KeyResult $keyResult)
    {
        $keyResult->setObjective($this);
        $this->keyResults[] = $keyResult;
    }

    /**
     * From 0 ~ 1
     * Get progress of this key result
     * @return float|int
     */
    public function getProgress()
    {
        $keyResults = $this->getFullKeyResults();
        if (!count($keyResults)) {
            return 0;
        }

        $total = 0;
        $count = 0;
        /** @var KeyResult $keyResult */
        foreach ($keyResults as $keyResult) {
            $count++;

            $total += $keyResult->getProgress();
        }

        return round($total / $count, 2);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get children key results
     * @return array
     */
    public function getChildrenKeyResults()
    {
        $result = [];

        /** @var Objective $childObj */
        foreach ($this->getChildren() as $childObj) {
            $result = array_merge($result, $childObj->getKeyResults(true));
        }

        return $result;
    }

    /**
     * Get children key results
     * @return string
     */
    public function getFullKeyResultsHtml()
    {
        return $this->fullKeyResultsHtml;
    }

    /**
     * @param string $fullKeyResultsHtml
     */
    public function setFullKeyResultsHtml($fullKeyResultsHtml)
    {
        $this->fullKeyResultsHtml = $fullKeyResultsHtml;
    }

    /**
     * Get objective KRs and children KRs
     * @return array
     */
    public function getFullKeyResults()
    {
        $keyResults = $this->getKeyResults();
        if ($keyResults instanceof Collection) {
            $keyResults = $keyResults->toArray();
        }

        return array_merge($keyResults, $this->getChildrenKeyResults());
    }

}

