<?php

namespace OKRBundle\Manager;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\User;
use OKRBundle\Entity\Goal;
use OKRBundle\Entity\Objective;

/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 6/7/18
 * Time: 5:03 AM
 */

class ObjectiveManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ObjectiveManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Get repository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo()
    {
        return $this->em->getRepository('OKRBundle:Objective');
    }

    /**
     * Create new objective with goal
     * @param Goal $goal
     * @param User|null $assignee
     * @return Objective
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createNew(Goal $goal, User $assignee = null)
    {
        $obj = new Objective();
        $obj->setGoal($goal);

        if ($assignee) {
            $obj->setAssignee($assignee);
        }

        $this->em->persist($obj);
        $this->em->flush();

        return $obj;
    }

    /**
     * Save batch data from design form
     * @param $data
     * @param Goal $goal
     * @param $designType
     * @throws \Exception
     */
    public function saveBatch($data, Goal $goal, $designType)
    {
        $i = 0;
        foreach ($data as $objData) {
            // Skip if id is empty
            if (!isset($objData['id']) || !$objData['id']) {
                continue;
            }

            /** @var Objective $obj */
            $obj = $this->getRepo()->find($objData['id']);
            if (!$obj) {
                continue;
            }
            // Don't reset if design from objective page
            if ($designType == 'objective' && $i === 0) {
                $i = $obj->getSort();
            }

            $obj->setGoal($goal);

            // Find parent and set
            if (isset($objData['changed_position']) && $objData['changed_position']) {
                if ($objData['parent_id']) {
                    /** @var Objective $parentObj */
                    $parentObj = $this->getRepo()->find($objData['parent_id']);
                    if ($parentObj) {
                        $obj->setParent($parentObj);
                    } else {
                        // No parent if parent not found
                        $obj->setParent(null);
                    }

                } else if ($designType === 'goal') {
                    // Don't set null parent if design type is goal (design goal from view objective page)
                    $obj->setParent(null);
                }
            }


            if (isset($objData['changed_name']) && $objData['changed_name']) {
                $obj->setName($objData['name']);
            }
            if (isset($objData['changed_confident']) && $objData['changed_confident']) {
                $obj->setConfident($objData['confident']);
            }

            // Set assignee
            if (isset($objData['changed_assignee_id']) && $objData['changed_assignee_id']) {
                if ($objData['assignee_id']) {
                    $user = $this->em->getRepository('ApplicationSonataUserBundle:User')->find($objData['assignee_id']);
                    if (!$user) {
                        throw new \Exception(sprintf('User id %d not found', $objData['assignee_id']));
                    }
                    $obj->setAssignee($user);
                } else {
                    $obj->setAssignee(null);
                }
            }

            // sorting
            if ($designType != 'my') {
                $obj->setSort($i);
            }

            // Delete?
            if ($objData['delete']) {
                $this->em->remove($obj);
            } else {
                $this->em->persist($obj);
            }

            $i++;
        }

        // Flush to save all
        $this->em->flush();
    }

    /**
     * Delete objective
     * @param $id
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($id)
    {
        if ($objective = $this->getRepo()->find($id)) {
            $this->em->remove($objective);
            $this->em->flush();
        }
    }

    /**
     * Delete objectives unchanged objectives in last 1 hour
     * @param int $mins
     */
    public function deleteUnchanged($mins = 50)
    {
        $this->em->createQueryBuilder()
            ->delete('OKRBundle:Objective', 'o')
            ->where('o.name = :name AND o.createdAt <= :processTime')
            ->setParameters(array(
                'name' => Objective::DEFAULT_NAME,
                'processTime' => (new \DateTime)->modify('-' . $mins .' minutes'),
            ))
            ->getQuery()
            ->execute();
    }

    /**
     * Get objectives by goal id and assignee id
     * @param $goalId
     * @param $assigneeId
     * @return array
     */
    public function getByAssigneeId($goalId, $assigneeId)
    {
        return $this->getRepo()->createQueryBuilder('o')
            ->where('IDENTITY(o.goal) = :goalId AND IDENTITY(o.assignee) = :assigneeId')
            ->orderBy('o.sort', 'ASC')
            ->getQuery()
            ->setParameter('goalId', $goalId)
            ->setParameter('assigneeId', $assigneeId)
            ->getResult();
    }

}