<?php
/**
 * Created by PhpStorm.
 * User: quaninte
 * Date: 7/19/18
 * Time: 4:39 PM
 */

namespace OKRBundle\Manager;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use OKRBundle\Entity\Objective;

class KeyResultManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ObjectiveManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Get repository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo()
    {
        return $this->em->getRepository('OKRBundle:KeyResult');
    }

    /**
     * Get chilren key results of an objective
     * @param Objective $objective
     * @return array
     */
    public function findChildrenKRs(Objective $objective)
    {
        // Return empty if there are no children
        if (!count($objective->getChildren())) {
            return array();
        }

        return $this->getRepo()
            ->createQueryBuilder('kr')
            ->where('kr.objective IN (:objs) AND kr.showInParentObjective = true')
            ->getQuery()
            ->setParameter('objs', $objective->getChildren())
            ->getResult()
            ;
    }

}