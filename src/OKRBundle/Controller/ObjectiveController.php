<?php

namespace OKRBundle\Controller;

use OKRBundle\Entity\Objective;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/objective")
 */
class ObjectiveController extends Controller
{
    /**
     * Create new objective
     * @Route("/create/{goalId}")
     * @Method("GET")
     * @param $goalId
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function createAction($goalId)
    {
        $goal = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('OKRBundle:Goal')->find($goalId);
        if (!$goal) {
            return new JsonResponse(array(
                'error' => 'Goal not existed',
            ));
        }

        $objective = $this->container->get('okr.objective_manager')->createNew($goal, $this->getUser());
        $objectiveHtml = $this->container->get('twig')->render('OKRBundle:Admin/Goal:objective.html.twig', array(
            'obj' => $objective,
            'userList' => $this->container->get('fos_user.user_manager')->findUsers(),
        ));

        return new JsonResponse(array(
            'success' => true,
            'html' => $objectiveHtml,
        ));
    }

    /**
     * Get design form of an objective
     * @Route("/{id}/{designType}")
     * @Method("GET")
     * @param $id
     * @param $designType
     * @return JsonResponse
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getAction($id, $designType = null)
    {
        /** @var Objective $objective */
        $objective = $this->container->get('okr.objective_manager')->getRepo()->find($id);
        if (!$objective) {
            return new JsonResponse(array(
                'error' => 'Objective not existed',
            ));
        }
        $objectiveHtml = $this->container->get('twig')->render('OKRBundle:Admin/Goal:objective.html.twig', array(
            'obj' => $objective,
            'no_children' => true,
            'no_interact' => $designType == 'my' ? true : false,
            'userList' => $this->container->get('fos_user.user_manager')->findUsers(),
        ));

        return new JsonResponse(array(
            'success' => true,
            'html' => $objectiveHtml,
        ));
    }

    /**
     * @Route("/save-goal/{goalId}/{designType}")
     * @Method("POST")
     * @param $goalId
     * @param $designType
     * @return JsonResponse
     * @throws \Exception
     */
    public function saveGoalAction($goalId, $designType)
    {
        // If goal is missing
        if (!$goalId) {
            return new JsonResponse(array(
                'error' => 'Goal id missing'
            ), 500);
        }

        $goal = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('OKRBundle:Goal')->find($goalId);
        if (!$goal) {
            return new JsonResponse(array(
                'error' => 'Goal not existed',
            ));
        }

        $data = $this->get("request")->getContent();
        if(!$data) {
            return new JsonResponse(array(
                'error' => 'post content missing'
            ), 500);
        }
        $data = json_decode($data, true);

        // Do saving
        $this->container->get('okr.objective_manager')->saveBatch($data, $goal, $designType);

        return new JsonResponse(array(
            'success' => true,
        ));
    }

    /**
     * Delete objective
     * @Route("/delete/{id}")
     * @Method("GET")
     * @param $id
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction($id)
    {
        $this->container->get('okr.objective_manager')->delete($id);

        return new JsonResponse(array(
            'success' => true,
        ));
    }

    /**
     * Show objectives by userId
     * @Route("/user/{goalId}/{userId}", name="user_objectives")
     * @Method("GET")
     * @param $goalId
     * @param $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction($goalId, $userId)
    {
        $goal = $this->get('doctrine.orm.entity_manager')
            ->getRepository('OKRBundle:Goal')->find($goalId);
        if (!$goal) {
            throw new NotFoundHttpException('Goal not found');
        }

        $user = $this->get('fos_user.user_manager')->find($userId);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $objectives = $this->get('okr.objective_manager')->getByAssigneeId($goalId, $userId);

        return $this->render('@OKR/Admin/Objective/user.html.twig', [
            'goal' => $goal,
            'user' => $user,
            'objectives' => $objectives,
        ]);
    }

}
