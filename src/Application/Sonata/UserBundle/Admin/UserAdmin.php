<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace Application\Sonata\UserBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;

class UserAdmin extends BaseUserAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        parent::configureFormFields($formMapper);

        $formMapper
            ->tab('User')
                ->with('Social')
                    ->add('slackId', null, array(
                        'label' => 'Slack ID',
                    ))
                    ->remove('facebookUid')
                    ->remove('facebookName')
                    ->remove('twitterUid')
                    ->remove('twitterName')
                    ->remove('gplusName')
                ->end()
                ->with('Profile')
                    ->addHelp('phone', 'Phone with country code, plus sign & no space: +84984533878 is correct (instead of 0984533878 or 84 984533878 incorrect)')
                ->end()
            ->end()
        ;
    }

}