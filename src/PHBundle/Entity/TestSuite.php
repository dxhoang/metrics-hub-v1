<?php
/**
 * User: quaninte
 * Date: 9/27/18
 * Time: 11:44 PM
 */

namespace PHBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use PHBundle\Constants;


/**
 * Service
 *
 * @ORM\Table(name="ph_test_suite")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\TestSuiteRepository")
 */
class TestSuite
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Service
     *
     * @ORM\ManyToOne(targetEntity="PHBundle\Entity\Service")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $service;

    /**
     * @var Repo
     *
     * @ORM\ManyToOne(targetEntity="PHBundle\Entity\Repo")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $repo;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="env", type="string", length=255)
     */
    private $env;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255)
     */
    private $filePath;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = false;

    /**
     * @var TestRun
     * @ORM\OneToOne(targetEntity="PHBundle\Entity\TestRun")
     * @ORM\JoinColumn(name="last_run_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $lastRun;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="ph_test_suite_failure_subscribers"
     * )
     */
    private $failureSubscribers;

    /**
     * Status of the test suite, it is finished, running or queued
     * @var string
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(name="run_url", type="string", nullable=true)
     */
    private $runUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="trello_task", type="string", length=255, nullable=true)
     */
    private $trelloTask;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __toString()
    {
        return $this->getFilePath();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TestSuite
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TestSuite
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TestSuite
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TestSuite
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set service
     *
     * @param \PHBundle\Entity\Service $service
     *
     * @return TestSuite
     */
    public function setService(\PHBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \PHBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Get passed
     *
     * @return boolean
     */
    public function isPassed()
    {
        if ($this->getLastRun()) {
            return ($this->getLastRun()->getErrors() + $this->getLastRun()->getFailures()) === 0;
        }

        return false;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return TestSuite
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return TestSuite
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        // Set env
        if (!$this->getEnv()) {
            if (strpos(strtolower($filePath), 'prod')) {
                $env = 'prod';
            } else {
                $env = 'staging';
            }

            $this->setEnv($env);
        }

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Get last duration
     * @param bool $returnDefaultIfFalse
     * @param int $default
     * @return bool|int
     */
    public function getLastDuration($returnDefaultIfFalse = false, $default = 1200)
    {
        if ($this->getLastRun()) {
            return $this->getLastRun()->getDuration();
        }

        if ($returnDefaultIfFalse) {
            return $default;
        }

        return false;
    }

    /**
     * Set lastRun
     *
     * @param \PHBundle\Entity\TestRun $lastRun
     *
     * @return TestSuite
     */
    public function setLastRun(\PHBundle\Entity\TestRun $lastRun = null)
    {
        $this->lastRun = $lastRun;

        return $this;
    }

    /**
     * Get lastRun
     *
     * @return \PHBundle\Entity\TestRun
     */
    public function getLastRun()
    {
        return $this->lastRun;
    }

    /**
     * Set env
     *
     * @param string $env
     *
     * @return TestSuite
     */
    public function setEnv($env)
    {
        $this->env = $env;

        return $this;
    }

    /**
     * Get env
     *
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->failureSubscribers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     *
     * @return TestSuite
     */
    public function addFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers[] = $failureSubscriber;

        return $this;
    }

    /**
     * Remove failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     */
    public function removeFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers->removeElement($failureSubscriber);
    }

    /**
     * Get failureSubscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFailureSubscribers()
    {
        return $this->failureSubscribers;
    }

    /**
     * Get all failureSubscribers (merge with service failure subscribers)
     *
     * @return array
     */
    public function getAllFailureSubscribers()
    {
        $subscribers = array();
        /** @var User $user */
        foreach ($this->getFailureSubscribers() as $user) {
            $subscribers[$user->getId()] = $user;
        }

        if ($this->getService()) {
            /** @var User $user */
            foreach ($this->getService()->getFailureSubscribers() as $user) {
                if (!isset($subscribers[$user->getId()])) {
                    $subscribers[$user->getId()] = $user;
                }
            }
        }
        return $subscribers;
    }

    /**
     * Set repo
     *
     * @param \PHBundle\Entity\Repo $repo
     *
     * @return TestSuite
     */
    public function setRepo(\PHBundle\Entity\Repo $repo = null)
    {
        $this->repo = $repo;

        return $this;
    }

    /**
     * Get repo
     *
     * @return \PHBundle\Entity\Repo
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TestSuite
     */
    public function setStatus($status)
    {
        if ($status === Constants::STATUS_FINISHED) {
            $this->setRunUrl('');
        }
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set runUrl
     *
     * @param string $runUrl
     *
     * @return TestSuite
     */
    public function setRunUrl($runUrl)
    {
        $this->runUrl = $runUrl;

        return $this;
    }

    /**
     * Get runUrl
     *
     * @return string
     */
    public function getRunUrl()
    {
        return $this->runUrl;
    }

    /**
     * Set trelloTask
     *
     * @param string $trelloTask
     *
     * @return TestSuite
     */
    public function setTrelloTask($trelloTask)
    {
        $this->trelloTask = $trelloTask;

        return $this;
    }

    /**
     * Get trelloTask
     *
     * @return string
     */
    public function getTrelloTask()
    {
        return $this->trelloTask;
    }
}
