<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Service
 *
 * @ORM\Table(name="ph_repo")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\RepoRepository")
 */
class Repo
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=true)
     */
    private $uri;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Service", mappedBy="repos", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_service_repo"
     * )
     */
    private $services;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Job", inversedBy="repos", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_repo_job"
     * )
     */
    private $jobs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="scan_test_suite", type="boolean", nullable=true)
     */
    private $scanTestSuite;

    /**
     * @var string
     *
     * @ORM\Column(name="katalon_jenkins_job_url_prod", type="string", length=255, nullable=true)
     */
    private $katalonJenkinsJobURLProd;

    /**
     * @var string
     *
     * @ORM\Column(name="katalon_jenkins_job_url_staging", type="string", length=255, nullable=true)
     */
    private $katalonJenkinsJobURLStaging;

    /**
     * @var string
     *
     * @ORM\Column(name="katalon_jenkins_job_url_parallel", type="string", length=255, nullable=true)
     */
    private $katalonJenkinsJobURLParallel;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="PHBundle\Entity\TestSuite", mappedBy="repo")
     */
    private $testSuites;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Repo
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Repo
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Add service
     *
     * @param \PHBundle\Entity\Service $service
     *
     * @return Repo
     */
    public function addService(\PHBundle\Entity\Service $service)
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addRepo($this);
        }

        return $this;
    }

    /**
     * Remove service
     *
     * @param \PHBundle\Entity\Service $service
     */
    public function removeService(\PHBundle\Entity\Service $service)
    {
        $service->removeRepo($this);
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add job
     *
     * @param \PHBundle\Entity\Job $job
     *
     * @return Repo
     */
    public function addJob(\PHBundle\Entity\Job $job)
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;

            $job->addRepo($this);
        }

        return $this;
    }

    /**
     * Remove job
     *
     * @param \PHBundle\Entity\Job $job
     */
    public function removeJob(\PHBundle\Entity\Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Set scanTestSuite
     *
     * @param boolean $scanTestSuite
     *
     * @return Repo
     */
    public function setScanTestSuite($scanTestSuite)
    {
        $this->scanTestSuite = $scanTestSuite;

        return $this;
    }

    /**
     * Get scanTestSuite
     *
     * @return boolean
     */
    public function getScanTestSuite()
    {
        return $this->scanTestSuite;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Repo
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        if (!$this->uri && $this->link) {
            $detect = 'https://bitbucket.org/';
            if (substr($this->link, 0, strlen($detect)) === $detect) {
                // change https://bitbucket.org/brodev/beeketing-admin to git@bitbucket.org:brodev/beeketing-admin.git
                $this->uri = str_replace($detect, 'git@bitbucket.org:', $this->link);
                $this->uri = $this->uri . '.git';
            }
        }

        return $this->uri;
    }

    /**
     * Set katalonJenkinsJobURLProd
     *
     * @param string $katalonJenkinsJobURLProd
     *
     * @return Repo
     */
    public function setKatalonJenkinsJobURLProd($katalonJenkinsJobURLProd)
    {
        $this->katalonJenkinsJobURLProd = $katalonJenkinsJobURLProd;

        return $this;
    }

    /**
     * Get katalonJenkinsJobURLProd
     *
     * @return string
     */
    public function getKatalonJenkinsJobURLProd()
    {
        return $this->katalonJenkinsJobURLProd;
    }

    /**
     * Set katalonJenkinsJobURLStaging
     *
     * @param string $katalonJenkinsJobURLStaging
     *
     * @return Repo
     */
    public function setKatalonJenkinsJobURLStaging($katalonJenkinsJobURLStaging)
    {
        $this->katalonJenkinsJobURLStaging = $katalonJenkinsJobURLStaging;

        return $this;
    }

    /**
     * Get katalonJenkinsJobURLStaging
     *
     * @return string
     */
    public function getKatalonJenkinsJobURLStaging()
    {
        return $this->katalonJenkinsJobURLStaging;
    }

    /**
     * Set katalonJenkinsJobURLParallel
     *
     * @param string $katalonJenkinsJobURLParallel
     *
     * @return Repo
     */
    public function setKatalonJenkinsJobURLParallel($katalonJenkinsJobURLParallel)
    {
        $this->katalonJenkinsJobURLParallel = $katalonJenkinsJobURLParallel;

        return $this;
    }

    /**
     * Get katalonJenkinsJobURLParallel
     *
     * @return string
     */
    public function getKatalonJenkinsJobURLParallel()
    {
        return $this->katalonJenkinsJobURLParallel;
    }

    /**
     * Get katalon jenkins URL by env
     * @param $env
     * @return string
     */
    public function getKatalonJenkinsURLByEnv($env)
    {
        if ($env === 'prod') {
            return $this->getKatalonJenkinsJobURLProd();
        } else if ($env === 'parallel') {
            return $this->getKatalonJenkinsJobURLParallel();
        }

        return $this->getKatalonJenkinsJobURLStaging();
    }


    /**
     * Add testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     *
     * @return Repo
     */
    public function addTestSuite(\PHBundle\Entity\TestSuite $testSuite)
    {
        $this->testSuites[] = $testSuite;

        return $this;
    }

    /**
     * Remove testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     */
    public function removeTestSuite(\PHBundle\Entity\TestSuite $testSuite)
    {
        $this->testSuites->removeElement($testSuite);
    }

    /**
     * Get testSuites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestSuites()
    {
        return $this->testSuites;
    }
}
