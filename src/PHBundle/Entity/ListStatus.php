<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/17/18
 */

namespace PHBundle\Entity;


class ListStatus
{

    public $success = 0;

    public $total = 0;

    /**
     * Return true if all list success
     * @return bool
     */
    public function isAllSuccess()
    {
        return $this->success === $this->total;
    }

}