<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Service
 *
 * @ORM\Table(name="ph_job")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\JobRepository")
 */
class Job
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Repo", mappedBy="jobs", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_repo_job"
     * )
     */
    private $repos;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Service", mappedBy="jobs", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_service_job"
     * )
     */
    private $services;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="env", type="string", length=30, nullable=true)
     */
    private $env;

    /**
     * Jenkins site that host this job: builder / cicd
     * @var string
     *
     * @ORM\Column(name="jenkins_site", type="string", length=255)
     */
    private $jenkinsSite;

    /**
     * type of this job: automation_test / code_build
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30)
     */
    private $type;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PHBundle\Entity\Build", mappedBy="job")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $builds;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="ph_job_failure_subscribers"
     * )
     */
    private $failureSubscribers;

    public function __toString()
    {
        return $this->getJenkinsSite() . ' - ' . $this->getName() . ' - ' . $this->getEnv();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->failureSubscribers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add repo
     *
     * @param \PHBundle\Entity\Repo $repo
     *
     * @return Job
     */
    public function addRepo(\PHBundle\Entity\Repo $repo)
    {
        if (!$this->repos->contains($repo)) {
            $this->repos[] = $repo;
            $repo->addJob($this);
        }

        return $this;
    }

    /**
     * Remove repo
     *
     * @param \PHBundle\Entity\Repo $repo
     */
    public function removeRepo(\PHBundle\Entity\Repo $repo)
    {
        $this->repos->removeElement($repo);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        // Make sure url ends with /
        if (substr($url, -1) != '/') {
            $url .= '/';
        }

        $this->url = $url;
    }

    /**
     * Add build
     *
     * @param \PHBundle\Entity\Build $build
     *
     * @return Job
     */
    public function addBuild(\PHBundle\Entity\Build $build)
    {
        $this->builds[] = $build;

        return $this;
    }

    /**
     * Remove build
     *
     * @param \PHBundle\Entity\Build $build
     */
    public function removeBuild(\PHBundle\Entity\Build $build)
    {
        $this->builds->removeElement($build);
    }

    /**
     * Get builds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBuilds()
    {
        return $this->builds;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set env
     *
     * @param string $env
     *
     * @return Job
     */
    public function setEnv($env)
    {
        $this->env = $env;

        return $this;
    }

    /**
     * Get env
     *
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * Set jenkinsSite
     *
     * @param string $jenkinsSite
     *
     * @return Job
     */
    public function setJenkinsSite($jenkinsSite)
    {
        $this->jenkinsSite = $jenkinsSite;

        return $this;
    }

    /**
     * Get jenkinsSite
     *
     * @return string
     */
    public function getJenkinsSite()
    {
        return $this->jenkinsSite;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Job
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get repos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepos()
    {
        return $this->repos;
    }

    /**
     * Add service
     *
     * @param \PHBundle\Entity\Service $service
     *
     * @return Job
     */
    public function addService(\PHBundle\Entity\Service $service)
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addJob($this);
        }

        return $this;
    }

    /**
     * Remove service
     *
     * @param \PHBundle\Entity\Service $service
     */
    public function removeService(\PHBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     *
     * @return Job
     */
    public function addFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers[] = $failureSubscriber;

        return $this;
    }

    /**
     * Remove failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     */
    public function removeFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers->removeElement($failureSubscriber);
    }

    /**
     * Get failureSubscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFailureSubscribers()
    {
        return $this->failureSubscribers;
    }
}
