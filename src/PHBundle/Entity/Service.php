<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Service
 *
 * @ORM\Table(name="ph_service")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Repo", inversedBy="services", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_service_repo"
     * )
     */
    private $repos;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PHBundle\Entity\Job", inversedBy="services", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="ph_service_job"
     * )
     */
    private $jobs;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="PHBundle\Entity\TestSuite", mappedBy="service")
     */
    private $testSuites;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="ph_service_failure_subscribers"
     * )
     */
    private $failureSubscribers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->failureSubscribers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Add repo
     *
     * @param \PHBundle\Entity\Repo $repo
     *
     * @return Service
     */
    public function addRepo(\PHBundle\Entity\Repo $repo)
    {
        if (!$this->repos->contains($repo)) {
            $this->repos[] = $repo;
            $repo->addService($this);
        }

        return $this;
    }

    /**
     * Remove repo
     *
     * @param \PHBundle\Entity\Repo $repo
     */
    public function removeRepo(\PHBundle\Entity\Repo $repo)
    {
        $repo->removeService($this);
        $this->repos->removeElement($repo);
    }

    /**
     * Get repos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepos()
    {
        return $this->repos;
    }


    /**
     * Add job
     *
     * @param \PHBundle\Entity\Job $job
     *
     * @return Service
     */
    public function addJob(\PHBundle\Entity\Job $job)
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->addService($this);
        }

        return $this;
    }

    /**
     * Remove job
     *
     * @param \PHBundle\Entity\Job $job
     */
    public function removeJob(\PHBundle\Entity\Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Service
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get tags as array
     *
     * @return array
     */
    public function getTagsArray()
    {
        $result = array();

        if (!$this->tags) {
            return $result;
        }
        foreach (explode(',', $this->tags) as $tag) {
            if ($tag = trim($tag)) {
                $result[] = $tag;
            }
        }

        return $result;
    }

    /**
     * Add testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     *
     * @return Service
     */
    public function addTestSuite(\PHBundle\Entity\TestSuite $testSuite)
    {
        $this->testSuites[] = $testSuite;
        $testSuite->setService($this);

        return $this;
    }

    /**
     * Remove testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     */
    public function removeTestSuite(\PHBundle\Entity\TestSuite $testSuite)
    {
        $this->testSuites->removeElement($testSuite);
        $testSuite->setService(null);
    }

    /**
     * Get testSuites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestSuites()
    {
        return $this->testSuites;
    }

    /**
     * Add failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     *
     * @return Service
     */
    public function addFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers[] = $failureSubscriber;

        return $this;
    }

    /**
     * Remove failureSubscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $failureSubscriber
     */
    public function removeFailureSubscriber(\Application\Sonata\UserBundle\Entity\User $failureSubscriber)
    {
        $this->failureSubscribers->removeElement($failureSubscriber);
    }

    /**
     * Get failureSubscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFailureSubscribers()
    {
        return $this->failureSubscribers;
    }
}
