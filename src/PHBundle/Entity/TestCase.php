<?php
/**
 * User: quaninte
 * Date: 9/27/18
 * Time: 11:45 PM
 */

namespace PHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Service
 *
 * @ORM\Table(name="ph_test_case")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\TestCaseRepository")
 */
class TestCase
{

    public static $PRIORITY_CHOICES = array(
        0 => 'Low',
        1 => 'Medium',
        2 => 'High',
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var TestSuite
     *
     * @ORM\ManyToOne(targetEntity="PHBundle\Entity\TestSuite")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $testSuite;

    /**
     * @var string
     *
     * @ORM\Column(name="pre_conditions", type="text", nullable=true)
     */
    private $preConditions;

    /**
     * @var string
     *
     * @ORM\Column(name="steps", type="text", nullable=true)
     */
    private $steps;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="expected_result", type="text", nullable=true)
     */
    private $expectedResult;

    /**
     * @var int
     *
     * @ORM\Column(name="passed", type="boolean")
     */
    private $passed = false;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", length=1)
     */
    private $priority = 0;


    /**
     * @var int
     *
     * @ORM\Column(name="automation", type="boolean")
     */
    private $automation = true;

    /**
     * @var string
     *
     * @ORM\Column(name="katalon_file", type="string", length=255, nullable=true)
     */
    private $katalonFile;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;



    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TestCase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set steps
     *
     * @param string $steps
     *
     * @return TestCase
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * Get steps
     *
     * @return string
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return TestCase
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set expectedResult
     *
     * @param string $expectedResult
     *
     * @return TestCase
     */
    public function setExpectedResult($expectedResult)
    {
        $this->expectedResult = $expectedResult;

        return $this;
    }

    /**
     * Get expectedResult
     *
     * @return string
     */
    public function getExpectedResult()
    {
        return $this->expectedResult;
    }

    /**
     * Set passed
     *
     * @param boolean $passed
     *
     * @return TestCase
     */
    public function setPassed($passed)
    {
        $this->passed = $passed;

        return $this;
    }

    /**
     * Get passed
     *
     * @return boolean
     */
    public function getPassed()
    {
        return $this->passed;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return TestCase
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set automation
     *
     * @param boolean $automation
     *
     * @return TestCase
     */
    public function setAutomation($automation)
    {
        $this->automation = $automation;

        return $this;
    }

    /**
     * Get automation
     *
     * @return boolean
     */
    public function getAutomation()
    {
        return $this->automation;
    }

    /**
     * Set katalonFile
     *
     * @param string $katalonFile
     *
     * @return TestCase
     */
    public function setKatalonFile($katalonFile)
    {
        $this->katalonFile = $katalonFile;

        return $this;
    }

    /**
     * Get katalonFile
     *
     * @return string
     */
    public function getKatalonFile()
    {
        return $this->katalonFile;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TestCase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TestCase
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     *
     * @return TestCase
     */
    public function setTestSuite(\PHBundle\Entity\TestSuite $testSuite = null)
    {
        $this->testSuite = $testSuite;

        return $this;
    }

    /**
     * Get testSuite
     *
     * @return \PHBundle\Entity\TestSuite
     */
    public function getTestSuite()
    {
        return $this->testSuite;
    }

    /**
     * Set preConditions
     *
     * @param string $preConditions
     *
     * @return TestCase
     */
    public function setPreConditions($preConditions)
    {
        $this->preConditions = $preConditions;

        return $this;
    }

    /**
     * Get preConditions
     *
     * @return string
     */
    public function getPreConditions()
    {
        return $this->preConditions;
    }



}
