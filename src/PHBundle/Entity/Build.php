<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Service
 *
 * @ORM\Table(name="ph_build", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="job_number", columns={"job_id", "number"})
 * })
 * @ORM\Entity(repositoryClass="PHBundle\Repository\BuildRepository")
 */
class Build
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="PHBundle\Entity\Job", inversedBy="builds")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $job;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var int
     *
     * @ORM\Column(name="build_timestamp", type="integer", length=11)
     */
    private $buildTimestamp;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Set job
     *
     * @param \PHBundle\Entity\Job $job
     *
     * @return Build
     */
    public function setJob(\PHBundle\Entity\Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \PHBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set buildTimestamp
     *
     * @param integer $buildTimestamp
     *
     * @return Build
     */
    public function setBuildTimestamp($buildTimestamp)
    {
        $this->buildTimestamp = $buildTimestamp;

        return $this;
    }

    /**
     * Get buildTimestamp
     *
     * @return integer
     */
    public function getBuildTimestamp()
    {
        return $this->buildTimestamp;
    }

    /**
     * Get build date time
     *
     * @return string
     */
    public function getBuildDateTime()
    {
        $dateTime = new \DateTime();
        $dateTime->setTimestamp($this->buildTimestamp);

        return $dateTime->format('r');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Build
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Build
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Build
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Build
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Build
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get build url
     * @return string
     */
    public function getUrl()
    {
        return trim($this->getJob()->getUrl(), '/') . '/' . $this->getNumber();
    }

}
