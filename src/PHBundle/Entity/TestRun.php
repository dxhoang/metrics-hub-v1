<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/1/18
 */

namespace PHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * TestRun
 *
 * @ORM\Table(name="ph_test_run")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\TestRunRepository")
 */
class TestRun
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TestSuite
     *
     * @ORM\ManyToOne(targetEntity="PHBundle\Entity\TestSuite")
     * @ORM\JoinColumn(name="test_suite_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $testSuite;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", length=11)
     */
    private $duration = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="errors", type="integer", length=11)
     */
    private $errors = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="failures", type="integer", length=11)
     */
    private $failures = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="report_folder", type="string", length=255, nullable=true)
     */
    private $reportFolder;

    /**
     * @var bool
     *
     * @ORM\Column(name="timeout", type="boolean", nullable=true)
     */
    private $timeout = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="test_server_error", type="boolean", nullable=true)
     */
    private $testServerError = false;

    /**
     * @var string
     *
     * @ORM\Column(name="build_log_url", type="string", nullable=true)
     */
    private $buildLogURL;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return TestRun
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set errors
     *
     * @param integer $errors
     *
     * @return TestRun
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get errors
     *
     * @return integer
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TestRun
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set reportFolder
     *
     * @param string $reportFolder
     *
     * @return TestRun
     */
    public function setReportFolder($reportFolder)
    {
        $this->reportFolder = $reportFolder;

        return $this;
    }

    /**
     * Get reportFolder
     *
     * @return string
     */
    public function getReportFolder()
    {
        return $this->reportFolder;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TestRun
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TestRun
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set testSuite
     *
     * @param \PHBundle\Entity\TestSuite $testSuite
     *
     * @return TestRun
     */
    public function setTestSuite(\PHBundle\Entity\TestSuite $testSuite = null)
    {
        $this->testSuite = $testSuite;

        return $this;
    }

    /**
     * Get testSuite
     *
     * @return \PHBundle\Entity\TestSuite
     */
    public function getTestSuite()
    {
        return $this->testSuite;
    }

    /**
     * Set failures
     *
     * @param integer $failures
     *
     * @return TestRun
     */
    public function setFailures($failures)
    {
        $this->failures = $failures;

        return $this;
    }

    /**
     * Get failures
     *
     * @return integer
     */
    public function getFailures()
    {
        return $this->failures;
    }

    public function getReportPath()
    {
        return $this->getReportFolder() . '/Report.html';
    }

    public function getReportConsoleLogPath()
    {
        return $this->getReportFolder() . '/JUnit_Report.xml';
    }

    /**
     * Return true if test run passed
     * @return bool
     */
    public function isPassed()
    {
        return ( ($this->getFailures() === 0) && ($this->getErrors() === 0) );
    }

    /**
     * Set buildLogURL
     *
     * @param string $buildLogURL
     *
     * @return TestRun
     */
    public function setBuildLogURL($buildLogURL)
    {
        $this->buildLogURL = $buildLogURL;

        return $this;
    }

    /**
     * Get buildLogURL
     *
     * @return string
     */
    public function getBuildLogURL()
    {
        if (!$this->buildLogURL) {
            $this->buildLogURL = '#';
        }

        return $this->buildLogURL;
    }

    /**
     * Set timeout
     *
     * @param boolean $timeout
     *
     * @return TestRun
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * Get timeout
     *
     * @return boolean
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Get tooltip
     * @return string
     */
    public function getTooltip()
    {
        if ($this->isPassed()) {
            return 'Duration: ' . $this->getDuration();
        }

        return sprintf('Timeout: %s, Errors: %d, Failure: %d, Server Error: %s', $this->timeout ? 'true' : 'false',
            $this->getErrors(), $this->getFailures(),  $this->getTestServerError() ? 'true' : 'false');
    }

    /**
     * Set testServerError
     *
     * @param boolean $testServerError
     *
     * @return TestRun
     */
    public function setTestServerError($testServerError)
    {
        $this->testServerError = $testServerError;

        return $this;
    }

    /**
     * Get testServerError
     *
     * @return boolean
     */
    public function getTestServerError()
    {
        return $this->testServerError;
    }
}
