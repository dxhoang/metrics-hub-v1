<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/30/18
 */

namespace PHBundle\Entity;


use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="ph_alert")
 * @ORM\Entity(repositoryClass="PHBundle\Repository\AlertRepository")
 */
class Alert
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="ph_alert_subscribers"
     * )
     */
    private $subscribers;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message = 'system alert';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subscribers = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Alert
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add subscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $subscriber
     *
     * @return Alert
     */
    public function addSubscriber(\Application\Sonata\UserBundle\Entity\User $subscriber)
    {
        $this->subscribers[] = $subscriber;

        return $this;
    }

    /**
     * Remove subscriber
     *
     * @param \Application\Sonata\UserBundle\Entity\User $subscriber
     */
    public function removeSubscriber(\Application\Sonata\UserBundle\Entity\User $subscriber)
    {
        $this->subscribers->removeElement($subscriber);
    }

    /**
     * Get subscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }

    /**
     * List subscribers with phone
     *
     * @return string
     */
    public function getSubscribersWithPhone()
    {
        $html = '<ul>';

        /** @var User $subscriber */
        foreach ($this->subscribers as $subscriber) {
            $html .= '<li> ' . $subscriber->getEmail() . ' - '
                . ($subscriber->getPhone() ? $subscriber->getPhone() : '<b>Phone missing!</b>') .  ' </li>';
        }

        $html .= '</ul>';

        return $html;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Alert
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
