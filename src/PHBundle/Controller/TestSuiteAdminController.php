<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/3/18
 */

namespace PHBundle\Controller;


use PHBundle\Entity\Repo;
use PHBundle\Manager\TestSuiteManager;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestSuiteAdminController extends CRUDController
{

    /**
     * @param ProxyQuery $proxyQuery
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Twig_Error
     * @throws \Exception
     */
    public function batchActionQueueToRunTests(ProxyQuery $proxyQuery)
    {
        // Manager
        /** @var TestSuiteManager $testSuiteManager */
        $testSuiteManager = $this->container->get('ph.manager.test_suite');

        // To redirect after assign
        $request = $this->getRequest();
        if ($referer = $request->headers->get('referer')) {
            $request->getSession()->set('assign_redirect', $referer);
        }

        $testSuites = $proxyQuery->getQueryBuilder()->getQuery()->getResult();
        $env = 'parallel';

        // Run test suites
        $testSuiteManager->runTestSuites($testSuites, $env);

        $this->get('session')->getFlashBag()->add('success', 'Queued to run tests');

        if ($referer = $request->headers->get('referer')) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute('admin_ph_testsuite_list');
    }


}