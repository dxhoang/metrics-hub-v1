<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/30/18
 */

namespace PHBundle\Controller;


use Application\Sonata\UserBundle\Entity\User;
use PHBundle\Entity\Alert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Twilio\TwiML\VoiceResponse;

/**
 * @Route("/alert")
 */
class AlertController extends Controller
{

    /**
     * Run service test
     * @Route("/notify/{code}/{isTest}", name="alert_notify")
     * @param $code
     * @param int $isTest
     * @return JsonResponse
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function notifyAction($code, $isTest = 0)
    {
        /** @var Alert $alert */
        $alert = $this->get('doctrine.orm.entity_manager')->getRepository('PHBundle:Alert')->findOneBy(array(
            'code' => $code,
        ));

        if (!$alert) {
            throw new NotFoundHttpException(sprintf('Alert with code %s not found', $code));
        }

        /** @var \Twilio\Rest\Client $twilioClient */
        $twilioClient = $this->get('twilio');

        $result = array(
            'called' => 0,
            'failed' => 0,
            'is_test' => $isTest ? true : false,
        );

        // Traverse subscribers
        /** @var User $subscriber */
        foreach ($alert->getSubscribers() as $subscriber) {
            if (!$subscriber->getPhone()) {
                $result['failed']++;
                continue;
            }

            $url = $this->generateUrl('alert_generate_twiml_message', array(
                'message' => $alert->getMessage(),
            ), UrlGenerator::ABSOLUTE_URL);

            if (!$isTest) {
                $twilioClient->calls->create($subscriber->getPhone(),$this->getParameter('twilio_from_number'), array(
                    'url' => $url,
                ));
            }
            $result['called']++;
        }

        return new JsonResponse($result);
    }

    /**
     * Generate TwiML message
     * @Route("/generate-twiml/{message}", name="alert_generate_twiml_message")
     * @param $message
     * @return Response
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function generateTwiMLMessageAction($message)
    {
        $response = new VoiceResponse();
        $response->say($message);

        return new Response($response, 200, array(
            'Content-Type' => 'text/xml',
        ));
    }

}