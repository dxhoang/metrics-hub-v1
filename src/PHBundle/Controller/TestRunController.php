<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/2/18
 */

namespace PHBundle\Controller;

use AppBundle\Controller\AdminController;
use PHBundle\Entity\TestRun;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/test-run")
 */
class TestRunController extends AdminController
{
    /**
     * @Route("/view-report/{id}", name="test_run_view_report")
     * @Method("GET")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewReportAction($id)
    {
        /** @var TestRun $testRun */
        $testRun = $this->container->get('ph.manager.test_run')->getRepository()->find($id);

        if (!$testRun) {
            throw new NotFoundHttpException('Test run not found');
        }

        if (!$testRun->getReportFolder() || $testRun->getTimeout()) {
            $url = $testRun->getBuildLogURL();
            if ($url === '#') {
                $url = 'http://builder.beeketing.com/job/katalon-product-hub-runner';
            }
            return $this->redirect($url);
        }

        $ctx = $this->container->get('router')->getContext();

        $reportHTMLUrl = $ctx->getScheme() . '://' . $ctx->getHost() . $ctx->getBaseUrl() . '/reports/data/' . $testRun->getReportPath();
        $reportConsoleLogUrl = $ctx->getScheme() . '://' . $ctx->getHost() . $ctx->getBaseUrl() . '/reports/data/' . $testRun->getReportConsoleLogPath();

        return $this->render('@PH/Admin/TestRun/view_report.html.twig', array(
            'testRun' => $testRun,
            'reportHTMLUrl' => $reportHTMLUrl,
            'reportConsoleLogUrl' => $reportConsoleLogUrl,
        ));
    }

}