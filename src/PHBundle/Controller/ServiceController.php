<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 3:01 AM
 */

namespace PHBundle\Controller;


use PHBundle\Entity\Service;
use PHBundle\Manager\TestSuiteManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/service")
 */
class ServiceController extends Controller
{

    /**
     * Run service test
     * @Route("/run-test/{serviceId}/{env}/{redirect}", name="admin_service_run_test")
     * @param $serviceId
     * @param $env
     * @param bool $redirect
     * @return RedirectResponse|JsonResponse
     * @throws \Exception
     */
    public function runTestAction($serviceId, $env, $redirect = true)
    {
        if (!is_bool($redirect)) {
            $redirect = boolval($redirect);
        }

        /** @var TestSuiteManager $testSuiteManager */
        $testSuiteManager = $this->get('ph.manager.test_suite');

        // If not all
        if ($serviceId !== 'all') {
            $services = array();

            // Find service by id
            if ($service = $this->get('ph.manager.service')->getRepository()->find($serviceId)) {
                $services[] = $service;
            }
        } else {
            // Else
            // Find all services
            $services = $this->get('ph.manager.service')->getRepository()->findAll();
        }

        if (!$services) {
            //throw new NotFoundHttpException('Services not found');
            $this->get('session')->getFlashBag()->add('danger', 'Services not found');

            if ($redirect) {
                return $this->redirect($this->generateUrl('admin_ph_service_list'));
            }

            return new JsonResponse(array(
                'success' => true,
            ));

        } else {

            // Find all test suites of services
            $testSuites = $testSuiteManager->getRepository()->findByServices($services, $env, true);

            // Run test suites
            $testSuiteManager->runTestSuites($testSuites, $env);

            $this->get('session')->getFlashBag()->add('success', 'Queued to run tests');

            if ($redirect) {
                return $this->redirect($this->generateUrl('admin_ph_service_list'));
            }

            return new JsonResponse(array(
                'success' => true,
            ));
        }
    }

    /**
     * Run service test
     * @Route("/status", name="admin_service_status")
     * @return JsonResponse
     */
    public function statusAction()
    {
        $data = array();

        $services = $this->get('ph.manager.service')->getRepository()->findAll();
        /** @var Service $service */
        foreach ($services as $service) {
            $data[$service->getName()] = array();

            foreach (array('staging', 'prod') as $env) {
                $listStatus = $this->get('ph.manager.test_suite')->getServiceStatus($service, $env);
                $data[$service->getName()][$env] = array(
                    'passed' => $listStatus->isAllSuccess(),
                    'success' => $listStatus->success,
                    'total' => $listStatus->total,
                );
            }
        }

        return new JsonResponse($data);
    }

}