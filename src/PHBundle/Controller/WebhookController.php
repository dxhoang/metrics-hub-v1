<?php

namespace PHBundle\Controller;

use PHBundle\Constants;
use PHBundle\Entity\Repo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/webhook")
 */
class WebhookController extends Controller
{
    /**
     * @Route("/build")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function buildAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['job_url']) || !isset($data['build_number'])) {
            return new JsonResponse(array(
                'hit' => false,
                'message' => 'Missing job_url or build_number in request content',
            ), 500);
        }

        if (!$job = $this->container->get('ph.manager.job')->getRepository()->findOneByURL($data['job_url'])) {
            return new JsonResponse(array(
                'hit' => false,
                'message' => 'Job not found',
            ), 404);
        }

        // Queue to update
        $this->container->get('old_sound_rabbit_mq.update_build_producer')->publish(json_encode(array(
            'job_id' => $job->getId(),
            'build_number' => $data['build_number'],
        )));

        return new JsonResponse(array(
            'hit' => true,
        ));
    }

    /**
     * @Route("/report-zip")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function updateTestRunAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (!$reportZip = $data['report_zip_local']) {
            return $this->responseJsonError('Missing field report_zip (str)');
        }
        if (!$runCode = $data['run_code']) {
            return $this->responseJsonError('Missing field run_code (str)');
        }
        if (!$buildLogUrl = $data['build_log_url']) {
            return $this->responseJsonError('Missing field build_log_url (str)');
        }
        if (!$filePaths = $data['file_paths']) {
            return $this->responseJsonError('Missing field build_log_url (str)');
        }

        // Queue to process
        $this->container->get('old_sound_rabbit_mq.process_report_result_producer')->publish(json_encode(array(
            'file_name' => $reportZip,
            'run_code' => $runCode,
            'build_log_url' => $buildLogUrl,
            'file_paths' => $filePaths,
        )));

        return new JsonResponse(array(
            'hit' => true,
        ));
    }

    /**
     * @Route("/test-suite-timeout")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function testSuiteTimeoutAction()
    {
        if (!$filePath = $_POST['file_path']) {
            throw new \Exception('Missing field file_path (str)');
        }
        if (!$runCode = $_POST['run_code']) {
            throw new \Exception('Missing field run_code (str)');
        }
        if (!$buildLogUrl = $_POST['build_log_url']) {
            throw new \Exception('Missing field build_log_url (str)');
        }

        // Find test suite
        if (substr($filePath, -3) !== '.ts') {
            $filePath .= '.ts';
        }

        $testSuiteManager = $this->container->get('ph.manager.test_suite');
        $testSuite = $testSuiteManager->getRepository()->findByFilePath($filePath);
        if (!$testSuite) {
            return new JsonResponse(array(
                'error' => sprintf('Test suite %s not found', $filePath),
            ), 404);
        }

        // Set timeout and log
        $testRunManager = $this->container->get('ph.manager.test_run');
        $testRun = $testRunManager->create($testSuite);
        $testRun->setTimeout(true)
            ->setCode($runCode)
            ->setErrors(1)
            ->setBuildLogURL($buildLogUrl)
        ;
        $testRunManager->save($testRun);

        $testSuite->setLastRun($testRun);
        $testSuiteManager->save($testSuite);

        return new JsonResponse(array(
            'hit' => true,
        ));
    }

    /**
     * Response with json error
     * @param $message
     * @return JsonResponse
     */
    private function responseJsonError($message)
    {
        return new JsonResponse(array(
            'error' => $message,
        ), 500);
    }

    /**
     * @Route("/start-test-suites")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startTestSuitesAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['file_paths']) || !isset($data['repo'])) {
            return new JsonResponse(array(
                'hit' => false,
                'message' => 'Missing file_paths or repo in request content',
            ), 500);
        }

        // Find repo
        /** @var Repo $repo */
        $repo = $this->get('ph.manager.repo')->getRepository()->findOneBy(array(
            'uri' => $data['repo'],
        ));

        // Find test suites by file paths
        $testSuiteManger = $this->get('ph.manager.test_suite');
        foreach ($data['file_paths'] as $filePath) {
            if (substr($filePath, -3) !== '.ts') {
                $filePath .= '.ts';
            }

            if ($repo) {
                $testSuite = $testSuiteManger->getRepository()->findByFilePathAndRepo($filePath, $repo);
            } else {
                $testSuite = $testSuiteManger->getRepository()->findByFilePath($filePath);
            }

            if ($testSuite) {
                $testSuiteManger->setStatus($testSuite, Constants::STATUS_RUNNING);
            }
        }

        $testSuiteManger->flush();

        return new JsonResponse(array(
            'hit' => true,
        ));
    }

}
