<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace PHBundle\Controller;


use Doctrine\ORM\QueryBuilder;
use PHBundle\Entity\Job;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

class JobAdminController extends CRUDController
{

    public function batchActionAssignService(ProxyQuery $proxyQuery)
    {
        // To redirect after assign
        $request = $this->getRequest();
        if ($referer = $request->headers->get('referer')) {
            $request->getSession()->set('assign_redirect', $referer);
        }

        /** @var QueryBuilder $qb */
        $qb = $proxyQuery->getQueryBuilder();
        $jobIds = array();

        /** @var Job $job */
        foreach ($qb->getQuery()->getResult() as $job) {
            $jobIds[] = $job->getId();
        }
        $jobIdsString = implode($jobIds, ',');


        return $this->redirectToRoute('admin_assign_jobs_to_service', array(
            'job_ids' => $jobIdsString,
        ));
    }

}