<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace PHBundle\Controller;

use PHBundle\Entity\Job;
use PHBundle\Entity\Service;
use PHBundle\Manager\JobManager;
use PHBundle\Manager\ServiceManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/job")
 */
class JobController extends Controller
{

    /**
     * Assign jobs to service
     * @Route("/assign-to-service", name="admin_assign_jobs_to_service")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function assignJobsToServiceAction(Request $request)
    {
        /** @var JobManager $jobManager */
        $jobManager = $this->container->get('service_container')->get('ph.manager.job');

        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->container->get('service_container')->get('ph.manager.service');

        $services = $serviceManager->getRepository()->findAll();
        $serviceOptions = array();
        /** @var Service $service */
        foreach ($services as $service) {
            $serviceOptions[$service->getId()] = $service->getName();
        }

        $form = $this->createFormBuilder()
            ->add('jobIds', TextType::class, array(
                'required' => true,
            ))
            ->add('service', ChoiceType::class, array(
                'choices' => $serviceOptions,
                'required' => true,
            ))
            ->getForm()
        ;

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if ($form->isValid()) {
                // Get service
                /** @var Service $service */
                $service = $serviceManager->getRepository()->find($form->get('service')->getData());

                $jobIdsString = $form->get('jobIds')->getData();
                $jobIds = explode(',', $jobIdsString);
                foreach ($jobIds as $jobId) {
                    /** @var Job $job */
                    if ($job = $jobManager->getRepository()->find($jobId)) {
                        $job->addService($service);

                        $jobManager->save($job);
                    }
                }

                // Done
                // Redirect back to list jobs
                $this->container->get('session')->getFlashBag()->add('success',
                    sprintf('Assign %d jobs to service %s', count($jobIds), $service->getName()));
                if ($url = $request->getSession()->get('assign_redirect')) {
                    $request->getSession()->remove('assign_redirect');

                    return $this->redirect($url);
                }

                return $this->redirectToRoute('admin_ph_job_list');
            }
        } else {
            $jobIdsString = $request->query->get('job_ids');
            $form->get('jobIds')->setData($jobIdsString);
        }

        return $this->render('@PH/Admin/Job/select_service.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}