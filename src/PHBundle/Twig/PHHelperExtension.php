<?php
/**
 * User: quaninte
 */

namespace PHBundle\Twig;

use PHBundle\Entity\Job;
use PHBundle\Entity\Repo;
use PHBundle\Entity\Service;
use PHBundle\Entity\TestSuite;
use PHBundle\Manager\BuildManager;
use PHBundle\Manager\JobManager;
use PHBundle\Manager\RepoManager;
use PHBundle\Manager\ServiceManager;
use PHBundle\Manager\TestSuiteManager;

class PHHelperExtension extends \Twig_Extension
{

    /**
     * @var BuildManager
     */
    private $buildManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var RepoManager
     */
    private $repoManager;

    /**
     * @var TestSuiteManager
     */
    private $testSuiteManager;

    /**
     * PHHelperExtension constructor.
     * @param BuildManager $buildManager
     * @param JobManager $jobManager
     * @param RepoManager $repoManager
     * @param TestSuiteManager $testSuiteManager
     */
    public function __construct(BuildManager $buildManager, JobManager $jobManager, RepoManager $repoManager,
                                TestSuiteManager $testSuiteManager)
    {
        $this->buildManager = $buildManager;
        $this->jobManager = $jobManager;
        $this->repoManager = $repoManager;
        $this->testSuiteManager = $testSuiteManager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_job_status', array($this, 'getJobStatus')),
            new \Twig_SimpleFunction('get_job_last_success', array($this, 'getLastSuccessBuildTime')),
            new \Twig_SimpleFunction('get_list_status', array($this, 'getListStatus')),
        );
    }

    /**
     * Get job status
     * @param Job $job
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getJobStatus(Job $job)
    {
        return $this->buildManager->getJobStatus($job);
    }

    /**
     * Get last success build of job
     * @param Job $job
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastSuccessBuildTime(Job $job)
    {
        if (!$build = $this->buildManager->getLastSuccess($job)) {
            return 'Not found';
        }

        return $this->timeAgo($build->getBuildTimestamp());
    }

    /**
     * Get list status for repo and service
     * @param Repo|Service $obj
     * @param $type
     * @param $env
     * @return \PHBundle\Entity\ListStatus
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getListStatus($obj, $type, $env)
    {
        if ($obj instanceof Repo || $obj instanceof Service) {
            if ($type === 'code_build') {
                return $this->jobManager->getJobsStatus($obj->getJobs(), $type, $env);
            } else if ($obj instanceof Service) {
                return $this->testSuiteManager->getServiceStatus($obj, $env);
            }
        }

        throw new \Exception('Not supported');
    }

    /**
     * Get relative time
     * @param $time
     * @return string
     */
    private function timeAgo($time)
    {
        if (!is_numeric($time)) {
            $time = strtotime($time);
        }

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "age");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "100");

        $now = time();

        $difference = $now - $time;
        if ($difference <= 10 && $difference >= 0)
            return $tense = 'just now';
        elseif ($difference > 0)
            $tense = 'ago';
        elseif ($difference < 0)
            $tense = 'later';

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        $period = $periods[$j] . ($difference > 1 ? 's' : '');

        return "{$difference} {$period} {$tense} ";
    }
}