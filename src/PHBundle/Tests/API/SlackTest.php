<?php
/**
 * User: quaninte
 * Date: 10/4/18
 * Time: 7:24 AM
 */

namespace PHBundle\Tests\API;

use Application\Sonata\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * ./bin/phpunit -c app/ src/PHBundle/Tests/API/SlackTest.php
 *
 * Class SlackTest
 * @package PHBundle\Tests\API
 */
class SlackTest extends WebTestCase
{

    public function testNotifyTestSuiteFailure()
    {
        $client = static::createClient();

        $slackAPI = $client->getContainer()->get('ph.api.slack');
        /** @var User $user */
        $user = $client->getContainer()->get('fos_user.user_manager')->findUserByEmail('quan@brodev.com');

        $slackAPI->notifyTestSuiteFailure($user->getSlackId(), 1, 'test.ts', 1, 'test');

    }

}