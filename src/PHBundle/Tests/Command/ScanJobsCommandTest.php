<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\Tests\Command;

use AppBundle\Util\CommandEcho;
use PHBundle\Command\ScanJobsCommand;
use PHBundle\Constants;
use PHBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;


/**
 * To run
 * ./bin/phpunit -c app/ src/PHBundle/Tests/Command/ScanJobsCommandTest.php
 *
 * Class ScanJobsCommandTest
 * @package PHBundle\Tests\Command
 */
class ScanJobsCommandTest extends WebTestCase
{

    /**
     * @param Client
     * @return ScanJobsCommand
     */
    private function createCommand(Client $client)
    {
        $command = new ScanJobsCommand();
        $command->setContainer($client->getContainer());
        $command->initServices();

        return $command;
    }

    public function testAddWebhookNotificationToJob()
    {
        CommandEcho::$enable = true;
        $client = static::createClient();
        $command = $this->createCommand($client);

        $jsCode = 'builder';

        $jenkins = $client->getContainer()->get('ph.manager.jenkins_sites')->getAPIService($jsCode);
        $job = new Job();
        $job->setName('test')
            ->setEnv('dev')
            ->setJenkinsSite($jsCode)
            ->setType(Constants::CODE_BUILD)
            ->setUrl('http://builder.beeketing.com/job/product-hub-test-job/');

        $command->addWebhookNotificationToJob($jenkins, $job);
    }

    public function testAddRepoFromJob()
    {
        CommandEcho::$enable = true;
        $client = static::createClient();
        $command = $this->createCommand($client);

        $jsCode = 'builder';

        $jenkins = $client->getContainer()->get('ph.manager.jenkins_sites')->getAPIService($jsCode);
        $job = new Job();
        $job->setName('test')
            ->setEnv('dev')
            ->setJenkinsSite($jsCode)
            ->setType(Constants::CODE_BUILD)
            ->setUrl('http://builder.beeketing.com/job/product-hub-test-job/');

        $command->addRepoFromJob($jenkins, $job);
    }

}