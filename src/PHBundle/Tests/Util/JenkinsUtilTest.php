<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/19/18
 */

namespace PHBundle\Tests\Util;


use PHBundle\Util\JenkinsUtil;
use PHPUnit\Framework\TestCase;

class JenkinsUtilTest extends TestCase
{

    public function testGetEnvByUrl()
    {
        $env = JenkinsUtil::getEnvByUrl('builder', 'http://builder.beeketing.com/job/Admin-Dev/', 'Admin-Dev');
        $this->assertEquals('dev', $env);
    }

}