<?php
/**
 * User: quaninte
 * Date: 10/4/18
 * Time: 7:36 AM
 */

namespace PHBundle\Tests\RabbitMQ;


use PHBundle\Entity\TestSuite;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProcessReportResultConsumerTest extends WebTestCase
{

    public function testNotifyFailure()
    {
        $client = static::createClient();
        $consumer = $client->getContainer()->get('ph.consumer.process_report_result');

        /** @var TestSuite $testSuite */
        $testSuite = $client->getContainer()->get('ph.manager.test_suite')->getRepository()->find(112);
        $testRun = $testSuite->getLastRun();

        $consumer->notifyFailure($testSuite, $testRun);
    }

}