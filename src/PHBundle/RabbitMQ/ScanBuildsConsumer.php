<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\RabbitMQ;


use AppBundle\Util\CommandEcho;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PHBundle\Entity\Job;
use PHBundle\Manager\BuildManager;
use PHBundle\Manager\JenkinsSiteManager;
use PHBundle\Manager\JobManager;
use PhpAmqpLib\Message\AMQPMessage;

class ScanBuildsConsumer implements ConsumerInterface
{

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var JenkinsSiteManager
     */
    private $jsManager;

    /**
     * @var BuildManager
     */
    private $buildManager;

    /**
     * ScanBuildsConsumer constructor.
     * @param JobManager $jobManager
     * @param JenkinsSiteManager $jsManager
     * @param BuildManager $buildManager
     */
    public function __construct(JobManager $jobManager, JenkinsSiteManager $jsManager, BuildManager $buildManager)
    {
        $this->jobManager = $jobManager;
        $this->jsManager = $jsManager;
        $this->buildManager = $buildManager;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(AMQPMessage $msg)
    {
        $jobId = (int)$msg->getBody();

        /** @var Job|boolean $job */
        $job = $this->jobManager->getRepository()->find($jobId);
        if (!$job) {
            CommandEcho::writeln(sprintf('No job %d found', $jobId));
            return true;
        }
        CommandEcho::writeln(sprintf('Scan builds for job %d', $jobId));

        $jenkins = $this->jsManager->getAPIService($job->getJenkinsSite());

        // Get all builds
        CommandEcho::writeln('Get builds for job: ' . $job->getUrl());
        $builds = $jenkins->getBuilds($job->getUrl());

        // If builds found
        if ($builds) {
            CommandEcho::writeln(sprintf('Found %d builds', count($builds)));

            // Traverse builds
            foreach ($builds as $buildShortData) {
                CommandEcho::writeln('Processing build #' . $buildShortData['number']);
                // If build not existed
                if ($build = $this->buildManager->addOrUpdateBuild($buildShortData, $job)) {
                    CommandEcho::writeln('success');
                } else {
                    CommandEcho::writeln('failed');
                }
            }
        } else {
            CommandEcho::writeln(sprintf('Failed when getting builds'));
        }

        return true;
    }
}