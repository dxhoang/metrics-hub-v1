<?php
/**
 * User: quaninte
 * Date: 10/1/18
 * Time: 7:17 AM
 */

namespace PHBundle\RabbitMQ;


use AppBundle\Util\CommandEcho;
use Application\Sonata\UserBundle\Entity\User;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PHBundle\API\Slack;
use PHBundle\Constants;
use PHBundle\Entity\TestRun;
use PHBundle\Entity\TestSuite;
use PHBundle\Manager\TestRunManager;
use PHBundle\Manager\TestSuiteManager;
use PHBundle\Util\KatalonUtil;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessReportResultConsumer implements ConsumerInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var TestSuiteManager
     */
    private $testSuiteManager;

    /**
     * @var TestRunManager
     */
    private $testRunManager;

    /**
     * @var Slack
     */
    private $slackApi;

    /**
     * ProcessReportResultConsumer constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->testSuiteManager = $this->container->get('ph.manager.test_suite');
        $this->testRunManager = $this->container->get('ph.manager.test_run');
        $this->slackApi = $this->container->get('ph.api.slack');
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(AMQPMessage $msg)
    {
        $data = json_decode($msg->getBody(), true);
        $reportFolder = realpath($this->container->getParameter('kernel.root_dir') . '/../web/reports');
        $pathToZip = $reportFolder . '/zip/' . $data['file_name'];
        $reportContainerDirName = basename($data['run_code'], '.zip') . time() . mt_rand(1000, 9999);
        $targetFolder = $reportFolder . '/data/' . $reportContainerDirName;
        $buildLogURL = isset($data['build_log_url']) ? $data['build_log_url'] : '#';

        // Unzip file
        shell_exec(sprintf('rm -rf %s && unzip "%s" -d "%s"', $targetFolder, $pathToZip, $targetFolder));

        // Read test suite results xml file (JUnit_Report.xml)
        $cmd = sprintf("find '%s' -type f -name 'JUnit_Report.xml'", $targetFolder);
        $reportFilePaths = explode("\n", trim(shell_exec($cmd)));

        // Default file paths
        if (!isset($data['file_paths'])) {
            $data['file_paths'] = array();
        }

        // Traverse report file locations
        foreach ($reportFilePaths as $reportLocation) {
            // Skip reports like (/Reports/20181010_051915/JUnit_Report.xml)
            if (strpos($reportLocation, 'Reports/201') !== false) {
                continue;
            }

            // Clean up
            $baseDirReportLocation = str_replace($targetFolder, '', $reportLocation);
            $baseDirReportLocation = str_replace('/JUnit_Report.xml', '', $baseDirReportLocation);

            // Read report file JUnit_Report.xml
            CommandEcho::writeln(sprintf('Processing %s', $reportLocation));
            CommandEcho::writeln(sprintf('base dir %s', $baseDirReportLocation));

            if (!file_exists($reportLocation)) {
                CommandEcho::writeln('File not found ~> skip');
                continue;
            }

            $xmlContent = file_get_contents($reportLocation);
            $JUnitReportXML = simplexml_load_string($xmlContent);
            foreach ($JUnitReportXML->testsuite as $testSuiteXML) {

                // Find and update test result
                $testId = (string)$testSuiteXML['id'];

                $remove = 'Test Suites/';
                $filePathW = substr($testId, strlen($remove));
                $filePath = $filePathW . '.ts';

                // Remove from file path to mark timeout later
                if (($key = array_search($filePathW, $data['file_paths'])) !== false) {
                    unset($data['file_paths'][$key]);
                }

                if ($testSuite = $this->testSuiteManager->getRepository()->findByFilePath($filePath)) {
                    // Duration
                    $duration = (int)$testSuiteXML['time'];

                    // Failure
                    $errors = isset($testSuiteXML['errors']) ? (int)$testSuiteXML['errors'] : 0;
                    $failures = (int)$testSuiteXML['failures'];

                    // Create test run
                    $testRun = $this->testRunManager->create($testSuite);
                    $testRun->setDuration($duration)
                        ->setErrors($errors)
                        ->setFailures($failures)
                        ->setCode($data['run_code'])
                        ->setReportFolder($reportContainerDirName . '/' . $baseDirReportLocation)
                        ->setBuildLogURL($buildLogURL)
                    ;

                    // If test suite is failed
                    // Check if this is server error or not
                    if (!$testRun->isPassed()) {
                        $testRun->setTestServerError(KatalonUtil::isServerError($xmlContent));
                    } else if ($testRun->getDuration() > (Constants::$timeoutMins * 60)) {
                        // If process run longer than 20m, mark as timeout
                        $testRun->setErrors($testRun->getErrors() + 1)
                            ->setTimeout(true)
                        ;
                    }

                    $this->testRunManager->save($testRun, false);

                    // Save test suite
                    $testSuite
                        ->setLastRun($testRun)
                        ->setStatus(Constants::STATUS_FINISHED) // set finished status
                    ;
                    $this->testSuiteManager->save($testSuite);

                    // Notify slack on failure
                    if (!$testRun->isPassed()) {
                        $this->notifyFailure($testSuite, $testRun);
                    }

                    CommandEcho::writeln(sprintf('`%s` Added new test run id %d', $testId, $testRun->getId()));
                } else {
                    CommandEcho::writeln(sprintf('test suite with file path "%s" not found', $filePath));
                }
            }
        }

        // Mark all pending file paths as timed out
        CommandEcho::writeln(sprintf('Mark as timed out: ' . json_encode($data['file_paths'])));
        foreach ($data['file_paths'] as $filePathW) {
            $filePath = $filePathW . '.ts';

            if ($testSuite = $this->testSuiteManager->getRepository()->findByFilePath($filePath)) {
                $testRun = $this->testRunManager->create($testSuite);
                $testRun->setTimeout(true)
                    ->setCode($data['run_code'])
                    ->setErrors(1)
                    ->setBuildLogURL($data['build_log_url'])
                ;
                $this->testRunManager->save($testRun);

                $testSuite
                    ->setLastRun($testRun)
                    ->setStatus(Constants::STATUS_FINISHED) // set finished status
                ;
                $this->testSuiteManager->save($testSuite);
            }
        }

    }

    /**
     * Notify failure
     * @param TestSuite $testSuite
     * @param TestRun $testRun
     */
    public function notifyFailure(TestSuite $testSuite, TestRun $testRun)
    {
        /** @var User $user */
        foreach ($testSuite->getAllFailureSubscribers() as $user) {
            if ($slackId = $user->getSlackId()) {
                $this->slackApi->notifyTestSuiteFailure($slackId, $testSuite->getId(), $testSuite->getFilePath(),
                    $testRun->getId(), $testRun->getTooltip());
            }
        }
    }

}