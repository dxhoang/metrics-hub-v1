<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\RabbitMQ;


use AppBundle\Util\CommandEcho;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PHBundle\Entity\Job;
use PHBundle\Manager\BuildManager;
use PHBundle\Manager\JenkinsSiteManager;
use PHBundle\Manager\JobManager;
use PhpAmqpLib\Message\AMQPMessage;

class UpdateBuildConsumer implements ConsumerInterface
{

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var JenkinsSiteManager
     */
    private $jsManager;

    /**
     * @var BuildManager
     */
    private $buildManager;

    /**
     * @var Producer
     */
    private $producer;

    /**
     * UpdateBuildConsumer constructor.
     * @param JobManager $jobManager
     * @param JenkinsSiteManager $jsManager
     * @param BuildManager $buildManager
     */
    public function __construct(JobManager $jobManager, JenkinsSiteManager $jsManager, BuildManager $buildManager, Producer $producer)
    {
        $this->jobManager = $jobManager;
        $this->jsManager = $jsManager;
        $this->buildManager = $buildManager;
        $this->producer = $producer;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(AMQPMessage $msg)
    {
        $data = json_decode($msg->getBody(), true);

        if (!isset($data['job_id']) || !isset($data['build_number'])) {
            CommandEcho::writeln('Bad input data: Missing job_id or build_number in message content');
            return true;
        }

        // retry counter
        if (!isset($data['retries'])) {
            $data['retries'] = 0;
        }

        CommandEcho::writeln(sprintf('Updating build %d for job %d', $data['build_number'], $data['job_id']));

        /** @var Job $job */
        $job = $this->jobManager->getRepository()->find($data['job_id']);
        if (!$job) {
            CommandEcho::writeln('Job not found');
            return true;
        }

        $jenkins = $this->jsManager->getAPIService($job->getJenkinsSite());
        $buildData = $jenkins->getBuildByJobUrlAndBuildNumber($job->getUrl(), $data['build_number']);
        if (!$buildData) {
            // Data not found
            CommandEcho::writeln('Data not found');
            return true;
        }

        // If building?
        if ($buildData['building']) {
            // Maximum retry 20 times
            if ($data['retries'] < 20) {
                // Sleep 200ms then requeue
                usleep(500);
                $data['retries']++;
                $this->producer->publish(json_encode($data));

                return true;
            }

            // Else use building
        }

        if (!$this->buildManager->addOrUpdateBuild($buildData, $job, true, true)) {
            CommandEcho::writeln('Failed');
            return false;
        }

        CommandEcho::writeln('Success');
        return true;
    }
}