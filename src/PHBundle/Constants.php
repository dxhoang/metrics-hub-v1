<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace PHBundle;


class Constants
{

    /**
     * List of jenkins sites
     * @var array
     */
    public static $jenkinsSites = array(
        'cicd' => 'cicd.beeketing.com',
        'builder' => 'builder.beeketing.com',
    );

    /**
     * List of environments
     * @var array
     */
    public static $ENVs = array(
        'dev' => 'Dev',
        'staging' => 'Staging',
        'prod' => 'Production',
    );

    /**
     * List of job types
     * @var array
     */
    const AUTOMATION_TEST = 'automation_test';
    const CODE_BUILD = 'code_build';

    public static $jobTypes = array(
        'automation_test' => 'Automation Test',
        'code_build' => 'Code Build',
    );

    /**
     * Keyword to mark test run as failed
     * @var array
     */
    public static $testServerErrorKeywords = array(
        'marionette',
        'Could not start a new session',
        'Failed to interpret value as array',
    );

    /**
     * If duration longer than 20m will be marked as timed out
     * @var int
     */
    public static $timeoutMins = 20;

    /**
     * Test suite statuses
     */
    const STATUS_RUNNING = 'running';
    const STATUS_FINISHED = 'finished';
    const STATUS_QUEUED = 'queued';

    public static function getStatusOptions()
    {
        return array(
            self::STATUS_FINISHED => self::STATUS_FINISHED,
            self::STATUS_RUNNING => self::STATUS_RUNNING,
            self::STATUS_QUEUED => self::STATUS_QUEUED,
        );
    }

}