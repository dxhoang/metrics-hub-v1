<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RepoAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('link')
            ->add('uri')
            ->add('scanTestSuite')
            ->add('services', null, array(
                'by_reference' => false,
            ))
            ->add('jobs', null, array(
                'by_reference' => false,
            ))
            ->add('katalonJenkinsJobURLProd')
            ->add('katalonJenkinsJobURLStaging')
            ->add('katalonJenkinsJobURLParallel')
            ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('services')
            ->add('scanTestSuite')
            ->add('build_prod', 'string' , array(
                'template' => '@PH/Admin/BuildStatus/build_prod.html.twig',
            ))
            ->add('build_staging', 'string' , array(
                'template' => '@PH/Admin/BuildStatus/build_staging.html.twig',
            ))
            ->add('build_dev', 'string' , array(
                'template' => '@PH/Admin/BuildStatus/build_dev.html.twig',
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'jobs' => array(
                        'template' =>  '@PH/Admin/Repo/actions.html.twig',
                    ),
                ),
            ))
            ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('name')
            ->add('link')
            ->add('uri')
            ->add('services')
            ->add('jobs')
            ->add('scanTestSuite')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('services')
            ->add('scanTestSuite')
            ;
    }


}