<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Admin;


use PHBundle\Constants;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class JobAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('repos')
            ->add('services')
            ->add('type')
            ->add('env')
            ->add('status', 'string' , array(
                'template' => '@PH/Admin/list_status.html.twig',
            ))
            ->add('jenkinsSite')
            ->add('_action', null, array(
                'actions' => array(
                    'view' => array(
                        'template' =>  '@PH/Admin/Job/actions.html.twig',
                    ),
                ),
            ))
            ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('repos', null, array(
                'by_reference' => false,
            ))
            ->add('services', null, array(
                'by_reference' => false,
            ))
            ->add('url')
            ->add('env', 'choice', array(
                'choices' => Constants::$ENVs,
            ))
            ->add('jenkinsSite', 'choice', array(
                'choices' => Constants::$jenkinsSites,
            ))
            ->add('type', 'choice', array(
                'choices' => Constants::$jobTypes,
            ))
            ->add('failureSubscribers')
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('repos')
            ->add('services')
            ->add('type')
            ->add('env')
            ;
    }

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();
        $actions['assign_service'] = array(
            'label' => 'Assign To Service',
            'ask_confirmation' => false,
        );

        return $actions;
    }

}