<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 12:03 AM
 */

namespace PHBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class TestCaseAdmin extends AbstractAdmin
{

    protected $baseRoutePattern = 'testcase';
    protected $baseRouteName = 'testcase';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list','create']);
    }


}