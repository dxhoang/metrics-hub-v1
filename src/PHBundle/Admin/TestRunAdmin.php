<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 12:03 AM
 */

namespace PHBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TestRunAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt'
    );

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->tab('default')
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('code')
            ->add('testSuite', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('passed', 'boolean')
            ->add('failures')
            ->add('errors')
            ->add('timeout', 'boolean')
            ->add('testServerError', 'boolean')
            ->add('duration')
            ->add('finishedTime', 'string' , array(
                'template' => '@PH/Admin/TestRun/run_time.html.twig',
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'children' => array(
                        'template' =>  '@PH/Admin/TestRun/actions.html.twig',
                    ),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('code')
            ->add('testSuite', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('passed', 'boolean')
            ->add('failures')
            ->add('errors')
            ->add('timeout', 'boolean')
            ->add('testServerError', 'boolean')
            ->add('duration')
            ->add('createdAt')
            ->add('_action', 'actions', array(
                'template' =>  '@PH/Admin/TestRun/actions.html.twig',
            ))
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('code')
            ->add('testSuite')
            ->add('failures')
            ->add('errors')
            ->add('timeout')
            ->add('testServerError')
            ->add('duration')
        ;
    }

}