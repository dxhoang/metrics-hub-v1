<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Admin;


use PHBundle\Entity\Build;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class BuildAdmin extends AbstractAdmin
{

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct($code, $class, $baseControllerName, TokenStorage $tokenStorage)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->tokenStorage = $tokenStorage;
    }

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'buildTimestamp',
    );

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('number')
            ->add('job')
            ->add('status', 'string', array(
                'template' => '@PH/Admin/Build/status.html.twig',
            ))
            ->add('note')
            ->add('buildDateTime')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'note' => array(
                        'template' =>  '@PH/Admin/Build/note.html.twig',
                    ),
                ),
            ))
            ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        // Auto add placeholder when adding note
        if ($this->getRequest()->query->get('add_note')) {
            /** @var Build $build */
            $build = $this->getSubject();

            $note = $build->getNote();
            $note .= "\n";
            $now = new \DateTime();
            $note .= $this->tokenStorage->getToken()->getUsername() . ' - ' . $now->format('Y-m-d h:i') . ' - ';

            $build->setNote($note);
        }

        $form
            ->add('job')
            ->add('number')
            ->add('status')
            ->add('note')
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('job')
        ;
    }

}