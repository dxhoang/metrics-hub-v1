<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 12:03 AM
 */

namespace PHBundle\Admin;


use PHBundle\Constants;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TestSuiteAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt'
    );

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('code')
            ->add('env')
            ->add('service')
            ->add('repo')
            ->add('filePath')
            ->add('active')
            ->add('failureSubscribers')
            ->add('trelloTask')
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('env')
            ->add('service')
            ->add('repo')
            ->add('active')
            ->add('lastDuration')
            ->add('lastRun', 'string' , array(
                'template' => '@PH/Admin/TestSuite/last_run.html.twig',
            ))
            ->add('status', 'string' , array(
                'template' => '@PH/Admin/TestSuite/status.html.twig',
            ))
            ->add('filePath')
            ->add('updatedAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'children' => array(
                        'template' =>  '@PH/Admin/TestSuite/actions.html.twig',
                    ),
                ),
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('code')
            ->add('env')
            ->add('active')
            ->add('service')
            ->add('filePath')
            ->add('repo')
            ->add('status')
            ->add('lastRun.timeout')
            ->add('lastRun.duration')
            ->add('lastRun.errors')
            ->add('lastRun.failures')
        ;
    }

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();
        $actions['queue_to_run_tests'] = array(
            'label' => 'Queue to run tests',
            'ask_confirmation' => false,
        );

        return $actions;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('name')
            ->add('env')
            ->add('service')
            ->add('active')
            ->add('status')
            ->add('lastDuration')
            ->add('lastRun', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('lastRunStatus', 'string' , array(
                'template' => '@PH/Admin/TestSuite/last_run.html.twig',
            ))
            ->add('filePath')
            ->add('updatedAt')
            ->add('createdAt')
            ->add('_action', 'actions', array(
                'template' =>  '@PH/Admin/TestSuite/actions.html.twig',
            ))
            ;
    }

}