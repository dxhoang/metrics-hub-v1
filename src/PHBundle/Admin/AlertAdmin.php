<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/30/18
 */

namespace PHBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AlertAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('code', null, array(
                'route' => array(
                    'name' => 'show',
                ),
            ))
            ->add('subscribers')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'children' => array(
                        'template' =>  '@PH/Admin/Alert/actions.html.twig',
                    ),
                ),
            ))
        ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('code')
            ->add('subscribers')
            ->add('message')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('code')
            ->add('subscribers')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('code')
            ->add('message')
            ->add('subscribers')
            ->add('subscribersWithPhone', 'html')
        ;
    }

}