<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/12/18
 */

namespace PHBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class ServiceAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('test_prod', 'string' , array(
                'template' => '@PH/Admin/BuildStatus/test_prod.html.twig',
            ))
            ->add('test_staging', 'string' , array(
                'template' => '@PH/Admin/BuildStatus/test_staging.html.twig',
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'children' => array(
                        'template' =>  '@PH/Admin/Service/actions.html.twig',
                    ),
                ),
            ))
            ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('repos', null, array(
                'by_reference' => false,
            ))
            ->add('jobs', null, array(
                'by_reference' => false,
            ))
            ->add('testSuites', null, array(
                'by_reference' => false,
            ))
            ->add('tags', null, array(
                'help' => 'comma (,) separated',
                'required' => false,
            ))
            ->add('failureSubscribers')
            ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('name')
            ->add('repos')
            ->add('tags')
        ;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $menu->addChild('Queue ALL service tests STAGING', array(
            'route' => 'admin_service_run_test',
            'routeParameters' => array(
                'serviceId' => 'all',
                'env' => 'staging',
            ),
        ));
        $menu->addChild('Queue ALL service tests PROD', array(
            'route' => 'admin_service_run_test',
            'routeParameters' => array(
                'serviceId' => 'all',
                'env' => 'prod',
            ),
        ));
    }

}