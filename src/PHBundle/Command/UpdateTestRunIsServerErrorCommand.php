<?php
/**
 * User: quaninte
 * Date: 10/10/18
 * Time: 1:08 AM
 */

namespace PHBundle\Command;


use PHBundle\Entity\TestRun;
use PHBundle\Util\KatalonUtil;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateTestRunIsServerErrorCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('update-test-run-is-server-error')
            ->addOption('time-range', 't', InputOption::VALUE_OPTIONAL, 'Time range to find test suites', '-15 days')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Vars
        $reportFolder = $this->getContainer()->getParameter('kernel.root_dir') . '/../web/reports/data/';

        // Find test suites
        $testRunManager = $this->getContainer()->get('ph.manager.test_run');

        $timeRange = $input->getOption('time-range');
        if ($timeRange === 'all') {
            $output->writeln('All time');
            $testRuns = $testRunManager->getRepository()->findAll();
        } else {
            $minTime = new \DateTime();
            $minTime->modify($timeRange);
            $output->writeln('From ' . $minTime->format('r'));

            $testRuns = $testRunManager->getRepository()
                ->createQueryBuilder('tr')
                ->where('tr.createdAt >= :minTime')
                ->setParameter('minTime', $minTime)
                ->getQuery()
                ->execute()
            ;
        }

        $i = 0;

        $output->writeln(sprintf('Found %d test runs', count($testRuns)));

        /** @var TestRun $testRun */
        foreach ($testRuns as $testRun) {
            $output->writeln(sprintf('Process for test suite %s run code %s', $testRun->getTestSuite()->getFilePath(), $testRun->getCode()));
            // Test xml
            $reportLocation = $reportFolder . $testRun->getReportConsoleLogPath();
            $output->writeln(sprintf('Processing %s', $reportLocation));

            $xmlContent = file_get_contents($reportLocation);

            // Set is serve error
            $testRun->setTestServerError(KatalonUtil::isServerError($xmlContent));

            // Fix report location
            $remove = '/JUnit_Report.xml';
            if (strpos($testRun->getReportFolder(), $remove) !== false) {
                $testRun->setReportFolder(str_replace($remove, '', $testRun->getReportFolder()));
            }

            $output->writeln('Is server error: ' . ($testRun->getTestServerError() ? 'true' : 'false'));

            // Update
            $testRunManager->save($testRun, false);

            if ($i === 100) {
                $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
                $i = 0;
            } else {
                $i++;
            }
        }

        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
    }

}