<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace PHBundle\Command;


use PHBundle\Entity\Build;
use PHBundle\Manager\BuildManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateOldBuildsCommand extends ContainerAwareCommand
{

    /**
     * @var BuildManager
     */
    private $buildManager;

    protected function configure()
    {
        $this->setName('update-old-builds');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->buildManager = $this->getContainer()->get('ph.manager.build');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $qb = $this->buildManager->getRepository()->createQueryBuilder('b');
        $builds = $qb->where('b.status = :status AND b.buildTimestamp <= :minTimestamp')
            ->setParameter('status', 'BUILDING')
            ->setParameter('minTimestamp', (new \DateTime())->modify('-1 hour')->getTimestamp())
            ->getQuery()
            ->getResult()
            ;

        $output->writeln(sprintf('Found %d old building jobs', count($builds)));

        /** @var Build $build */
        foreach ($builds as $build) {
            $build->setStatus('FAILURE');
            $this->buildManager->save($build);
        }
    }

}