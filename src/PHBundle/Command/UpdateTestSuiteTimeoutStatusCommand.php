<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 11/12/18
 */

namespace PHBundle\Command;


use PHBundle\Constants;
use PHBundle\Manager\TestSuiteManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateTestSuiteTimeoutStatusCommand extends ContainerAwareCommand
{

    /**
     * @var TestSuiteManager
     */
    private $testSuiteManager;

    protected function configure()
    {
        $this->setName('update-test-suite-timeout-status');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->testSuiteManager = $this->getContainer()->get('ph.manager.test_suite');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Find all test suite that is with status running or queued

        // If last updated is > 20 mins ~> set to finished
        $testSuites = $this->testSuiteManager->getRepository()->findTimedOutStatus();

        $output->writeln(sprintf('Updating %d test suites', count($testSuites)));
        foreach ($testSuites as $testSuite) {
            $this->testSuiteManager->setStatus($testSuite, Constants::STATUS_FINISHED);
        }

        $this->testSuiteManager->flush();
        $output->writeln('Done');
    }

}