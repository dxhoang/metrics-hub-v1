<?php
/**
 * User: quaninte
 * Date: 9/20/18
 * Time: 2:33 AM
 */

namespace PHBundle\Command;


use AppBundle\Util\CommandEcho;
use PHBundle\API\Jenkins;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateKatalonJobsCommand extends ContainerAwareCommand
{

    /**
     * @var Jenkins
     */
    private $jenkins;

    protected function configure()
    {
        $this->setName('update-katalon-jobs')
            ->addOption('update-command', 'c', InputOption::VALUE_OPTIONAL,
                'Update shell command', false)
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->jenkins = $this->getContainer()->get('ph.manager.jenkins_sites')->getAPIService('builder');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Twig_Error
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $beforeKeywordOpt1 = 'TEST_SUITE="';
        $afterKeywordOp1 = '"';

        $beforeKeywordOpt2 = 'TEST_SUITE=&quot;';
        $afterKeywordOp2 = '&quot;';

        $chainProjects = array();

        // Get all jobs in builder that is katalon
        foreach ($this->jenkins->getJobs('http://builder.beeketing.com/job/katalon/') as $jenkinsJob) {
            $beforeKeyword = $beforeKeywordOpt1;
            $afterKeyword = $afterKeywordOp1;

            CommandEcho::writeln('Processing job ' . $jenkinsJob['url']);

            // Skip if not katalon
            if (strpos($jenkinsJob['url'], 'katalon') === false) {
                CommandEcho::writeln('Skip not katalon');
                continue;
            }

            // Read config.xml
            $xmlConfig = $this->jenkins->getJobConfigFile($jenkinsJob['url']);

            // Get test suite name
            $beforeKeywordPos = strpos($xmlConfig, $beforeKeyword);
            if ($beforeKeywordPos === false) {
                $beforeKeyword = $beforeKeywordOpt2;
                $afterKeyword = $afterKeywordOp2;

                // Try alternative keyword
                $beforeKeywordPos = strpos($xmlConfig, $beforeKeyword);
                if ($beforeKeywordPos === false) {
                    // Skip if not found keyword
                    CommandEcho::writeln('Skip not found keyword');
                    continue;
                }
            }

            $startPos = $beforeKeywordPos + strlen($beforeKeyword);
            $endPos = strpos($xmlConfig, $afterKeyword, $startPos);
            $testSuiteName = substr($xmlConfig, $startPos, $endPos - $startPos);
            CommandEcho::writeln('Found name: ' . $testSuiteName);

            // Set test suite name as description
            $parsedConfig = simplexml_load_string($xmlConfig);
            $parsedConfig->description = $testSuiteName;


            // Update command
            if ($input->getOption('update-command')) {
                $output->writeln('Updating command');
                // Update shell command
                $shellCommand = $this->getContainer()->get('templating')->render('@PH/Katalon/shell_script.txt.twig', array(
                    'testSuiteName' => $testSuiteName,
                ));

                $parsedConfig->builders->{'hudson.tasks.Shell'}->command = $shellCommand;
            }

            // Build servers
            $parsedConfig->assignedNode = 'builder-slave-02 || builder-slave-03';

            // Set timeout within 20 mins
            // If config existed hudson.plugins.build__timeout.BuildTimeoutWrapper->strategy
            if (
                isset($parsedConfig->buildWrappers) &&
                isset($parsedConfig->buildWrappers->{'hudson.plugins.build__timeout.BuildTimeoutWrapper'})
            ) {
                // remove
                unset($parsedConfig->buildWrappers->{'hudson.plugins.build__timeout.BuildTimeoutWrapper'});
            }

            // Update config
            $child = $parsedConfig->buildWrappers->addChild('hudson.plugins.build__timeout.BuildTimeoutWrapper');
            $child->addAttribute('plugin', 'build-timeout@1.17.1');

            $child = $child->addChild('strategy');
            $child->addAttribute('class', 'hudson.plugins.build_timeout.impl.AbsoluteTimeOutStrategy');

            $child->addChild('timeoutMinutes', 20);

            // Artifact Archiver
            if (isset($parsedConfig->publishers->{'hudson.tasks.ArtifactArchiver'})) {
                $artTree = $parsedConfig->publishers->{'hudson.tasks.ArtifactArchiver'};
            } else {
                $artTree = $parsedConfig->publishers->addChild('hudson.tasks.ArtifactArchiver');
            }
            $artTree->artifacts = 'Reports/*';
            $artTree->allowEmptyArchive = false;
            $artTree->onlyIfSuccessful = false;
            $artTree->fingerprint = false;
            $artTree->defaultExcludes = true;
            $artTree->caseSensitive = true;

            // Print if found chain
            if (isset($parsedConfig->publishers->{'hudson.tasks.BuildTrigger'})) {
                $chainProjects[] = $jenkinsJob['url'];
            }

            // Update config.xml
            if (!$this->jenkins->updateJobConfigFile($jenkinsJob['url'], $parsedConfig->asXML())) {
                CommandEcho::writeln('Failed to update config.xml');
            }
        }

        $output->writeln('=====');
        $output->writeln('Finished');
        $output->writeln('=====');
        $output->writeln(count($chainProjects) . ' Chain builds detected jobs');

        $i = 0;
        foreach ($chainProjects as $projectUrl) {
            $i++;
            $output->writeln($i . '. ' . $projectUrl);
        }
    }

}