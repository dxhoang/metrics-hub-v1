<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 12:52 AM
 */

namespace PHBundle\Command;


use AppBundle\Util\CommandEcho;
use Doctrine\ORM\EntityManager;
use PHBundle\Entity\Repo;
use PHBundle\Entity\TestSuite;
use PHBundle\Manager\RepoManager;
use PHBundle\Manager\TestSuiteManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScanTestCommand extends ContainerAwareCommand
{

    /**
     * @var TestSuiteManager
     */
    private $testSuiteManager;


    /**
     * @var RepoManager
     */
    private $repoManager;


    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this->setName('scan-test')
            ->addOption('update-repo', 'u', InputOption::VALUE_OPTIONAL,
                'Update git repo', true)
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->testSuiteManager = $this->getContainer()->get('ph.manager.test_suite');
        $this->repoManager = $this->getContainer()->get('ph.manager.repo');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get all test suite repos
        $repos = $this->repoManager->getRepository()->findByScanTestSuite();

        $output->writeln(sprintf('Found %d repos', count($repos)));

        /** @var Repo $repo */
        foreach ($repos as $repo) {
            $this->scanRepo($input, $output, $repo);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param Repo $repo
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function scanRepo(InputInterface $input, OutputInterface $output, Repo $repo)
    {
        // List of test found to filter and delete later
        $foundTests = array();

        $output->writeln(sprintf('Scanning uri %s', $repo->getUri()));

        $target = $this->getContainer()->getParameter('kernel.root_dir') . '/../repos/' . self::slugify($repo->getUri());

        // Clone or update git repo
        if ($input->getOption('update-repo')) {
            $this->getContainer()->get('ph.util.git')->cloneOrUpdateRepo($repo->getUri(), $target);
        }

        // Scan all test cases
        $files = array();
        $this->getDirContents($target . DIRECTORY_SEPARATOR . 'Test Suites', $files, 'ts');
        foreach ($files as $file) {
            $output->writeln(sprintf('Processing file %s', $file));

            // Read xml content
            $parsedXML = simplexml_load_string(file_get_contents($file));

            if (!$parsedXML->testSuiteGuid) {
                $output->writeln('Skip because no code');
                continue;
            }
            $output->writeln('code: ' . $parsedXML->testSuiteGuid);

            $parts = explode('Test Suites', $file, 2);
            $shortPath = trim($parts[1], '/');

            // Add to found tests list
            if (!in_array($shortPath, $foundTests)) {
                $foundTests[] = $shortPath;
            }

            /** @var TestSuite $testSuite */
            $testSuite = $this->testSuiteManager->getRepository()->findByFilePath($shortPath);

            if ($testSuite && !$testSuite->getRepo()) {
                // Add repo if repo is not set
                $testSuite->setRepo($repo);

                $this->em->persist($testSuite);
            } else if (!$testSuite || $testSuite->getRepo()->getId() != $repo->getId()) {
                // If repo is different, make sure file path and this repo not existed
                if ($testSuite) {
                    $testSuite = $this->testSuiteManager->getRepository()->findOneBy(array(
                        'filePath' => $testSuite->getFilePath(),
                        'repo' => $repo,
                    ));
                }

                // Add repo is different or test suite not existed
                if (!$testSuite) {
                    $testSuite = new TestSuite();
                    $testSuite
                        ->setName($parsedXML->name)
                        ->setCode($parsedXML->testSuiteGuid)
                        ->setFilePath($shortPath)
                        ->setRepo($repo)
                    ;

                    $this->em->persist($testSuite);

                    $output->writeln('Not found ~> adding.');
                }
            }
        }

        $this->em->flush();

        // Now delete all test suites that not in found tests list
        $this->removeDeletedTestSuites($repo, $foundTests);
    }

    /**
     * Remove deleted test suites
     * @param Repo $repo
     * @param array $foundTests
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function removeDeletedTestSuites(Repo $repo, $foundTests = array())
    {
        CommandEcho::writeln(sprintf('Removed delete test suites for repo %s', $repo->getName()));
        $deleted = 0;

        // Get all test suites in this repos
        /** @var TestSuite $testSuite */
        foreach ($repo->getTestSuites() as $testSuite) {
            // If not in found tests
            if (!in_array($testSuite->getFilePath(), $foundTests)) {
                // Delete
                $this->em->remove($testSuite);
                $deleted++;
            }
        }

        $this->em->flush();

        CommandEcho::writeln('Deleted ' . $deleted);
    }

    /**
     * GEt dir contents
     * @param $dir
     * @param array $results
     * @param string $ext
     * @return array
     */
    private function getDirContents($dir, &$results = array(), $ext = '*')
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (is_dir($path) && $value != "." && $value != "..") {
                $this->getDirContents($path, $results, $ext);
            } else if ($ext === '*' || pathinfo($path, PATHINFO_EXTENSION) === $ext) {
                $results[] = $path;
            }
        }

        return $results;
    }

    /**
     * @param $file
     * @return \SimpleXMLElement
     */
    public function getTestCaseNameByFile($file)
    {
        return simplexml_load_string(file_get_contents($file))->name;
    }

    /**
     * Slugify
     * @param $text
     * @return null|string|string[]
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}