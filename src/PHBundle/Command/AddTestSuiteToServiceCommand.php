<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/1/18
 */

namespace PHBundle\Command;

use PHBundle\Entity\Service;
use PHBundle\Entity\TestSuite;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AddTestSuiteToServiceCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('add-test-suites-to-service')
            ->addOption('reassign', 'r', InputOption::VALUE_OPTIONAL,
                'Override current test suite service')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Managers
        $serviceManager = $this->getContainer()->get('ph.manager.service');
        $testSuiteManager = $this->getContainer()->get('ph.manager.test_suite');

        $services = $serviceManager->getRepository()->findAll();

        // Find all test suites without service
        if ($input->getOption('reassign')) {
            $testSuites = $testSuiteManager->getRepository()->findAll();
        } else {
            $testSuites = $testSuiteManager->getRepository()->findNoService();
        }

        $output->writeln(sprintf('Found %d test suites', count($testSuites)));

        /** @var TestSuite $testSuite */
        foreach ($testSuites as $testSuite) {
            // Fix env
            if (!$testSuite->getEnv()) {
                $testSuite->setFilePath($testSuite->getFilePath());
                $testSuiteManager->save($testSuite);
            }

            $found = false;

            $output->writeln(sprintf('TS: ' . $testSuite->getFilePath()));
            // Traverse all services
            /** @var Service $service */
            foreach ($services as $service) {
                // Traverse all tags
                foreach ($service->getTagsArray() as $tag) {
                    // If tag found in link
                    if (strpos(strtolower($testSuite->getFilePath()), $tag) !== false) {
                        // Set service for test suite
                        $testSuite->setService($service);

                        // Save
                        $testSuiteManager->save($testSuite);
                        $output->writeln(sprintf('Assign to service: %s', $service->getName()));

                        $found = true;
                        break;
                    }
                }

                if ($found) {
                    break;
                }
            }
        }

    }

}