<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/1/18
 */

namespace PHBundle\Command;


use PHBundle\Controller\ServiceController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StartTestCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('start-all-test')
            ->addArgument('env', InputOption::VALUE_REQUIRED, 'env to start test')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Twig_Error
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ServiceController $serviceController */
        $serviceController = $this->getContainer()->get('ph.controller.service');

        $serviceController->runTestAction('all', $input->getArgument('env'));
    }

}