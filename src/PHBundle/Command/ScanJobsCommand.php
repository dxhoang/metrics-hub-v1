<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace PHBundle\Command;

use AppBundle\Util\CommandEcho;
use Doctrine\ORM\EntityManager;
use PHBundle\API\Jenkins;
use PHBundle\Constants;
use PHBundle\Entity\Job;
use PHBundle\Manager\JenkinsSiteManager;
use PHBundle\Manager\JobManager;
use PHBundle\Manager\RepoManager;
use PHBundle\Util\JenkinsUtil;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScanJobsCommand extends ContainerAwareCommand
{

    /**
     * @var JenkinsSiteManager
     */
    private $jenkinsSiteManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var string
     */
    private $webhookContent = '';

    /**
     * @var RepoManager
     */
    private $repoManager;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Post build script tag
     */
    const PostBuildScriptTag = 'org.jenkinsci.plugins.postbuildscript.PostBuildScript';
    const PostBuildScriptShellTag = 'hudson.tasks.Shell';

    protected function configure()
    {
        $this->setName('scan-jobs')
            ->addOption('update-all-webhook-config', 'w', InputOption::VALUE_OPTIONAL,
                'Update webhook config along the way', false)
        ;
    }

    public function initServices()
    {
        $this->jenkinsSiteManager = $this->getContainer()->get('ph.manager.jenkins_sites');
        $this->jobManager = $this->getContainer()->get('ph.manager.job');
        $this->repoManager = $this->getContainer()->get('ph.manager.repo');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->initServices();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Traverse jenkins sites
        foreach (Constants::$jenkinsSites as $siteCode => $siteName) {
            $jenkins = $this->jenkinsSiteManager->getAPIService($siteCode);

            $this->processJobs($jenkins, $jenkins->getJobs(), $output, $input->getOption('update-all-webhook-config'));
        }
    }

    /**
     * @param Jenkins $jenkins
     * @param array $jenkinsJobs
     * @param OutputInterface $output
     * @param bool $updateOldJobWebhook
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function processJobs(Jenkins $jenkins, array $jenkinsJobs, OutputInterface $output, $updateOldJobWebhook = false) {
        $output->writeln('Processing for ' . count($jenkinsJobs) . ' jobs');

        // Traverse jobs in site
        /** @var array $jenkinsJob */
        foreach ($jenkinsJobs as $jenkinsJob) {
            if ($jenkins->isFolder($jenkinsJob['_class'])) {
                $output->writeln('Found folder ' . $jenkinsJob['name']);
                $this->processJobs($jenkins, $jenkins->getJobs($jenkinsJob['url']), $output, $updateOldJobWebhook);
            } else {
                // If not in our database
                if (!$job = $this->jobManager->getRepository()->findOneByURL($jenkinsJob['url'])) {
                    $output->writeln('Adding job ' . $jenkinsJob['name']);
                    // Add new job
                    $jobData = $jenkins->getObject($jenkinsJob['url']);
                    $job = $this->jobManager->createFromJenkinsJob($jenkinsJob['name'], $jenkinsJob['url'], $jobData['fullDisplayName']);

                    // Update webhook config for new job
                    if ($job) {
                        $this->addWebhookNotificationToJob($jenkins, $job);
                        $this->addRepoFromJob($jenkins, $job);
                    }
                } else {
                    // If env is empty, recheck
                    if (!$job->getEnv()) {
                        $job->setEnv(JenkinsUtil::getEnvByUrl($job->getJenkinsSite(), $job->getUrl(), $job->getName()));
                        $this->jobManager->save($job);
                    }

                    $output->writeln('Job existed ' . $jenkinsJob['name']);

                    // If need update job config
                    if ($updateOldJobWebhook) {
                        $this->addWebhookNotificationToJob($jenkins, $job);
                        $this->addRepoFromJob($jenkins, $job);
                    }
                }
            }
        }
    }

    /**
     * Add webhook notification to job
     * @param Jenkins $jenkins
     * @param Job $job
     * @return bool
     */
    public function addWebhookNotificationToJob(Jenkins $jenkins, Job $job)
    {
        // Skip for cicd
        if ($job->getJenkinsSite() === 'cicd') {
            CommandEcho::writeln('Don\'t update for cicd site ' . $job->getUrl());
            return true;
        }

        // Get job config.xml
        $xmlConfig = $jenkins->getJobConfigFile($job->getUrl());

        // Parse xml
        $parsedConfig = simplexml_load_string($xmlConfig);

        // If notification tag existed
        $existed = false;
        if (isset($parsedConfig->publishers->{self::PostBuildScriptTag})
            && isset($parsedConfig->publishers->{self::PostBuildScriptTag}->buildSteps)
            && isset($parsedConfig->publishers->{self::PostBuildScriptTag}->buildSteps->{self::PostBuildScriptShellTag})
        ) {
            /** @var \SimpleXMLElement $step */
            foreach ($parsedConfig->publishers->{self::PostBuildScriptTag}->buildSteps as $step) {
                foreach($step as $item) {
                    if (isset($item->command) && strpos($item->command, 'Product Hub Webhook Block') !== false) {
                        $existed = true;
                        $item->command = $this->getWebhookContent();

                        break;
                    }
                }

                // Skip if found
                if ($existed) {
                    break;
                }
            }
        }

        // Add if not found
        if (!$existed) {
            // Add post build scrip tag
            if (!isset($parsedConfig->publishers->{self::PostBuildScriptTag})) {
                $parent = $parsedConfig->publishers->addChild(self::PostBuildScriptTag);
            } else {
                $parent = $parsedConfig->publishers->{self::PostBuildScriptTag};
            }

            // Add build steps
            if (!isset($parent->buildSteps)) {
                $parent = $parent->addChild('buildSteps');
            } else {
                $parent = $parent->buildSteps;
            }

            // Add post build script shell tag
            $parent = $parent->addChild(self::PostBuildScriptShellTag);
            $parent->command = $this->getWebhookContent();
        }

        // Update config
        if ($jenkins->updateJobConfigFile($job->getUrl(), $parsedConfig->asXML())) {
            return true;
        }

        return false;
    }

    /**
     * Get webhook content
     * @return string
     */
    private function getWebhookContent()
    {
        if ($this->webhookContent) {
            return $this->webhookContent;
        }

        $this->webhookContent = file_get_contents(__DIR__ . '/Webhook/build_webhook.txt');
        return $this->webhookContent;
    }

    /**
     * Add repo from job
     * @param Jenkins $jenkins
     * @param Job $job
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addRepoFromJob(Jenkins $jenkins, Job $job)
    {
        CommandEcho::writeln('Finding repo for job ' . $job->getUrl());
        // Get .xml config
        // Get job config.xml
        $xmlConfig = $jenkins->getJobConfigFile($job->getUrl());

        // Get all git repos in config file
        preg_match_all("|git@bitbucket.org:(.*)\.git|U", $xmlConfig, $matches, PREG_PATTERN_ORDER);

        // Traverse all repos
        foreach ($matches[1] as $repoName) {
            // Build Url
            $link = 'https://bitbucket.org/' . $repoName;
            CommandEcho::writeln('Found repo: ' . $link);

            // If repo not existed
            if (!$repo = $this->repoManager->getRepository()->findByLink($link)) {
                // Set name & link
                // Create repo
                $repo = $this->repoManager->createWithNameUrl($repoName, $link);

                // Link job with repo
                $job->addRepo($repo);

                $this->em->persist($repo);
                $this->em->persist($job);
            } else {
                // Else
                // If relation with job <> repo not existed
                if (!$job->getRepos()->contains($repo)) {
                    // Link job with repo
                    $job->addRepo($repo);

                    $this->repoManager->save($repo);

                    $this->em->persist($repo);
                    $this->em->persist($job);
                }
            }
        }

        // Flush to db
        $this->em->flush();
    }

}