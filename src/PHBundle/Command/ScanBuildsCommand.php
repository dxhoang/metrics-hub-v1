<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\Command;


use PHBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScanBuildsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('scan-builds');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get all jobs
        $jobs = $this->getContainer()->get('ph.manager.job')->getRepository()->findAll();

        $output->writeln(sprintf('Found %d jobs', count($jobs)));

        /** @var Job $job */
        foreach ($jobs as $job) {
            // Queue job to scan builds
            $output->writeln(sprintf('Queue to get builds for job %d', $job->getId()));
            $this->getContainer()->get('old_sound_rabbit_mq.scan_builds_producer')->publish($job->getId());
        }
    }

}