<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace PHBundle\Command;


use Application\Sonata\UserBundle\Entity\User;
use FOS\UserBundle\Entity\UserManager;
use PHBundle\API\Slack;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScanSlackUsersCommand extends ContainerAwareCommand
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var Slack
     */
    private $slackApi;

    protected function configure()
    {
        $this->setName('scan-slack-users');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->userManager = $this->getContainer()->get('fos_user.user_manager');
        $this->slackApi = $this->getContainer()->get('ph.api.slack');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get all users using slack api
        $users = $this->slackApi->getUsers();

        // Traverse
        foreach ($users as $userData) {
            if (!isset($userData['profile']['email']) || !$userData['profile']['email'] || $userData['deleted']) {
                continue;
            }

            // If user with email Not existed
            /** @var User $user */
            if (!$user = $this->userManager->findUserByEmail($userData['profile']['email'])) {
                // Add user with slack id
                $user = $this->userManager->createUser();
                $user->setEmail($userData['profile']['email']);
                $user->setUsername($userData['profile']['email']);


                if (!isset($userData['profile']['first_name'])) {
                    $nameParts = explode(' ', $userData['real_name'], 2);
                    if (!isset($nameParts[1])) {
                        $nameParts[1] = '';
                    }

                    $userData['profile']['first_name'] = $nameParts[0];
                    $userData['profile']['last_name'] = $nameParts[1];
                }

                $user->setFirstname($userData['profile']['first_name']);
                $user->setLastname($userData['profile']['last_name']);
                $user->setPassword('');
                $user->setEnabled(true);
            }

            // Update slack id
            $user->setSlackId($userData['id']);

            $output->writeln(sprintf('User email %s is set with slack id %s', $user->getEmail(), $user->getSlackId()));
            $this->userManager->updateUser($user);
        }
    }

}