<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/17/18
 */

namespace PHBundle\Manager;


use Doctrine\ORM\EntityManager;
use PHBundle\Entity\ListStatus;
use PHBundle\Entity\Repo;
use PHBundle\Entity\Service;

class RepoManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * RepoManager constructor.
     * @param EntityManager $em
     * @param JobManager $jobManager
     */
    public function __construct(EntityManager $em, JobManager $jobManager)
    {
        $this->em = $em;
        $this->jobManager = $jobManager;
    }

    /**
     * Get repository
     * @return \PHBundle\Repository\RepoRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:Repo');
    }

    /**
     * Create repo with name & url
     * @param $name
     * @param $url
     * @return Repo
     */
    public function createWithNameUrl($name, $url)
    {
        $repo = new Repo();
        $repo->setName($name)
            ->setLink($url);

        return $repo;

    }

    /**
     * Save repo to db
     * @param Repo $repo
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Repo $repo)
    {
        $this->em->persist($repo);
        $this->em->flush();
    }

    /**
     * Get repo status
     * @param Service $service
     * @param null $type
     * @param null $env
     * @return ListStatus
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getServiceStatus(Service $service, $type = null, $env = null)
    {
        $result = new ListStatus();

        /** @var Repo $repo */
        foreach ($service->getRepos() as $repo) {
            $repoStatus = $this->jobManager->getJobsStatus($repo->getJobs(), $type, $env);
            if ($repoStatus->isAllSuccess()) {
                $result->success += $repoStatus->success;
            }

            $result->total += $repoStatus->total;
        }

        return $result;
    }

}