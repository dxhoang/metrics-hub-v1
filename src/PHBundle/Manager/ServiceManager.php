<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/17/18
 */

namespace PHBundle\Manager;


use Doctrine\ORM\EntityManager;

class ServiceManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ServiceManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get repo
     * @return \PHBundle\Repository\ServiceRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:Service');
    }

}