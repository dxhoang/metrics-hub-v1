<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 1:31 AM
 */

namespace PHBundle\Manager;


use Doctrine\ORM\EntityManager;
use PHBundle\Entity\TestCase;

class TestCaseManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * TestCaseManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \PHBundle\Repository\TestCaseRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:TestCase');
    }

    /**
     * Add by file
     * @param $file
     * @param string $name
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addByFile($file, $name)
    {
        $testCase = new TestCase();
        $testCase->setKatalonFile($file)
            ->setName($name)
        ;

        $this->em->persist($testCase);
        $this->em->flush();
    }

}