<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 10/1/18
 */

namespace PHBundle\Manager;


use Doctrine\ORM\EntityManager;
use PHBundle\Entity\TestRun;
use PHBundle\Entity\TestSuite;

class TestRunManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * TestRunManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \PHBundle\Repository\TestRunRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:TestRun');
    }

    /**
     * Create new test run
     * @param TestSuite $testSuite
     * @return TestRun
     */
    public function create($testSuite = null)
    {
        $testRun = new TestRun();
        if ($testSuite) {
            $testRun->setTestSuite($testSuite);
        }

        return $testRun;
    }

    /**
     * Save test run
     * @param TestRun $testRun
     * @param bool $flush
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TestRun $testRun, $flush = true)
    {
        $this->em->persist($testRun);
        if ($flush) {
            $this->em->flush();
        }
    }

}