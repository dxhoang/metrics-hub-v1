<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 1:31 AM
 */

namespace PHBundle\Manager;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use PHBundle\Constants;
use PHBundle\Entity\ListStatus;
use PHBundle\Entity\Repo;
use PHBundle\Entity\Service;
use PHBundle\Entity\TestSuite;

class TestSuiteManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var int
     */
    private $parallelSessions = 9; # 9 - katalon maximum is 9 parallel test suites in a collection

    /**
     * @var int
     */
    private $testSuiteCutOffMins = 20;

    /**
     * @var string
     */
    private $katalonBrowser;

    /**
     * @var JenkinsSiteManager
     */
    private $jenkinsSiteManager;

    /**
     * TestCaseManager constructor.
     * @param EntityManager $em
     * @param $testSuiteCutOffMins
     * @param $katalonBrowser
     * @param JenkinsSiteManager $jenkinsSiteManager
     */
    public function __construct(EntityManager $em, $testSuiteCutOffMins, $katalonBrowser,
                                JenkinsSiteManager $jenkinsSiteManager)
    {
        $this->em = $em;
        $this->testSuiteCutOffMins = $testSuiteCutOffMins;
        $this->katalonBrowser = $katalonBrowser;
        $this->jenkinsSiteManager = $jenkinsSiteManager;
    }

    /**
     * @return \PHBundle\Repository\TestSuiteRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:TestSuite');
    }

    /**
     * Add by file
     * @param TestSuite $testSuite
     * @param bool $flush
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TestSuite $testSuite, $flush = true)
    {
        $this->em->persist($testSuite);

        if ($flush) {
            $this->em->flush();
        }
    }

    /**
     * Build collection for test suites
     * @param array|Collection|ArrayCollection $testSuites
     * @return array
     */
    public function buildCollectionsForTestSuites($testSuites)
    {
        // Debug
        //$this->parallelSessions = 10;

        // Calculate how many collections do we need
        $collectionsNeeded = ceil(count($testSuites) / $this->parallelSessions);

        if (!$collectionsNeeded) {
            return array();
        }

        // Sort test suites by duration, fastest last
        $testSuiteCutOffMins = $this->testSuiteCutOffMins;
        usort($testSuites, function(TestSuite $left, TestSuite $right) use ($testSuiteCutOffMins) {
            $leftDuration = $left->getLastDuration(true, $testSuiteCutOffMins * 60);
            $rightDuration = $left->getLastDuration(true, $testSuiteCutOffMins * 60);

            return $rightDuration - $leftDuration;
        });

        // Build groups
        $testSuiteGroups = array();

        $i = 0;
        $groupId = 0;
        /** @var TestSuite $testSuite */
        foreach ($testSuites as $testSuite) {
            if (!isset($testSuiteGroups[$groupId])) {
                $testSuiteGroups[$groupId] = array();
            }

            // Add to group
            $filePath = $testSuite->getFilePath();
            $testSuiteGroups[$groupId][] = substr($filePath, 0, strlen($filePath) - 3); // Remov

            if ($i == ($this->parallelSessions - 1) ) {
                $groupId++;
                $i = 0;
            } else {
                $i++;
            }
        }

        $contents = array();
        // Build collection files
        foreach ($testSuiteGroups as $key => $filePaths) {
            $name = 'TSC-' . $key;

            $contents[] = array(
                'name' => $name,
                'browser' => $this->katalonBrowser,
                'filePaths' => $filePaths,
            );
        }

        return $contents;
    }

    /**
     * Get list status
     * @param Service $service
     * @param $env
     * @return ListStatus
     */
    public function getServiceStatus(Service $service, $env)
    {
        $result = new ListStatus();

        $testSuites = $this->getRepository()->createQueryBuilder('s')
            ->where('s.service = :service AND s.env = :env AND s.active = :active')
            ->setParameter('service', $service)
            ->setParameter('env', $env)
            ->setParameter('active', true)
            ->getQuery()
            ->getResult()
        ;

        /** @var TestSuite $testSuite */
        foreach ($testSuites as $testSuite) {
            if ($testSuite->isPassed()) {
                $result->success++;
            }
            $result->total++;
        }

        return $result;
    }

    /**
     * Group test suites by repo
     * @param $testSuites
     * @return array
     */
    public function groupTestSuitesByRepo($testSuites)
    {
        $result = array();

        /** @var TestSuite $testSuite */
        foreach ($testSuites as $testSuite) {
            // Skip no repo test suites
            if (!$testSuite->getRepo()) {
                continue;
            }

            if (!isset($result[$testSuite->getRepo()->getId()])) {
                $result[$testSuite->getRepo()->getId()] = array(
                    'repo' => $testSuite->getRepo(),
                    'test_suites' => array(),
                );
            }

            $result[$testSuite->getRepo()->getId()]['test_suites'][] = $testSuite;
        }

        return $result;
    }

    /**
     * Run test suites
     * @param array $testSuites
     * @param $env
     * @throws \Exception
     */
    public function runTestSuites(array $testSuites, $env)
    {
        // Group test suites by repo
        $testSuiteGroups = $this->groupTestSuitesByRepo($testSuites);

        // Traverse all groups to call correct jenkins job
        foreach ($testSuiteGroups as $testSuiteGroup) {
            // Build collections for Test Suites
            $collectionsXMLs = $this->buildCollectionsForTestSuites($testSuiteGroup['test_suites']);

            /** @var Repo $repo */
            $repo = $testSuiteGroup['repo'];
            $jobUrl = $repo->getKatalonJenkinsURLByEnv($env);

            if (!$jobUrl) {
                throw new \Exception('Job url is empty for repo ' . $repo->getId());
            }

            // Send to jenkins to start job
            $this->jenkinsSiteManager->getAPIService('builder')
                ->buildProductHubRunners($jobUrl, $collectionsXMLs);

            // Set test suites to queued and set run URL
            /** @var TestSuite $testSuite */
            foreach ($testSuiteGroup['test_suites'] as $testSuite) {
                $testSuite
                    ->setStatus(Constants::STATUS_QUEUED)
                    ->setRunUrl($jobUrl)
                ;
                $this->em->persist($testSuite);
            }
        }

        $this->em->flush();
    }

    /**
     * Set test suite status
     * @param TestSuite $testSuite
     * @param $status
     */
    public function setStatus(TestSuite $testSuite, $status)
    {
        $testSuite->setStatus($status);
        $this->em->persist($testSuite);
    }

    public function flush()
    {
        $this->em->flush();
    }

}