<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\Manager;


use AppBundle\Util\CommandEcho;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use PHBundle\API\Slack;
use PHBundle\Constants;
use PHBundle\Entity\Build;
use PHBundle\Entity\Job;
use PHBundle\Repository\BuildRepository;

class BuildManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var JenkinsSiteManager
     */
    private $jsManager;

    /**
     * @var Slack
     */
    private $slackApi;

    /**
     * BuildManager constructor.
     * @param EntityManager $em
     * @param JenkinsSiteManager $jsManager
     * @param Slack $slack
     */
    public function __construct(EntityManager $em, JenkinsSiteManager $jsManager, Slack $slack)
    {
        $this->em = $em;
        $this->jsManager = $jsManager;
        $this->slackApi = $slack;
    }

    /**
     * Get repo
     * @return BuildRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:Build');
    }

    /**
     * Get build status from data
     * @param $data
     * @return string
     */
    private function getBuildStatusFromData($data) {
        return $data['building'] ? 'BUILDING' : $data['result'];
    }

    /**
     * Add new job from data
     * @param $data
     * @param Job $job
     * @return Build
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addFromBuildData($data, Job $job)
    {
        $build = new Build();
        $build->setJob($job)
            ->setNumber($data['number'])
            ->setStatus($this->getBuildStatusFromData($data))
            ->setBuildTimestamp(ceil($data['timestamp'] / 1000))
        ;

        $this->em->persist($build);
        $this->em->flush();

        return $build;
    }

    /**
     * Update a build
     * @param $data
     * @param Build $build
     * @return Build
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateBuild($data, Build $build)
    {
        $build
            ->setStatus($this->getBuildStatusFromData($data))
            ->setBuildTimestamp(ceil($data['timestamp'] / 1000))
        ;
        $this->em->persist($build);
        $this->em->flush();

        return $build;
    }

    /**
     * Add or update build by build data
     * @param $buildShortData
     * @param Job $job
     * @param bool $isFullData
     * @param bool $notifyExternal
     * @return Build
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOrUpdateBuild($buildShortData, Job $job, $isFullData = false, $notifyExternal = false)
    {
        $jenkins = $this->jsManager->getAPIService($job->getJenkinsSite());

        // If build not existed
        $build = $this->getRepository()->findByBuildNumberAndJobId($buildShortData['number'], $job->getId());
        if ($isFullData) {
            $buildData = $buildShortData;
        } else {
            $buildData = $jenkins->getObject($buildShortData['url']);
        }
        if (!$build) {
            // Get build data
            return $this->addFromBuildData($buildData, $job);
        }
        // If we need to notify slack
        if ($notifyExternal) {
            $this->notifyExternal($job, $build);
        }

        return $this->updateBuild($buildData, $build);
    }

    /**
     * Return status of job
     * @param Job $job
     * @return string Can be FAILED, SUCCESS, BUILDING
     * @throws NonUniqueResultException
     */
    public function getJobStatus(Job $job)
    {
        // Get latest build exclude ABORTED
        $qb = $this->getRepository()->createQueryBuilder('b')
            ->where('b.status <> :aborted AND b.job = :job')
            ->orderBy('b.buildTimestamp', 'DESC')
            ->setMaxResults(1)
            ->setParameter('aborted', 'ABORTED')
            ->setParameter('job', $job)
        ;

        // If this job is automation test
        if ($job->getType() === Constants::AUTOMATION_TEST) {
            // If automation test require run within 12 hours ago
            $minBuildTimestamp = new \DateTime();
            $minBuildTimestamp->modify('-12 hours');

            $qb->andWhere('b.buildTimestamp >= :minBuildTimestamp')
                ->setParameter('minBuildTimestamp', $minBuildTimestamp->getTimestamp())
            ;
        }

        try {
            /** @var Build $build */
            $build = $qb->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return 'NONE';
        }

        // Return success if passed
        return $build->getStatus();
    }

    /**
     * Save build
     * @param Build $build
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Build $build)
    {
        $this->em->persist($build);
        $this->em->flush();
    }

    /**
     * Notify external
     * @param Job $job
     * @param Build $build
     */
    public function notifyExternal(Job $job, Build $build)
    {
        CommandEcho::writeln('Notify slack ' . $build->getStatus());
        if ($build->getStatus() === 'FAILURE') {
            /** @var User $user */
            foreach ($job->getFailureSubscribers() as $user) {
                if ($slackId = $user->getSlackId()) {
                    $this->slackApi->notifyFailure($slackId, $job, $build);
                }
            }
        }
    }

    /**
     * @param Job $job
     * @return bool|Build
     * @throws NonUniqueResultException
     */
    public function getLastSuccess(Job $job)
    {
        try {
            return $this->getRepository()->createQueryBuilder('b')
                ->where('b.status = :status and b.job = :job')
                ->orderBy('b.number', 'DESC')
                ->setMaxResults(1)
                ->setParameter('status', 'SUCCESS')
                ->setParameter('job', $job)
                ->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return false;
        }
    }

}