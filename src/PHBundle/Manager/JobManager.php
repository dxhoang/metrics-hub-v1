<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace PHBundle\Manager;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use PHBundle\Constants;
use PHBundle\Entity\Job;
use PHBundle\Entity\ListStatus;
use PHBundle\Repository\JobRepository;
use PHBundle\Util\JenkinsUtil;

class JobManager
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var BuildManager
     */
    private $buildManager;

    /**
     * JobManager constructor.
     * @param EntityManager $em
     * @param BuildManager $buildManager
     */
    public function __construct(EntityManager $em, BuildManager $buildManager)
    {
        $this->em = $em;
        $this->buildManager = $buildManager;
    }

    /**
     * Get repo
     * @return JobRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('PHBundle:Job');
    }

    /**
     * @param $name
     * @param $url
     * @param $fullDisplayName
     * @return Job
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createFromJenkinsJob($name, $url, $fullDisplayName)
    {
        $job = new Job();
        $job->setName($fullDisplayName)
            ->setUrl($url)
        ;

        // Detect type
        if (strpos($url, 'katalon') !== false) {
            $type = Constants::AUTOMATION_TEST;
        } else {
            $type = Constants::CODE_BUILD;
        }
        $job->setType($type);

        // Jenkins site
        foreach (Constants::$jenkinsSites as $key => $uri) {
            if (strpos($url, Constants::$jenkinsSites[$key]) !== false) {
                $job->setJenkinsSite($key);
            }
        }

        // Env
        $job->setEnv(JenkinsUtil::getEnvByUrl($job->getJenkinsSite(), $job->getUrl(), $name));

        $this->em->persist($job);
        $this->em->flush();

        return $job;
    }

    /**
     * Get repo status
     * @param ArrayCollection|array|Collection $jobs
     * @param null $type
     * @param null $env
     * @return ListStatus
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getJobsStatus($jobs, $type = null, $env = null)
    {
        $result = new ListStatus();

        /** @var Job $job */
        foreach ($jobs as $job) {
            // Filter type and env
            if ($type && $job->getType() !== $type) {
                continue;
            }
            if ($env && $job->getEnv() !== $env) {
                continue;
            }

            if ($this->buildManager->getJobStatus($job) === 'SUCCESS') {
                $result->success++;
            }

            $result->total++;
        }

        return $result;
    }

    /**
     * Save job
     * @param Job $job
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Job $job)
    {
        $this->em->persist($job);
        $this->em->flush();
    }

}