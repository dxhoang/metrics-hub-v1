<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace PHBundle\Manager;


use Buzz\Browser;
use Buzz\Client\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PHBundle\API\Jenkins;

class JenkinsSiteManager
{

    /**
     * @var int
     */
    private $clientTimeout = 15;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * JenkinsSiteManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get API service object
     * @param $siteCode
     * @return
     */
    public function getAPIService($siteCode)
    {
        $client = new Curl();
        $client->setTimeout($this->clientTimeout);

        $buzz = new Browser();
        $buzz->setClient($client);

        return new Jenkins($this->container->getParameter('jenkins_' . $siteCode . '_url'),
            $this->container->getParameter('jenkins_' . $siteCode . '_auth'), $buzz);
    }

}