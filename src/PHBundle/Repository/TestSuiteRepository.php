<?php
/**
 * User: quaninte
 * Date: 9/27/18
 * Time: 11:51 PM
 */

namespace PHBundle\Repository;


use Deployer\Collection\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use PHBundle\Constants;
use PHBundle\Entity\Repo;
use PHBundle\Entity\TestSuite;

class TestSuiteRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Find by code
     * @param $code
     * @return null|TestSuite
     */
    public function findByCode($code)
    {
        return $this->findOneBy(array(
            'code' => $code,
        ));
    }

    /**
     * Find by services
     * @param array|ArrayCollection|Collection $services
     * @param string $env
     * @param bool $activeOnly
     * @return array
     */
    public function findByServices($services, $env = 'all', $activeOnly = false)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.service IN (:services)')
            ->setParameter('services', $services);

        if ($env !== 'all') {
            $qb->andWhere('s.env = :env')
                ->setParameter('env', $env)
            ;
        }

        if ($activeOnly) {
            $qb->andWhere('s.active = :active')
                ->setParameter('active', true)
            ;
        }

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Find by file path
     * @param $filePath
     * @return null|TestSuite
     */
    public function findByFilePath($filePath)
    {
        return $this->findOneBy(array(
            'filePath' => $filePath,
        ));
    }

    /**
     * Find by file path and repo
     * @param $filePath
     * @param Repo $repo
     * @return null|TestSuite
     */
    public function findByFilePathAndRepo($filePath, Repo $repo)
    {
        return $this->findOneBy(array(
            'filePath' => $filePath,
            'repo' => $repo,
        ));
    }

    /**
     * Find test suite without any service
     * @return array
     */
    public function findNoService()
    {
        return $this->createQueryBuilder('s')
            ->where('s.service IS NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find timed out status test suites
     * @param string $currentTime
     * @param int $timeoutMins
     * @return array
     */
    public function findTimedOutStatus($currentTime = 'now', $timeoutMins = 20)
    {
        if ($currentTime === 'now') {
            $currentTime = new \DateTime();
        }

        return $this->createQueryBuilder('s')
            ->where('s.status IN (:statuses) AND s.updatedAt <= :lastTime')
            ->setParameters(array(
                'statuses' => array(
                    Constants::STATUS_RUNNING,
                    Constants::STATUS_QUEUED,
                ),
                'lastTime' => $currentTime->modify('-' . $timeoutMins . ' minutes')
            ))
            ->getQuery()
            ->getResult();
    }

}