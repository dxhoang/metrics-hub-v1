<?php
/**
 * User: quaninte
 * Date: 9/27/18
 * Time: 11:52 PM
 */

namespace PHBundle\Repository;


use PHBundle\Entity\TestCase;

class TestCaseRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Find by katalon file
     * @param $file
     * @return null|TestCase
     */
    public function findByKatalonFile($file)
    {
        return $this->findOneBy(array(
            'katalonFile' => $file,
        ));
    }

}