<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/13/18
 */

namespace PHBundle\Repository;


use PHBundle\Entity\Job;

class JobRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Get job by url
     * @param $url
     * @return null|Job
     */
    public function findOneByURL($url)
    {
        return $this->findOneBy(array(
            'url' => $url,
        ));
    }

}