<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/17/18
 */

namespace PHBundle\Repository;


use PHBundle\Entity\Repo;

class RepoRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Find repo by link
     * @param $link
     * @return null|Repo
     */
    public function findByLink($link)
    {
        return $this->findOneBy(array(
            'link' => $link,
        ));
    }

    /**
     * Find repos by scan test suite
     * @param bool $scanTestSuiteOnly
     * @return array
     */
    public function findByScanTestSuite($scanTestSuiteOnly = true)
    {
        return $this->findBy(array(
            'scanTestSuite' => $scanTestSuiteOnly,
        ));
    }

}