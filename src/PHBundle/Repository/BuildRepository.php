<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/14/18
 */

namespace PHBundle\Repository;


use PHBundle\Entity\Build;

class BuildRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Find build by number and jobId
     * @param $buildNumber
     * @param $jobId
     * @return null|Build
     */
    public function findByBuildNumberAndJobId($buildNumber, $jobId)
    {
        return $this->findOneBy(array(
            'number' => $buildNumber,
            'job' => $jobId,
        ));
    }

}