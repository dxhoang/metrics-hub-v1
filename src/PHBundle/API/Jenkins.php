<?php

namespace PHBundle\API;

use AppBundle\Util\CommandEcho;
use Buzz\Browser;
use Buzz\Exception\RequestException;
use Buzz\Listener\BasicAuthListener;
use Buzz\Message\Response;

class Jenkins
{

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var Browser
     */
    private $buzz;

    /**
     * Jenkins constructor.
     * @param string $baseUrl
     * @param string $authUserPwd
     */
    public function __construct($baseUrl, $authUserPwd, Browser $buzz)
    {
        $this->baseUrl = $baseUrl;
        $this->buzz = $buzz;

        if ($authUserPwd) {
            $authParams = explode(':', $authUserPwd);
            $this->buzz->addListener(new BasicAuthListener($authParams[0], $authParams[1]));
        }
    }

    /**
     * Get url to request
     * @param string $path
     * @return string
     */
    public function getUrl($path = '')
    {
        if (strpos($path, 'http') === 0) {
            $url = $path;
        } else {
            $url = $this->baseUrl . '/' . $path;
        }

        return rtrim($url, '/') . '/api/json';
    }

    /**
     * Get request
     * @param $url
     * @param int $retry
     * @param int $retried
     * @return bool|array
     */
    private function getRequest($url, $retry = 2, $retried = 0)
    {
        try {
            /** @var Response $res */
            $res = $this->buzz->get($url);
            if (!$res->isOK()) {
                return false;
            }
        } catch (RequestException $e) {
            CommandEcho::writeln('Request NOT OK! -> sleep and retry');
            usleep(500);

            if ($retried <= $retry) {
                return $this->getRequest($url, $retry, $retried + 1);
            }
            CommandEcho::writeln(sprintf('Failed to request %s after %d retries', $url, $retried));
            throw new $e;
        }

        return json_decode($res->getContent(), true);
    }

    /**
     * Get jobs
     * @param string $path
     * @return bool|array
     */
    public function getJobs($path = '')
    {
        if ($data = $this->getRequest($this->getUrl($path))) {
            return $data['jobs'];
        }

        return false;
    }

    /**
     * Return true for folder
     * @param $class
     * @return bool
     */
    public function isFolder($class) {
        $folderClasses = array(
            'com.cloudbees.hudson.plugins.folder.Folder',
            'org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject',
        );

        return in_array($class, $folderClasses);
    }

    /**
     * Get object data
     * @param string $path
     * @return bool|array
     */
    public function getObject($path)
    {
        if ($data = $this->getRequest($this->getUrl($path))) {
            return $data;
        }

        return false;
    }

    /**
     * Get builds array
     * @param $path
     * @return bool|array
     */
    public function getBuilds($path)
    {
        if ($data = $this->getObject($path)) {
            return $data['builds'];
        }

        return false;
    }

    /**
     * Get build by job url and builder number
     * @param $jobUrl
     * @param $buildNumber
     * @return bool|array
     */
    public function getBuildByJobUrlAndBuildNumber($jobUrl, $buildNumber)
    {
        if ($data = $this->getObject(trim($jobUrl, '/') . '/' . $buildNumber)) {
            return $data;
        }

        return false;
    }

    /**
     * Get job config file
     * @param $jobUrl
     * @return string
     */
    public function getJobConfigFile($jobUrl)
    {
        // Session cache
        if (!isset($this->jobConfigs)) {
            $this->jobConfigs = array();
        }

        if (isset($this->jobConfigs[$jobUrl])) {
            return $this->jobConfigs[$jobUrl];
        }

        $this->jobConfigs[$jobUrl] = $this->buzz->get($jobUrl . '/config.xml')->getContent();
        return $this->jobConfigs[$jobUrl];
    }

    /**
     * Update config content
     * @param $jobUrl
     * @param $configContent
     * @return boolean
     */
    public function updateJobConfigFile($jobUrl, $configContent)
    {
        return $this->buzz->post($jobUrl . '/config.xml', array(
            'Content-Type' => 'text/plain',
        ), $configContent)->isOK();
    }

    /**
     * Build runner job
     * @param $jobUrl
     * @param array $XMLs
     * @return array|bool|null|string
     */
    public function buildProductHubRunners($jobUrl, array $XMLs)
    {
        $res = $this->buzz->submit($jobUrl . 'buildWithParameters', array(
            'COLLECTION_XMLS' => json_encode($XMLs),
        ));

        $url = $res->getHeader('Location');
        if (substr($url, 0, 4) === 'http') {
            return $url;
        }

        return false;
    }

}
