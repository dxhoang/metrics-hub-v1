<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/18/18
 */

namespace PHBundle\API;


use AppBundle\Entity\Schedule;
use AppBundle\Util\CommandEcho;
use Buzz\Browser;
use PHBundle\Entity\Build;
use PHBundle\Entity\Job;
use PHBundle\Entity\TestRun;
use PHBundle\Entity\TestSuite;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;

class Slack
{

    /**
     * @var string
     */
    private $token;

    /**
     * @var Browser
     */
    private $buzz;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Slack constructor.
     * @param string $token
     * @param Browser $buzz
     * @param ContainerInterface $container
     */
    public function __construct($token, Browser $buzz, ContainerInterface $container)
    {
        $this->token = $token;
        $this->buzz = $buzz;
        $this->container = $container;
    }

    /**
     * Get endpoint url
     * @param $path
     * @return string
     */
    private function getUrl($path)
    {
        return 'https://slack.com/api/' . $path . '?token=' . $this->token;
    }

    /**
     * Get users
     * @return array
     */
    public function getUsers()
    {
        $res = $this->buzz->get($this->getUrl('users.list'));
        $arr = json_decode($res->getContent(), true);

        return $arr['members'];
    }

    /**
     * Notify failure
     * @param $slackId
     * @param Job $job
     * @param Build $build
     */
    public function notifyFailure($slackId, Job $job, Build $build)
    {
        $configUrl = $this->container->get('router')->generate('admin_ph_job_edit', array(
            'id' => $job->getId(),
        ), Router::ABSOLUTE_URL);
        $message = sprintf('Job <%s|%s> FAILED with build <%s|#%s> (<%s|config>)',
            $job->getUrl(), $job->getName(), $build->getUrl(), $build->getNumber(), $configUrl);

        $content = array(
            'channel' => $slackId,
            'text' => $message,
            'icon_emoji' => ':bee:',
            'username' => 'Product Hub',
        );
        $this->buzz->post($this->getUrl('chat.postMessage'), array(), $content);
    }

    /**
     * Notify test suite failure
     * @param $slackId
     * @param $testSuiteId
     * @param $filePath
     * @param $testRunId
     * @param null $status
     */
    public function notifyTestSuiteFailure($slackId, $testSuiteId, $filePath, $testRunId, $status)
    {
        $configUrl = $this->container->get('router')->generate('admin_ph_testsuite_edit', array(
            'id' => $testSuiteId,
        ), Router::ABSOLUTE_URL);

        $reportHTMLUrl = $this->container->get('router')->generate('test_run_view_report', array(
            'id' => $testRunId,
        ), Router::ABSOLUTE_URL);

        $message = sprintf('Suite `%s` FAILED with <%s|HTML Report> , info: %s (<%s|config>)',
            $filePath, $reportHTMLUrl, $status, $configUrl);

        $content = array(
            'channel' => $slackId,
            'text' => $message,
            'icon_emoji' => ':bee:',
            'username' => 'Product Hub',
        );
        $this->buzz->post($this->getUrl('chat.postMessage'), array(), $content);
    }

    /**
     * @param Schedule $schedule
     * @throws \Twig_Error
     */
    public function notifyBoard(Schedule $schedule)
    {
        $message = $this->container->get('templating')->render('@PH/Board/notify_board.txt.twig', array(
            'schedule' => $schedule,
        ));

        // Get all slack user names
        $slackChannels = $this->cleanTarget($schedule->getNotifySlacks());
        foreach ($slackChannels as $slackChannel) {
            CommandEcho::writeln('Notifying channel: ' . $slackChannel);

            $content = array(
                'channel' => $slackChannel,
                'text' => $message,
                'icon_emoji' => ':bee:',
                'username' => 'Product Hub',
            );
            $this->buzz->post($this->getUrl('chat.postMessage'), array(), $content);
        }
    }

    /**
     * Clean a target
     * @param $target
     * @return array
     */
    private function cleanTarget($target)
    {
        $result = array();

        $list = explode(',', $target);
        foreach ($list as $item) {
            if ($item) {
                $result[] = trim($item);
            }
        }

        return $result;
    }

}