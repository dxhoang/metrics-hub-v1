<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 9/19/18
 */

namespace PHBundle\Util;


use PHBundle\Constants;

class JenkinsUtil
{

    /**
     * Get job env
     * @param $jenkinsSite
     * @param $url
     * @param $name
     * @return string
     */
    public static function getEnvByUrl($jenkinsSite, $url, $name)
    {
        $env = '';

        foreach (Constants::$ENVs as $key => $label) {
            // CICD jenkins
            if ($jenkinsSite == 'cicd') {
                switch ($name) {
                    case 'master':
                        $env = 'staging';
                        break;

                    case 'staging':
                        $env = 'dev';
                        break;

                    case 'production':
                        $env = 'prod';
                        break;
                }
            } else {
                // Builder
                if ($key === 'staging') {
                    $keyword = 'stag';
                } else {
                    $keyword = $key;
                }
                if (strpos(strtolower($url), $keyword) !== false) {
                    $env = $key;
                }
            }

            // Break if found
            if ($env) {
                break;
            }
        }

        // If not found env, set to prod
        if (!$env) {
            if (strpos($url, 'katalon') !== false) {
                $env = 'staging';
            } else {
                $env = 'prod';
            }
        }

        return $env;
    }

}