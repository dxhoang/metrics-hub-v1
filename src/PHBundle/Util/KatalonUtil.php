<?php
/**
 * User: quaninte
 * Date: 10/10/18
 * Time: 1:06 AM
 */

namespace PHBundle\Util;


use PHBundle\Constants;

class KatalonUtil
{

    /**
     * Return true of this report contain keyword from server error
     * @param $xmlContent
     * @return bool
     */
    public static function isServerError($xmlContent)
    {
        foreach (Constants::$testServerErrorKeywords as $keyword) {
            if (strpos($xmlContent, $keyword) !== false) {
                return true;
            }
        }
    }

}