<?php
/**
 * User: quaninte
 * Date: 9/28/18
 * Time: 12:57 AM
 */

namespace PHBundle\Util;


use AppBundle\Util\CommandEcho;

class GitUtil
{

    /**
     * Clone or update a repo
     * @param $uri
     * @param $target
     * @param string $branch
     */
    public function cloneOrUpdateRepo($uri, $target, $branch = 'master')
    {
        // If dir exist
        if (is_dir($target)) {
            // Git pull
            $cmd = sprintf('cd %s && git pull origin %s', $target, $branch);
            $this->exec($cmd);
        } else {
            // Else
            // Mkdir -p
            $cmd = sprintf('mkdir -p %s', $target);
            $this->exec($cmd);

            // Clone
            $cmd = sprintf('git clone -b %s %s %s', $branch, $uri, $target);
            $this->exec($cmd);
        }
    }

    /**
     * Execute
     * @param $cmd
     */
    private function exec($cmd)
    {
        CommandEcho::writeln('Executing: ' . $cmd);
        shell_exec($cmd);
    }

}