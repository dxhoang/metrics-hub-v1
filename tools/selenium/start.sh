#!/usr/bin/env bash
export PATH=$(PWD):$PATH

java -jar selenium-server-standalone-3.9.1.jar

# On ubuntu
#xvfb-run java -Dwebdriver.chrome.driver=/home/run/sites/metrics-hub/tools/selenium/chromedriver -jar selenium-server-standalone-3.12.0.jar