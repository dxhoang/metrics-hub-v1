#!/usr/bin/env bash

kill $(ps -eo comm,pid,etimes | grep firefox | awk '{if ($3 > 1200) { print $3}}') > /tmp/null
kill $(ps -eo comm,pid,etimes | grep chrome | awk '{if ($3 > 1200) { print $3}}') > /tmp/null