var casper = require('casper').create({
    viewportSize: {width: 1280, height: 1024},
    verbose: true,
    // logLevel: 'debug',
    userAgent: 'Mozilla/5.0  poi poi poi (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22'
});

var url = atob(casper.cli.get(0));
var output = casper.cli.get(1);

var mixpanelLogin = 'https://mixpanel.com/login/?next=/report/566847/ab_test/';
var email = casper.cli.get('email');
var password = casper.cli.get('password');
var waitTime = 12000;

// Open login page
casper.start(mixpanelLogin, function() {
    // Enter email & password
    this.fill('form', {
        'email': email,
        'password': password
    }, true);

    // Wait for logged in
    casper.log('Start waiting for #blue_header');
    casper.waitForSelector('#blue_header', function() {

        casper.log('-- Found');

        // Open url
        casper.thenOpen(url, function () {
            casper.log('Start waiting for report content');
            casper.waitForSelector('.inner_wrap .columns .column, .second_col, .header.desc, .report-segmentation', function() {

                // Remove unused elements
                this.evaluate(function(){
                    //delete div 'content-meta'
                    $('.report_header, .segfilter, .retention_event_controls').hide();
                });

                // Wait 5s then take screenshot
                this.wait(waitTime, function () {
                    this.captureSelector(output, '#padding_wrapper');
                });
                casper.log('Completed!', 'info');
            }, function() {
                this.capture('error.png');
                casper.log('Failed to wait for report content, see error.png for screenshot', 'error');
            });
        });

    });

});
casper.run();