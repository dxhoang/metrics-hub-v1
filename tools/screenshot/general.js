var casper = require('casper').create({
    verbose: true,
    // logLevel: 'debug',
    userAgent: 'Mozilla/5.0  poi poi poi (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22'
});

var url = atob(casper.cli.get(0));
var output = casper.cli.get(1);

var width = casper.cli.get('width');
var height = casper.cli.get('height');
var waitTime = casper.cli.get('wait-time');

var maEmail = casper.cli.get('ma-email');
var maPassword = casper.cli.get('ma-password');

// Sizing viewport
casper.options.viewportSize = {width: width, height: height};

// Login mode
casper.start('https://modeanalytics.com/signin', function() {
    // Enter email & password
    this.fill('form', {
        'auth_key': maEmail,
        'password': maPassword
    }, true);

    // Once logged in
    casper.log('Wait for logged in');
    casper.waitForSelector('.account-body', function() {

        casper.log('Now open report url');
        this.thenOpen(url, function () {
            this.wait(waitTime, function () {
                this.captureSelector(output, 'html');
            });
        });
    });
});
casper.run();